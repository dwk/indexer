#ifndef DWIDSOURCES_H
#define DWIDSOURCES_H

#include "idxh.h"
#include <list>
#include <QWidget>
#include "widactor.h"
#include "globalenum.h"
#include "sourcequerymodel.h"
#include <QItemSelection>
#include <QStandardItem>

Q_DECLARE_LOGGING_CATEGORY(dwidsources)

namespace Ui {
class DWidSources;
}

class DWidSources : public QWidget, public WidActor
{
	Q_OBJECT
public:
	explicit DWidSources(QWidget *parent = 0);
	~DWidSources();
// WidActor
	virtual void connectAll(ObjectMSP &otherActors);
public slots:
	virtual void setupSl();
//
	void openSourceSl();
private:
	Ui::DWidSources *ui;
	SourceQueryModel *model;
	bool supressSelectionSignal=false;
	void applyTableGeom();
private slots:
	void sourceSelectedSl(int sourcePk);
	void imageActionGSl(int imagePk, ImageAction action);
	void editFinishedSl(const QModelIndex &topLeft, const QModelIndex &bottomRight);
	void tableSelChangedSl(const QItemSelection & selected, const QItemSelection & deselected);
	void on_pushNew_clicked();
	void on_pushNewRecursive_clicked();
	void on_pushDelete_clicked();
	void on_pushReload_clicked();
	void on_treeView_doubleClicked(const QModelIndex &index);
	void customContextMenuRequestedSl(const QPoint &pos);
signals:
	void sourceSelectedSig(int sourcePk);
};

#endif // DWIDSOURCES_H
