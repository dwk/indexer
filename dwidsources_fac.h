#ifndef DWIDSOURCESFAC_H
#define DWIDSOURCESFAC_H

#include "dockwidfactory.h"

namespace
{
QWidget * DockCreator(QWidget * parent)
{
	return new DWidSources(parent);
}
const char *techname="dwid_sources";
bool dockWidRegistered=DockWidFactory::get().registerDockWid("Sources", techname, Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea, Qt::BottomDockWidgetArea, DockCreator);
}
#endif // DWIDSOURCESFAC_H
