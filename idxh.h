#ifndef IDXH_H
#define IDXH_H

#include <vector>
#include <list>
#include <map>
#include <set>
#include <QString>
#include <QtGlobal>
#include <QLoggingCategory>

#define VERSION_STR "idxGUI1.1"

class QWidget;
class QObject;
typedef std::map< QString, QObject * > ObjectMSP;

#endif // IDXH_H
