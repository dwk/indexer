#ifndef DWIDIMAGEFAC_H
#define DWIDIMAGEFAC_H

#include "dockwidfactory.h"

namespace
{
QWidget * DockCreator(QWidget * parent)
{
	return new DWidImage(parent);
}
const char *techname="dwid_image";
bool dockWidRegistered=DockWidFactory::get().registerDockWid("Image", techname, Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea, Qt::BottomDockWidgetArea, DockCreator);
}
#endif // DWIDIMAGEFAC_H
