#include "widlabel.h"
#include <QPainter>
#include <QPainterPath>
#include <QLabel>
#include <QMenu>
#include <QContextMenuEvent>
#include <QSqlQuery>
#include <QSqlError>
#include <QRegularExpression>
#include <QRegularExpressionMatchIterator>
#include "idxi.h"

Q_LOGGING_CATEGORY(widlabel, "widlabel")

MarkerWid::MarkerWid(MarkerWid::Type type, QWidget *parent) :
	QWidget(parent), t(type)
{
	sz=QSize(MARKERSIZE*2, MARKERSIZE*2);
	setMinimumSize(sz);
}

void MarkerWid::enterEvent(QEvent *event)
{
	Q_UNUSED(event)
	if(s==Style::Strong)
		emit markerEnteredSig();
		//qDebug()<<"MarkerWid::enterEvent";
}

void MarkerWid::paintEvent(QPaintEvent *event)
{
	Q_UNUSED(event)
	QPainter painter(this);
	QPainterPath path;
	QBrush br(Qt::NoBrush);
	bool fill=false;
	switch(s)
	{
	case Style::Hint:
		painter.setPen(Qt::darkGray);
		break;
	case Style::Strong:
		painter.setPen(QPen(QBrush(Qt::black), 3));
		break;
	case Style::Active:
		painter.setPen(Qt::red);
		br=QBrush(Qt::red);
		fill=true;
		break;
	}
	switch(t)
	{
	case Type::None:
		return;
	case Type::MoreAbove:
		path.moveTo(MARKERSIZE, 0);
		path.lineTo(MARKERSIZE*2, MARKERSIZE);
		path.lineTo(0, MARKERSIZE);
		path.closeSubpath();
		break;
	case Type::MoreBelow:
		path.moveTo(0, 0);
		path.lineTo(MARKERSIZE*2, 0);
		path.lineTo(MARKERSIZE, MARKERSIZE);
		path.closeSubpath();
		break;
	case Type::MoreLeft:
		path.moveTo(0, MARKERSIZE);
		path.lineTo(MARKERSIZE, 0);
		path.lineTo(MARKERSIZE,MARKERSIZE*2);
		path.closeSubpath();
		break;
	case Type::MoreRight:
		path.moveTo(0, 0);
		path.lineTo(0,MARKERSIZE*2);
		path.lineTo(MARKERSIZE, MARKERSIZE);
		path.closeSubpath();
		break;
	}
	if(fill)
		painter.fillPath(path, br);
	painter.drawPath(path);
}



TagWid::TagWid(int tagPk, const QString &text, const QString &baseStyle, bool hasChildren, int minWidth, int actionIndicator, QWidget *parent) :
	QWidget(parent), lab(new QLabel(text, this)), styleBase(baseStyle), colBack(Qt::white), colFont(Qt::black), pk(tagPk)
{
	QStringList styles=styleBase.split(";", Qt::SkipEmptyParts);
	foreach(QString st, styles)
	{
		QStringList args=st.split(":");
		if(args.size()!=2)
		{
			qDebug()<<"invalid style"<<st;
			continue;
		}
		QString attribute=args.at(0).trimmed(), value=args.at(1).trimmed();
		if(attribute=="background" || attribute=="background-color")
		{
			colBack=cssVal2Color(value);
		}
		else if(attribute=="color" || attribute=="font-color")
		{
			colFont=cssVal2Color(value);
		}
		else if(attribute=="shape")
		{
			if(value=="box")
				shape=Shape::BOX;
			else if(value=="rounded-box")
				shape=Shape::RBOX;
			else if(value=="diamond")
				shape=Shape::DIAMOND;
			else
				qDebug()<<"invalid shape"<<value;
		}
		else
			qDebug()<<"invalid attribute"<<attribute;
	}
	lab->setStyleSheet(cssCompose());
	lab->show();
	sz.setHeight(lab->height()*2);
	sz.setWidth(std::max(lab->width()+20, minWidth));
	setMinimumSize(sz);
	setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	lab->move(sz.width()/2-lab->width()/2, sz.height()/2-lab->height()/2-1);
	if(hasChildren)
	{
		childMarker=new MarkerWid(MarkerWid::Type::MoreRight, this);
		childMarker->setStyle(MarkerWid::Style::Strong);
		childMarker->move(sz.width()-MARKERSIZE, sz.height()/2-MARKERSIZE);
		connect(childMarker, &MarkerWid::markerEnteredSig, this, [this]() -> void {actionMapSl(LabelAction::EXPAND);});
	}
	if(actionIndicator>=0 && actionIndicator<10)
	{
		actInd=new QLabel(QString::number(actionIndicator), this);
		actInd->setStyleSheet("background: white");
		actInd->show();
		actInd->move(0, sz.height()-actInd->height()-1);
	}
	//if(shape==Shape::RBOX)
	//	lab->hide();
}

int TagWid::getPosHint() const
{
	return posHint;
}

void TagWid::setPosHint(int value)
{
	posHint = value;
}

void TagWid::contextMenuEvent(QContextMenuEvent *event)
{
	QMenu menu(this);
	QAction *act=nullptr;
	if(opMode==WidOperationMode::DATABASE_UPDATE)
	{
		// selection
		int sels=DCON.getSelectedImages().size();
		act=menu.addAction(tr("attach to selected images"));
		act->setEnabled(sels>1);
		connect(act, &QAction::triggered, this, [this]() -> void {actionMapSl(LabelAction::SELECTION_FORCESET);});
		act=menu.addAction(tr("remove from selected images"));
		act->setEnabled(sels>1);
		connect(act, &QAction::triggered, this, [this]() -> void {actionMapSl(LabelAction::SELECTION_FORCECLEAR);});
		// source
		bool hasSource=(DCON.getCurrentSourcePk()>0);
		act=menu.addAction(tr("attach to all images in source"));
		act->setEnabled(hasSource);
		connect(act, &QAction::triggered, this, [this]() -> void {actionMapSl(LabelAction::SOURCE_FORCESET);});
		act=menu.addAction(tr("remove from all images in source"));
		act->setEnabled(hasSource);
		connect(act, &QAction::triggered, this, [this]() -> void {actionMapSl(LabelAction::SOURCE_FORCECLEAR);});
		// marked
		int mrks=DCON.getMarkedImages().size();
		if(mrks>1)
			act=menu.addAction(tr("attach to marked images"));
		else
			act=menu.addAction(tr("attach to marked image"));
		act->setEnabled(mrks>0);
		connect(act, &QAction::triggered, this, [this]() -> void {actionMapSl(LabelAction::MARKED_FORCESET);});
		if(mrks>1)
			act=menu.addAction(tr("remove from marked images"));
		else
			act=menu.addAction(tr("remove from marked image"));
		act->setEnabled(mrks>0);
		connect(act, &QAction::triggered, this, [this]() -> void {actionMapSl(LabelAction::MARKED_FORCECLEAR);});
	}
	// other actions
	act=menu.addAction(tr("search for this tag"));
	connect(act, &QAction::triggered, this, [this]() -> void {actionMapSl(LabelAction::SEARCH);});
	act=menu.addAction(tr("expand sub-tags"));
	connect(act, &QAction::triggered, this, [this]() -> void {actionMapSl(LabelAction::EXPAND);});
	menu.exec(event->globalPos());
}

void TagWid::mouseReleaseEvent(QMouseEvent *event)
{
	if(event->button()==Qt::LeftButton)
	{
		emit labelCmdSig(pk, LabelAction::SELECTION_APPLY);
		return;
	}
	QWidget::mouseReleaseEvent(event);
}

void TagWid::paintEvent(QPaintEvent *event)
{
	Q_UNUSED(event)
	QPainter painter(this);
	int r=sz.height()/2-1; // height is always even
	int d=sz.width()-2*r-1;
	switch(shape)
	{
	case Shape::BOX:
	{
		QRect re(0, 0, sz.width()-2, sz.height()-3);
		painter.setPen(colFont);
		painter.setBrush(colBack);
		painter.drawRect(re);
		break;
	}
	case Shape::RBOX:
	{
		QPainterPath p;
		painter.setRenderHint(QPainter::Antialiasing);
		painter.setPen(colFont);
		painter.setBrush(colBack);
		QRectF lb(0, 0, 2*r, 2*r), rb(d, 0, 2*r, 2*r);
		qreal a1(90), a2(270), a(180);
		//painter.drawPie(lb, a1*16, a*16);
		//painter.drawPie(rb, a2*16, a*16);
		//qDebug()<<"RBOX"<<r<<d<<lb<<rb<<a1<<a2<<a;
		p.moveTo(r+d, 0);
		//p.moveTo(r, r);
		p.arcTo(lb, a1, a);
		//p.moveTo(r+d, r);
		p.arcTo(rb, a2, a);
		p.closeSubpath();
		painter.drawPath(p);
		break;
	}
	case Shape::DIAMOND:
	{
		QPainterPath p;
		painter.setPen(colFont);
		painter.setBrush(colBack);
		p.moveTo(r, 0);
		p.lineTo(r+d, 0);
		p.lineTo(2*r+d, r);
		p.lineTo(r+d, 2*r);
		p.lineTo(r, 2*r);
		p.lineTo(0, r);
		p.closeSubpath();
		painter.drawPath(p);
		break;
	}
	}
}

QColor TagWid::cssVal2Color(const QString &value)
{
	QColor res(Qt::gray);
	if(value.startsWith("rgb"))
	{
		QRegularExpression rx("[0-9]+");
		QRegularExpressionMatchIterator matches = rx.globalMatch(value);
		std::vector<int> rgbs;
		while (matches.hasNext())
		{
			QRegularExpressionMatch match = matches.next();
			bool ok=false;
			int col=match.captured(0).toInt(&ok);
			if(!ok)
				break;
			rgbs.push_back(col);
		}
		if(rgbs.size()!=3)
		{
			qDebug()<<"invalid background rgb spec"<<value;
			return res;
		}
		res.setRgb(rgbs.at(0), rgbs.at(1), rgbs.at(2));
	}
	else if(value.startsWith("#") && value.size()==7)
	{
		bool ok=false;
		quint32 cv=value.mid(1).toUInt(&ok, 16);
		if(!ok)
		{
			qDebug()<<"invalid background #rrggbb spec"<<value;
			return res;
		}
		res=QColor(QRgb(0xff000000+cv));
	}
	else if(value.startsWith("#") && value.size()==9)
	{
		bool ok=false;
		QRgb cv(value.mid(1).toUInt(&ok, 16));
		if(!ok)
		{
			qDebug()<<"invalid background #aarrggbb spec"<<value;
			return res;
		}
		res=QColor(cv);
	}
	else
		qDebug()<<"invalid color spec"<<value;
	return res;
}

QString TagWid::cssCompose()
{
	QString res("background-color: %1;color: %2");
	//font-weight: bold|normal
	return res.arg(colBack.name(), colFont.name());
}

