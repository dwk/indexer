#ifndef GLOBALENUM_H
#define GLOBALENUM_H

#include <QObject>

enum class ImageAction {SELECT=1,	// single image selected or none selected if pk=-1
						SELECT_MULTI, // update DCON first, then emit with pk=-1; do not use for single or empty selection
						MARK,		// adds and removes single images from the marked set; used between WidImange and DWidImage
						UNMARK,		// ^
						MARK_MULTI, // between WidImange and DWidImage "pk" denotes action (-2=none, -3=selection); globally means marked set has changed (retreive from DCON)
						DELETE, 	// delete image file
						SAVE_AS,	// save (full resolution) image to file system
						LABELS_CHANGED, // the labels attached to this image changed; if pk=-1 labels of multiple images changed
						ATTRIB_CHANGED, // at least one attribute changed (decscription, GPS etc. but NOT pixel data or labels)
						PIXEL_CHANGED
						};
Q_DECLARE_METATYPE(ImageAction)
QString enumImageActionStr(ImageAction ia);
/*
	switch(action)
	{
	case ImageAction::SELECT:
	case ImageAction::SELECT_MULTI:
	case ImageAction::MARK:
	case ImageAction::UNMARK:
	case ImageAction::MARK_MULTI:
	case ImageAction::DELETE:
	case ImageAction::SAVE_AS:
	case ImageAction::LABELS_CHANGED:
	case ImageAction::ATTRIB_CHANGED:
	case ImageAction::PIXEL_CHANGED:
		break;
	}
*/

enum class LabelAction {NONE=0,
						SELECTION_APPLY,
						SELECTION_FORCESET,
						SELECTION_FORCECLEAR,
						SOURCE_FORCESET,
						SOURCE_FORCECLEAR,
						MARKED_FORCESET,
						MARKED_FORCECLEAR,
						EXPAND,
						SEARCH
					   };
Q_DECLARE_METATYPE(LabelAction)
QString enumLabelActionStr(LabelAction ia);

enum class WidOperationMode {DATABASE_UPDATE, APPLY_SIG};
Q_DECLARE_METATYPE(WidOperationMode)
QString enumWidOperationModeStr(WidOperationMode wom);

enum class DisplayPurpose {UNDEF=0, IMAGE, PROCRES1, PROCRES2};
Q_DECLARE_METATYPE(DisplayPurpose)


#endif // GLOBALENUM_H
