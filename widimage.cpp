#include "widimage.h"
#include <QMouseEvent>
#include <QPixmap>
#include <QMenu>
#include "idxi.h"

Q_LOGGING_CATEGORY(widimage, "widimage")

WidImage::WidImage(QWidget *parent) :
	QLabel(parent), magnified(new QLabel(this))
{
	setAlignment(Qt::AlignCenter);
}

WidImage::WidImage(const QString &text, QWidget *parent) :
	QLabel(text, parent), magnified(new QLabel(this))
{
	setAlignment(Qt::AlignCenter);
}

void WidImage::setPixmapExt(int imagePk, QPixmap *fullSize, const QPixmap *forDisplay)
{
	pk=imagePk;
	origPix=fullSize?*fullSize:QPixmap();
	if(fullSize && forDisplay)
	{
		if(fullSize->isNull() || forDisplay->isNull())
		{
			scale=1.;
			setText(tr("<NO pixmap>"));
			return;
		}
		scale=(double)fullSize->width()/(double)forDisplay->width();
		QLabel::setPixmap(*forDisplay);
	}
	else if(fullSize)
	{
		scale=1.;
		if(fullSize->isNull())
		{
			setText(tr("<no pixmap>"));
			return;
		}
		QLabel::setPixmap(origPix);
	}
	else if(forDisplay)
	{
		scale=1.;
		if(forDisplay->isNull())
		{
			setText(tr("<no image>"));
			return;
		}
		QLabel::setPixmap(*forDisplay);
	}
	else
	{
		// makes sense by now  (full screen) qWarning()<<"setPixmapExt(nullptr,nullptr) makes no sense";
		setText(tr("see output window"));
		return;
	}
}

void WidImage::setMark(bool mark)
{
	if(mark==marked)
		return;
	marked=mark;
	setFrameShape(marked?QFrame::WinPanel:QFrame::NoFrame);
	update();
}

void WidImage::setPixmap(const QPixmap &pixmap)
{
	setPixmapExt(-2, nullptr, &pixmap);
}

void WidImage::contextMenuEvent(QContextMenuEvent *event)
{
	//if(!origPix.isNull())
	//	return; // currently no context menu for full sized images
	QMenu menu(this);
	QAction *act=menu.addAction(tr("switch to this image"), this, [this]()->void{imageActSl(ImageAction::SELECT, 0);});
	act->setEnabled(pk>0);
	act=menu.addAction(tr("mark all images"), this, [this]()->void{imageActSl(ImageAction::MARK_MULTI, -3);});
	act->setEnabled(DCON.getSelectedImages().size()>1);
	act=menu.addAction(tr("unmark all images"), this, [this]()->void{imageActSl(ImageAction::MARK_MULTI, -2);});
	act->setEnabled(DCON.getMarkedImages().size()>0);
	act=menu.addAction(tr("save image as..."), this, [this]()->void{imageActSl(ImageAction::SAVE_AS, 0);});
	act->setEnabled(!origPix.isNull());
	menu.addSeparator();
	act=menu.addAction(tr("delete current image"), this, [this]()->void{imageActSl(ImageAction::DELETE, 0);});
	//act->setEnabled(false);
	menu.exec(event->globalPos());
}

void WidImage::mouseDoubleClickEvent(QMouseEvent *event)
{
	Q_UNUSED(event);
	imageActSl(ImageAction::SELECT, 0);
}

void WidImage::mouseMoveEvent(QMouseEvent *event)
{
	//qDebug()<<"MOVE!"<<event->pos()<<event->button();
	if(magnified->isVisible()) //event->button()==Qt::MidButton) move never reports a key...
	{
		updateMagnified(event->localPos().toPoint());
		return;
	}
	QLabel::mouseMoveEvent(event);
}

void WidImage::mousePressEvent(QMouseEvent *event)
{
	if(origPix.isNull() && event->button()==Qt::LeftButton)
	{ // selection enabled only for thumbs
		setMark(!marked);
		emit imageActionSig(pk, marked?ImageAction::MARK:ImageAction::UNMARK);
	}
	else if(!origPix.isNull() && event->button()==Qt::MidButton)
	{
		updateMagnified(event->localPos().toPoint());
		magnified->show();
		if(firstever)
		{ // hack - size is wrong after first show, no idea why
			firstever=false;
			magnified->hide();
			magnified->show();
		}
		return;
	}
	QLabel::mousePressEvent(event);
}

void WidImage::mouseReleaseEvent(QMouseEvent *event)
{
	if(!origPix.isNull() && event->button()==Qt::MidButton)
	{
		magnified->hide();
		return;
	}
	QLabel::mouseReleaseEvent(event);
}

void WidImage::updateMagnified(const QPoint &rawPos)
{
	int srcPxWidth=DCON.getMagnifierWidth()/2;
	int srcMag=DCON.getMagnifierMag();
	//qDebug()<<"showMagnified"<<rawPos;
	if(origPix.isNull())
		return;
	int cx=scale*rawPos.x();
	if(cx<srcPxWidth)
		cx=srcPxWidth;
	else if(cx+srcPxWidth>=origPix.width())
		cx=origPix.width()-srcPxWidth-1;
	int cy=scale*rawPos.y();
	if(cy<srcPxWidth)
		cy=srcPxWidth;
	else if(cy+srcPxWidth>=origPix.height())
		cy=origPix.height()-srcPxWidth-1;
	QPixmap cut=origPix.copy(cx-srcPxWidth, cy-srcPxWidth, 2*srcPxWidth, 2*srcPxWidth);
	magnified->setPixmap(cut.scaled(2*srcPxWidth*srcMag, 2*srcPxWidth*srcMag));
	int dpx=rawPos.x()-srcPxWidth*srcMag, dpy=rawPos.y()-srcPxWidth*srcMag;
	if(dpx<0)
		dpx=0;
	else if(dpx>+width()-2*srcPxWidth*srcMag)
		dpx=width()-1-2*srcPxWidth*srcMag;
	if(dpy<0)
		dpy=0;
	else if(dpy>=height()-2*srcPxWidth*srcMag)
		dpy=height()-1-2*srcPxWidth*srcMag;
	magnified->move(dpx, dpy);
}

void WidImage::imageActSl(ImageAction act, int actExt)
{
	switch(act)
	{
	case ImageAction::MARK_MULTI:
		emit imageActionSig(actExt, act);
		return;
	default: // all other actions are broadcasted
		emit imageActionSig(pk, act);
	}
}
