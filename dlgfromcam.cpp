#include "dlgfromcam.h"
#include <QFileDialog>
#include "ui_dlgfromcam.h"
#include "idxi.h"

Q_LOGGING_CATEGORY(dlgfromcam, "dlgfromcam")


DlgFromCam::DlgFromCam(QWidget *parent) :
	QDialog(parent), ui(new Ui::DlgFromCam)
{
	ui->setupUi(this);
	QSettings settings;
	ui->lineBase->setText(settings.value(QStringLiteral("fromCam/base")).toString());
	ui->lineCam->setText(settings.value(QStringLiteral("fromCam/cam")).toString());
	ui->lineExcludeFiles->setText(settings.value(QStringLiteral("fromCam/excludeFiles")).toString());
	ui->lineDateFormatr->setText(settings.value(QStringLiteral("fromCam/dateFormat")).toString());
	ui->labelInfo->setText(tr("press Check! when ready..."));
	ui->labelInfo_2->clear();
	ui->progressBar->setVisible(false);
}

DlgFromCam::~DlgFromCam()
{
	delete dbase;
	delete dcam;
	delete ui;
}

void DlgFromCam::on_lineBase_textChanged(const QString &)
{
	ui->pushCopy->setEnabled(false);
	ui->pushAdd2Old->setEnabled(false);
	ui->labelInfo->setText(tr("press Check! when ready..."));
	ui->labelInfo_2->clear();
}

void DlgFromCam::on_toolBase_clicked()
{
	QString newbase=QFileDialog::getExistingDirectory(this, tr("Source"), ui->lineBase->text());
	if(newbase.isEmpty())
		return; // Cancel selected
	ui->lineBase->setText(newbase);
}

void DlgFromCam::on_lineCam_textChanged(const QString &)
{
	ui->pushCopy->setEnabled(false);
	ui->pushAdd2Old->setEnabled(false);
	ui->labelInfo->setText(tr("press Check! when ready..."));
	ui->labelInfo_2->clear();
}

void DlgFromCam::on_toolCam_clicked()
{
	QString newcam=QFileDialog::getExistingDirectory(this, tr("Source"), ui->lineCam->text());
	if(newcam.isEmpty())
		return; // Cancel selected
	ui->lineCam->setText(newcam);
}

void DlgFromCam::on_pushCheck_clicked()
{
	filesToCopy.clear();
	delete dbase;
	dbase=new QDir(ui->lineBase->text());
	if(!dbase->exists())
	{
		ui->labelInfo->setText(tr("Base folder does not exist."));
		return;
	}
	delete dcam;
	dcam=new QDir(ui->lineCam->text());
	if(!dcam->exists())
	{
		ui->labelInfo->setText(tr("Camera folder does not exist."));
		return;
	}
	QStringList licam=dcam->entryList(QDir::Files, QDir::Name);
	QStringList excludes=ui->lineExcludeFiles->text().split(';', Qt::SkipEmptyParts);
	foreach(QString excl, excludes)
	{
		licam.removeAll(excl);
	}
	if(licam.isEmpty())
	{
		QMessageBox::warning(this, tr("Copy warning"), tr("Can't find any files to copy."));
		return;
	}
	QStringList libase=dbase->entryList(QDir::Dirs, QDir::Name);
	int bestIndex=-1;
	refdir.clear();
	bool fastBreak=ui->checkFast->isChecked();
	for(int ib=libase.size()-1; ib>=0; --ib)
	{
		if(libase.at(ib).startsWith('.'))
			continue;
		qDebug()<<"will check"<<libase.at(ib);
		QDir dtest(dbase->absoluteFilePath(libase.at(ib)));
		QStringList litest=dtest.entryList(QDir::Files, QDir::Name);
		int match=licam.lastIndexOf(litest.last());
		if(match>bestIndex)
		{
			bestIndex=match;
			refdir=libase.at(ib);
		}
		qDebug()<<"candidate"<<litest.last()<<"at"<<match<<bestIndex<<fastBreak;
		if(fastBreak && bestIndex>=0 && match>=0 && bestIndex>match)
			break;
	}
	++bestIndex;
	if(bestIndex==licam.size())
	{
		QMessageBox::warning(this, tr("Copy warning"), tr("All files seem to be on the drive already, check %1!").arg(refdir));
		return;
	}
	int fileCnt=licam.size()-bestIndex;
	if(bestIndex<0)
	{
		QMessageBox::warning(this, tr("Copy warning"), tr("Can't find any camera files anywhere in your base folder. Will copy ALL files from the camera!"));
		fileCnt=licam.size();
		ui->labelInfo->setText(tr("Ready to copy %1 - %2 (%3 files, ALL)").arg(licam.first(), licam.last()).arg(fileCnt));
		filesToCopy=licam;
	}
	else
	{
		ui->labelInfo->setText(tr("Ready to copy %1 - %2 (%3 files, ref %4)").arg(licam.at(bestIndex), licam.last()).arg(fileCnt).arg(refdir));
		for(int ic=bestIndex, icm=licam.size(); ic<icm; ++ic)
			filesToCopy<<licam.at(ic);
	}
	ui->pushCopy->setEnabled(filesToCopy.size()>0);
	ui->pushAdd2Old->setEnabled(filesToCopy.size()>0);
}

void DlgFromCam::on_pushCopy_clicked()
{
	if(!dbase || !dcam)
		return;
	QString targetDirName=QDateTime::currentDateTime().toString(ui->lineDateFormatr->text())+ui->lineTarget->text();
	QString targetDirPath=dbase->absoluteFilePath(targetDirName);
	if(!dbase->mkdir(targetDirName))
	{
		ui->labelInfo_2->setText(tr("FLAIED %1").arg(targetDirPath));
		ui->pushCopy->setEnabled(false);
		ui->pushAdd2Old->setEnabled(false);
		return;
	}
	if(!doCopy(targetDirPath))
		return;
	int baseSrcPk=Source::location2Pk(ui->lineBase->text());
	if(baseSrcPk<=0)
		return;
	Source &pfn=DCON.getSource(baseSrcPk);
	if(!pfn.isValid())
		return;
	Source nsrc(&pfn, nullptr);
	if(!nsrc.setLocation(targetDirPath, true))
		return;
	int newSrcPk=nsrc.getPk();
	DCON.selectSource(newSrcPk);
}

void DlgFromCam::on_pushAdd2Old_clicked()
{
	if(!dbase || !dcam)
		return;
	QString targetDir=dbase->absoluteFilePath(refdir);
	if(!doCopy(targetDir))
		return;
	DCON.selectSource(targetDir);
}

bool DlgFromCam::doCopy(const QString &targetDirPath)
{
	//qDebug()<<filesToCopy;
	QSettings settings;
	settings.setValue(QStringLiteral("fromCam/base"), ui->lineBase->text());
	settings.setValue(QStringLiteral("fromCam/cam"), ui->lineCam->text());
	settings.setValue(QStringLiteral("fromCam/excludeFiles"), ui->lineExcludeFiles->text());
	settings.setValue(QStringLiteral("fromCam/dateFormat"), ui->lineDateFormatr->text());
	ui->progressBar->setValue(0);
	ui->progressBar->setMaximum(filesToCopy.size());
	ui->progressBar->setVisible(true);
	ui->pushCheck->setEnabled(false);
	ui->pushCopy->setEnabled(false);
	ui->pushAdd2Old->setEnabled(false);
	ui->pushClose->setEnabled(false);
	QDir ndir(targetDirPath);
	int goodCopy=0, anyCopy=0;
	QStringList badCopy;
	foreach(QString fn, filesToCopy)
	{
		if(QFile::copy(dcam->absoluteFilePath(fn), ndir.absoluteFilePath(fn)))
			++goodCopy;
		else
			badCopy<<fn;
		ui->progressBar->setValue(++anyCopy);
	}
	ui->progressBar->setVisible(false);
	ui->pushCheck->setEnabled(true);
	ui->pushClose->setEnabled(true);
	ui->labelInfo_2->setText(tr("Copied %1 files.").arg(goodCopy));
	if(badCopy.size())
	{
		QMessageBox::critical(this, tr("Copy ERROR"), tr("There were copy errors for %1! Please check and copy manually.").arg(badCopy.join(' ')));
		ui->pushCopy->setEnabled(false);
		ui->pushAdd2Old->setEnabled(false);
		return false;
	}
	return true;
}


