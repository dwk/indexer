#ifndef DBWRAPPER_H
#define DBWRAPPER_H

#include "idxh.h"
#include <QObject>
#include <QSqlDatabase>

Q_DECLARE_LOGGING_CATEGORY(dbwrapper)

class DbWrapper : public QObject
{
	Q_OBJECT
public:
	explicit DbWrapper(const QString &dbName, QObject *parent = 0);
	~DbWrapper();
	QSqlDatabase &db() {return db_;}
	QString getVersion();
	QStringList getAllTableNames() const;
signals:

public slots:
private:
	bool running=false;
	QSqlDatabase db_;
};

#endif // DBWRAPPER_H
