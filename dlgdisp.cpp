#include "dlgdisp.h"
#include <QFocusEvent>
#include <QTimer>
#include <QScreen>
#include "ui_dlgdisp.h"
#include "idxi.h"

Q_LOGGING_CATEGORY(dlgdisp, "dlgdisp")

DlgDisp::DlgDisp(QWidget *parent) :
	QDialog(parent), ui(new Ui::DlgDisp)
{
	ui->setupUi(this);
}

DlgDisp::~DlgDisp()
{
	delete ui;
}

void DlgDisp::setPixmap(const QPixmap &pixmap, bool scale2Size, DisplayPurpose purpose)
{
	if(p!=purpose)
		emit purposeChangedSig(p, purpose);
	p=purpose;
	if(scale2Size)
	{
		QSize sz=ui->scrollArea->size();
		const int border=12;
		int saw=sz.width()-border;
		int sah=sz.height()-border;
		if(pixmap.width()>saw || pixmap.height()>sah)
		{
			QPixmap scpx;
			if(pixmap.width()/saw > pixmap.height()/sah)
				scpx=pixmap.scaledToWidth(saw);
			else
				scpx=pixmap.scaledToHeight(sah);
			ui->label->setPixmap(scpx);
		}
		else
			ui->label->setPixmap(pixmap);
	}
	else
		ui->label->setPixmap(pixmap);
}

void DlgDisp::accept()
{
	QDialog::accept();
}

void DlgDisp::hideEvent(QHideEvent *event)
{
	if(!fullScreenMagic)
		emit purposeChangedSig(p, DisplayPurpose::UNDEF);
	QDialog::hideEvent(event);
}

void DlgDisp::mouseDoubleClickEvent(QMouseEvent *event)
{
	if(event->button()==Qt::LeftButton)
	{
		if(fullScreen)
			stopFullScreen();
		else
			startFullScreen();
		event->accept();
	}
	QDialog::mouseDoubleClickEvent(event);
}

void DlgDisp::keyPressEvent(QKeyEvent *e)
{
	//qDebug()<<"DlgDisp::keyPressEvent"<<e->key();
	if(e->key()==Qt::Key_Escape && fullScreen)
	{
		e->accept();
		stopFullScreen();
		// don't call base class because ESC would hide dialog
	}
	else
		QDialog::keyPressEvent(e);
}

void DlgDisp::startFullScreen()
{
	//QTimer::singleShot(2000, this, &DlgDisp::stopFullScreen);
	//qDebug()<<"DlgDisp::on_pushFullScreen_clicked"<<qGuiApp->primaryScreen()->availableSize();
	oldPos=pos();
	oldSize=size();
	fullScreenMagic=true;
	setWindowFlags(windowFlags() | Qt::FramelessWindowHint);
	move(0,0);
	resize(qGuiApp->primaryScreen()->availableSize());
	show();
	fullScreenMagic=false;
	fullScreen=true;
}

void DlgDisp::stopFullScreen()
{
	fullScreenMagic=true;
	resize(oldSize);
	move(oldPos);
	setWindowFlags(windowFlags() & ~Qt::FramelessWindowHint);
	show();
	fullScreenMagic=false;
	fullScreen=false;
}

