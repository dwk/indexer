#include "dwidimages.h"
#include "dwidimages_fac.h"
#include "ui_dwidimages.h"
#include "sqlobject.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRelationalDelegate>
#include <QKeyEvent>
#include <QMenu>
#include <QProcess>
#include "dlgeditdesc.h"
#include "dlgimageedit.h"
#include "idxi.h"

Q_LOGGING_CATEGORY(dwidimages, "dwidimages")

DWidImages::DWidImages(QWidget *parent) :
	QWidget(parent),
	ui(new ::Ui::DWidImages)
{
	setObjectName(techname);
	ui->setupUi(this);

	iModel=new ImageQueryModel(this, DCON.db()->db());
	connect(iModel, SIGNAL(modelAboutToBeReset()), this, SLOT(modelPreResetSl()));
	connect(iModel, SIGNAL(modelReset()), this, SLOT(modelResetSl()));
	connect(iModel, &ImageQueryModel::imageActionGSig, this, &DWidImages::imageActionGSig);
	ui->tableView->setModel(iModel);
	ui->tableView->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(ui->tableView, SIGNAL(customContextMenuRequested(QPoint)),
				SLOT(customMenuRequestedSl(QPoint)));
	connect(ui->tableView->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)),
		this, SLOT(tableSelChangedSl(const QItemSelection &, const QItemSelection &)));
	connect(ui->tableView->verticalHeader(), &QHeaderView::sectionResized, this, &DWidImages::rowResizedSl);
	connect(ui->tableView->horizontalHeader(), &QHeaderView::sectionResized, this, &DWidImages::columnResizedSl);
}

DWidImages::~DWidImages()
{
	delete ui;
}

void DWidImages::connectAll(ObjectMSP &otherActors)
{
	qDebug()<<"connectAll"<<objectName();
	for(auto it=otherActors.begin(); it!=otherActors.end(); ++it)
	{
		QObject *obj=it->second;
		if(obj->objectName()==objectName())
			continue;
		const QMetaObject* mo = obj->metaObject();
		if(mo->indexOfSignal("sourceSelectedSig(int)")>=0)
		{
			qDebug()<<"    "<<obj->objectName()<<"sourceSelectedSig(int)";
			connect(obj, SIGNAL(sourceSelectedSig(int)), this, SLOT(sourceSelectedSl(int)));
		}
		if(mo->indexOfSignal("querySelectedSig(void*)")>=0)
		{
			qDebug()<<"    "<<obj->objectName()<<"querySelectedSig(void*)";
			connect(obj, SIGNAL(querySelectedSig(void*)), this, SLOT(querySelectedSl(void*)));
		}
		if(mo->indexOfSignal("imageActionGSig(int,ImageAction)")>=0)
		{
			qDebug()<<"    "<<obj->objectName()<<"imageActionGSig(int,ImageAction)";
			connect(obj, SIGNAL(imageActionGSig(int,ImageAction)), this, SLOT(imageActionGSl(int,ImageAction)));
		}
	}
}

void DWidImages::setupSl()
{
	//we will do that during model selection / query
}

void DWidImages::nextImageSl()
{
	QModelIndexList li=ui->tableView->selectionModel()->selectedRows();
	if(li.size())
	{
		int cr=li.last().row();
		qCDebug(dwidimages)<<"selecting next image"<<cr<<iModel->rowCount();
		if(cr<iModel->rowCount()-1)
		{
			QModelIndex idx=iModel->index(cr+1, 0);
			ui->tableView->selectionModel()->select(idx, QItemSelectionModel::Select | QItemSelectionModel::Current |QItemSelectionModel::Rows);
			ui->tableView->scrollTo(idx);
		}
	}
}

void DWidImages::prevImageSl()
{
	QModelIndexList li=ui->tableView->selectionModel()->selectedRows();
	if(li.size())
	{
		int cr=li.first().row();
		qCDebug(dwidimages)<<"selecting prev image"<<cr<<iModel->rowCount();
		if(cr>0)
		{
			QModelIndex idx=iModel->index(cr-1, 0);
			ui->tableView->selectionModel()->select(idx, QItemSelectionModel::Select | QItemSelectionModel::Current | QItemSelectionModel::Rows);
			ui->tableView->scrollTo(idx);
		}
	}
}

void DWidImages::editDescriptionSl()
{
	QModelIndexList li=ui->tableView->selectionModel()->selectedRows();
	if(li.size())
	{
		auto mi=li.begin();
		QString text=iModel->data(iModel->index((*mi).row(), 1)).toString();
		bool identical=true;
		for(++mi; identical && mi!=li.end(); ++mi)
		{
			if(iModel->data(iModel->index((*mi).row(), 1))!=text)
				identical=false;
		}
		if(!identical)
			text.clear();
		DlgEditDesc dlg(text, li.size(), identical, this);
		if(dlg.exec()==QDialog::Accepted)
		{
			text=dlg.getText();
			bool append=dlg.append();
			qCDebug(dwidimages)<<"editDescriptionSl"<<text<<append;
			foreach(QModelIndex idx, li)
			{
				qCDebug(dwidimages)<<"editDescriptionSl row"<<idx.row();
				if(append)
					iModel->setData(iModel->index(idx.row(), 1), iModel->data(iModel->index(idx.row(), 1)).toString()+text);
				else
					iModel->setData(iModel->index(idx.row(), 1), text);
			}
		}
	}
	else
		QApplication::beep();
}

void DWidImages::editImageSl()
{
	QModelIndexList li=ui->tableView->selectionModel()->selectedRows();
	if(li.size()==1)
	{
		auto mi=li.begin();
		DlgImageEdit dlg(iModel->row2ImagePk((*mi).row()), this);
		if(dlg.exec()==QDialog::Accepted)
		{ // Image was already updated in DlgImageEdit::accept()
			iModel->reloadData(*mi);
		}
	}
	else
		QApplication::beep();
}

void DWidImages::deleteImageSl()
{
	QModelIndexList li=ui->tableView->selectionModel()->selectedRows();
	if(li.size()==1)
	{
		auto mi=li.begin();
		int imagePk=iModel->row2ImagePk((*mi).row());
		if(imagePk>0)
		{
			emit imageActionGSig(imagePk, ImageAction::DELETE);
		}
	}
	else
		QApplication::beep();
}

void DWidImages::openGimpSl()
{
	QModelIndexList li=ui->tableView->selectionModel()->selectedRows();
	if(li.size()==1)
	{
		auto mi=li.begin();
		Image & img=DCON.getImage(iModel->row2ImagePk((*mi).row()));
		Source & src=DCON.getSource(img.getSourcePk());
		QStringList params;
		params<<src.getFullUrl(img.getName());
		QProcess p;
		qInfo()<<"executing gimp"<<params;
		p.startDetached(DCON.getCmdGimp(), params);
		p.waitForStarted();
	}
	else
		QApplication::beep();
}

void DWidImages::checkForUnsavedChanges()
{
	if(iModel->isDirty() && QMessageBox::question(this, tr("Unsaved Changes (Images)"), tr("There are pending, unsaved chnages. Commit these before (reloading)?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes)==QMessageBox::Yes)
	{
		on_pushSubmit_clicked();
	}
}

void DWidImages::applyTableGeom()
{
	QSettings settings;
	ui->tableView->verticalHeader()->setDefaultSectionSize(settings.value("pref/imagetab/rowheight", 25).toInt());
	auto hh=ui->tableView->horizontalHeader();
	QString n("pref/imagetab/colwidth%1");
	for(int i=0; i<iModel->columnCount(); ++i)
	{
		hh->resizeSection(i, settings.value(n.arg(i), 150).toInt());
	}
	if(iModel->rowCount())
	{
		qCDebug(dwidimages)<<"selecting first image";
		ui->tableView->selectionModel()->select(iModel->index(0, 0), QItemSelectionModel::Select | QItemSelectionModel::Rows);
	}
	else
		emit imageActionGSig(-1, ImageAction::SELECT);
}

void DWidImages::customMenuRequestedSl(QPoint pos)
{
	QModelIndex idx=ui->tableView->indexAt(pos);
	QString srcStr=ui->tableView->model()->data(ui->tableView->model()->index(idx.row(), 8)).toString();
	if(srcStr.isEmpty())
	{
		QApplication::beep();
		return;
	}
	QMenu menu(this);
	//QAction *act=
	menu.addAction(tr("switch to %1").arg(srcStr), [this, srcStr]() {switchToSourceSl(srcStr);});
	//act->setEnabled;
	menu.exec(ui->tableView->mapToGlobal(pos));
}

void DWidImages::switchToSourceSl(const QString & source)
{
	qCDebug(dwidimages)<<"switchToSourceSl"<<source;
	int spk=Source::location2Pk(source);
	sourceSelectedSl(spk);
	emit sourceSelectedSig(spk);
}

void DWidImages::sourceSelectedSl(int pk)
{
	checkForUnsavedChanges();
	iModel->setQuery(pk);
	// would cache all rows - performance killer
	//ui->tableView->resizeColumnsToContents();
	//ui->tableView->resizeRowsToContents();
	applyTableGeom();
}

void DWidImages::querySelectedSl(void* so)
{
	SqlObject *sqlObj=reinterpret_cast<SqlObject *>(so);
	checkForUnsavedChanges();
	iModel->setQuery(sqlObj);
	applyTableGeom();
}

void DWidImages::imageActionGSl(int imagePk, ImageAction action)
{
	switch(action)
	{
	case ImageAction::SELECT:
	{
		supressSelectionSgnal=true;
		ui->tableView->selectionModel()->clearSelection();
		if(imagePk<0)
		{
			supressSelectionSgnal=false;
			return;
		}
		int row=iModel->imagePk2Row(imagePk);
		if(row<0)
		{
			qWarning()<<"DWidImages::imageActionGSl pk not available"<<imagePk;
			supressSelectionSgnal=false;
			return;
		}
		QModelIndex idx=iModel->index(row, 0);
		ui->tableView->selectionModel()->select(idx, QItemSelectionModel::Select | QItemSelectionModel::Current | QItemSelectionModel::Rows);
		ui->tableView->scrollTo(idx);
		supressSelectionSgnal=false;
		break;
	} // SELECT
	case ImageAction::SELECT_MULTI:
	{
		supressSelectionSgnal=true;
		ui->tableView->selectionModel()->clearSelection();
		const QList<int> &pks=DCON.getSelectedImages();
		foreach(int pk, pks)
		{
			QModelIndex idx=iModel->index(iModel->imagePk2Row(pk), 0);
			if(idx.isValid())
				ui->tableView->selectionModel()->select(idx, QItemSelectionModel::Select | QItemSelectionModel::Current | QItemSelectionModel::Rows);
		}
		if(pks.size())
		{
			QModelIndex idx=iModel->index(iModel->imagePk2Row(pks.at(0)), 0);
			if(idx.isValid())
				ui->tableView->scrollTo(idx);
		}
		supressSelectionSgnal=false;
		break;
	} // SELECT_MULTI
	case ImageAction::MARK:
	case ImageAction::UNMARK:
	case ImageAction::MARK_MULTI:
	case ImageAction::DELETE: // no check for query size  since DB records need a re-merge to change
	case ImageAction::SAVE_AS:
		break;
	case ImageAction::LABELS_CHANGED:
	case ImageAction::ATTRIB_CHANGED:
		iModel->checkSize(); // check if search results were changed
		break;
	case ImageAction::PIXEL_CHANGED:
		break;
	}
}

void DWidImages::modelPreResetSl()
{
	QModelIndexList li=ui->tableView->selectionModel()->selectedRows();
	if(li.size()==1)
		selectedImagePk=iModel->row2ImagePk(li.at(0).row());
	else
		selectedImagePk=-1;
}

void DWidImages::modelResetSl()
{
	qCDebug(dwidimages)<<"modelResetSl";
	bool tryReselect=true;
	ui->label->setToolTip(QStringLiteral(""));
	switch(iModel->getMode())
	{
	case ImageQueryModel::Mode::EMPTY:
	{
		QPalette pal=ui->label->palette();
		ui->label->setText(tr("no source"));
		pal.setColor(QPalette::WindowText, Qt::black);
		ui->label->setPalette(pal);
		ui->pushSubmit->setDisabled(true);
		ui->pushMerge->setDisabled(true);
		ui->pushReload->setDisabled(true);
		tryReselect=false;
		break;
	}
	case ImageQueryModel::Mode::SOURCE:
	{
		QPalette pal=ui->label->palette();
		Source & src=DCON.getSource(iModel->getSourcePk());
		if(src.isValid())
		{
			if(src.size()!=iModel->rowCount())
			{
				ui->label->setText(QString("%1/%2 %3").arg(src.size()).arg(iModel->rowCount()).arg(src.location()));
				pal.setColor(QPalette::WindowText, Qt::darkRed);
			}
			else
			{
				QString sl=src.location();
				if(sl.size()<32)
					ui->label->setText(sl);
				else
				{
					int in=sl.lastIndexOf('/');
					if(in<0 || sl.size()-in>31)
						ui->label->setText(QString("...%1").arg(sl.mid(sl.size()-31)));
					else
						ui->label->setText(QString("...%1").arg(sl.mid(in+1)));
					ui->label->setToolTip(sl);
				}
				pal.setColor(QPalette::WindowText, Qt::black);
			}
			ui->pushSubmit->setEnabled(iModel->isDirty());
			ui->pushMerge->setEnabled(true);
			ui->pushReload->setEnabled(true);
		}
		else
		{
			ui->label->setText(tr("invalid source (%1)").arg(iModel->getSourcePk()));
			ui->pushSubmit->setDisabled(true);
			ui->pushMerge->setDisabled(true);
			ui->pushReload->setDisabled(true);
			pal.setColor(QPalette::WindowText, Qt::darkRed);
		}
		ui->label->setPalette(pal);
		break;
	}
	case ImageQueryModel::Mode::SEARCH:
	{
		QPalette pal=ui->label->palette();
		pal.setColor(QPalette::WindowText, Qt::black);
		ui->label->setPalette(pal);
		ui->label->setText(tr("%1 search result").arg(iModel->rowCount()));
		ui->pushSubmit->setDisabled(true);
		ui->pushMerge->setDisabled(true);
		ui->pushReload->setEnabled(true);
		break;
	}
	}
	if(selectedImagePk>0 && tryReselect)
	{ // nice try but this will usually not work - the model has nothing cached yet, so it does not know at which row the pk will show up
		int sr=iModel->imagePk2Row(selectedImagePk);
		if(sr>=0)
		{
			auto idx=iModel->index(sr, 0);
			ui->tableView->scrollTo(idx);
			ui->tableView->selectionModel()->select(idx, QItemSelectionModel::Select | QItemSelectionModel::Current |QItemSelectionModel::Rows);
		}
	}
}

void DWidImages::rowResizedSl(int logicalIndex, int oldSize, int newSize)
{
	Q_UNUSED(logicalIndex)
	Q_UNUSED(oldSize)
	qCDebug(dwidimages)<<"rowResizedSl"<<oldSize<<newSize;
	QSettings settings;
	settings.setValue("pref/imagetab/rowheight", newSize);
}

void DWidImages::columnResizedSl(int logicalIndex, int oldSize, int newSize)
{
	Q_UNUSED(oldSize)
	qCDebug(dwidimages)<<"columnResizedSl"<<logicalIndex<<oldSize<<newSize;
	QSettings settings;
	settings.setValue(QString("pref/imagetab/colwidth%1").arg(logicalIndex), newSize);
}

void DWidImages::tableSelChangedSl(const QItemSelection &selected, const QItemSelection &deselected)
{
	qCDebug(dwidimages)<<"tableSelChangedSl"<<selected<<deselected;
	if(selected==lastSelected && deselected==lastDeselected)
	{ // signal is emitted twice which creates uneccessary image (re)loads
		qCDebug(dwidimages)<<"tableSelChangedSl skipped due to idetical params";
		return;
	}
	lastSelected=selected;
	lastDeselected=deselected;
	if(supressSelectionSgnal)
		return;
	QModelIndexList li=ui->tableView->selectionModel()->selectedRows();
	QList<int> pks;
	foreach(QModelIndex index, li)
	{
		pks<<iModel->row2ImagePk(index.row());
	}
	qCDebug(dwidimages)<<"Images selection"<<pks;
	if(!pks.size())
		emit imageActionGSig(-1, ImageAction::SELECT);
	else if(pks.size()==1)
		emit imageActionGSig(pks.at(0), ImageAction::SELECT);
	else
	{
		DCON.imageMultiSelection(pks); // make sure DCON is up to date before emitting the signal
		emit imageActionGSig(-1, ImageAction::SELECT_MULTI);
	}
}

void DWidImages::on_pushSubmit_clicked()
{
	if(!iModel->isDirty())
	{
		ui->pushSubmit->setEnabled(false);
		return;
	}
	QModelIndexList li=ui->tableView->selectionModel()->selectedRows();
	int imgPk=-1;
	if(li.size())
	{
		imgPk=iModel->row2ImagePk(li.first().row());
		qCDebug(dwidimages)<<"on_pushSubmit_clicked selected"<<imgPk;
	}
}

void DWidImages::on_pushMerge_clicked()
{
	int srcPk=iModel->getSourcePk();
	if(srcPk<=0)
	{
		qWarning()<<"on_pushMerge_clicked only valid for SOURCE";
		ui->pushMerge->setEnabled(false);
		return;
	}
	DCON.dropSourceCache(srcPk); // maybe the user changed the source right now and wants to see the update
	Source & src=DCON.getSource(srcPk);
	QStringList missingInDb;
	std::vector<int> missingInSource;
	src.imageMergeLists(missingInDb, missingInSource);
	if(missingInSource.size())
	{
		int qres=QMessageBox::question(this, tr("Missing Images"),
			tr("There are %1 images missing in the source. If you proceed all information related to these images will be DELETED from the database. Do you prefer to confirm for each image separately? (Cancel will halt the merge process without changing the database.)").arg(missingInSource.size()),
			QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel, QMessageBox::Cancel);
		if(qres==QMessageBox::Cancel)
			return;
		for(auto pkIt=missingInSource.begin(); pkIt!=missingInSource.end(); ++pkIt)
		{
			if(qres==QMessageBox::Yes)
			{
				Image &img=DCON.getImage(*pkIt);
				int qqres=QMessageBox::question(this, tr("Deleting Image Information"),
						tr("Delete %1 from database?").arg(img.isValid()?img.getName():"<invalid>"),
						QMessageBox::Yes | QMessageBox::No, QMessageBox::No);
				if(qqres==QMessageBox::No)
					continue;
			}
			Image::deleteComplete(*pkIt, DCON.db()->db(), this);
			//qDebug()<<"Images merge: remove image"<<*pkIt;
		}
	}
	foreach(QString fn, missingInDb)
	{
		Image &img=DCON.getImage(srcPk, fn);
		if(!img.commit2Db())
			qWarning()<<"Image merge (commit) FAILED: no image or already in db"<<fn<<img.getPk()<<srcPk;
	}
	on_pushReload_clicked();
}

void DWidImages::on_pushReload_clicked()
{
	iModel->reload();
	applyTableGeom();
}
