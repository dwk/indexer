#include "dlgedittag.h"
#include <QTreeWidgetItem>
#include <QColorDialog>
#include "idxi.h"
#include "ui_dlgedittag.h"

Q_LOGGING_CATEGORY(dlgedittag, "dlgedittag")

DlgEditTag::DlgEditTag(QTreeWidgetItem * twi, QWidget *parent) :
	QDialog(parent), ui(new Ui::DlgEditTag), twi(twi)
{
	ui->setupUi(this);
	ui->lineVal->setText(twi->data(0, Qt::DisplayRole).toString());
	ui->lineDesc->setText(twi->data(1, Qt::DisplayRole).toString());
	QString deco=twi->data(0, Qt::UserRole+1).toString();
	QStringList decos=deco.split(';', Qt::SkipEmptyParts);
	ui->comboShape->setCurrentIndex(0);
	foreach(QString dc, decos)
	{// background: #c0ffc0; color: #004000; shape: rounded-box
		if(dc.contains("shape:"))
		{
			if(dc.contains("rounded-box")) // must precede "box"!!!
				ui->comboShape->setCurrentIndex(2);
			else if(dc.contains("box"))
				ui->comboShape->setCurrentIndex(1);
			else if(dc.contains("diamond"))
				ui->comboShape->setCurrentIndex(3);
		}
		else if(dc.contains("background-color:")) // must precede "color"!!!
		{
			int start=dc.indexOf('#');
			if(start>0)
				colorBackground.setNamedColor(dc.mid(start, 7));
		}
		else if(dc.contains("color:"))
		{
			int start=dc.indexOf('#');
			if(start>0)
				colorText.setNamedColor(dc.mid(start, 7));
		}
	}
	colorSetup();
}

DlgEditTag::~DlgEditTag()
{
	delete ui;
}

void DlgEditTag::accept()
{
	//qDebug()<<"DlgEditTag::accept";
	if(ui->lineVal->text().contains('\'') || ui->lineDesc->text().contains('\'') ||
		ui->lineVal->text().contains('%') || ui->lineDesc->text().contains('%'))
	{
		QMessageBox::warning(this, tr("Invalid Characters"), tr("Neither name nor description may contain apostrophes (') or percent signs (%)."));
		return;
	}
	twi->setData(0, Qt::DisplayRole, ui->lineVal->text());
	twi->setData(1, Qt::DisplayRole, ui->lineDesc->text());
	QStringList decos;
	if(ui->comboShape->currentIndex())
	{
		switch(ui->comboShape->currentIndex())
		{
		case 1:
			decos<<QStringLiteral("shape: box");
			break;
		case 2:
			decos<<QStringLiteral("shape: rounded-box");
			break;
		case 3:
			decos<<QStringLiteral("shape: diamond");
			break;
		default:
			break;
		}
	}
	if(colorBackground.isValid())
		decos<<QStringLiteral("background-color: %1").arg(colorBackground.name());
	if(colorText.isValid())
		decos<<QStringLiteral("color: %1").arg(colorText.name());
	twi->setData(0, Qt::UserRole+1, decos.join("; "));
	QDialog::accept();
}

void DlgEditTag::colorSetup()
{
	ui->pushColorReset->setEnabled(colorText.isValid() || colorBackground.isValid());
	QStringList css;
	if(colorText.isValid())
		css<<QStringLiteral("color:%1").arg(colorText.name());
	if(colorBackground.isValid())
		css<<QStringLiteral("background-color:%1").arg(colorBackground.name());
	ui->labelColorTest->setStyleSheet(css.join(';'));
}

void DlgEditTag::on_pushColorText_clicked()
{
	QColor newcol=QColorDialog::getColor(colorText, this, tr("Text and Frame Color"), QColorDialog::DontUseNativeDialog);
	if(newcol.isValid())
	{
		colorText=newcol;
		colorSetup();
	}
}

void DlgEditTag::on_pushColorBackground_clicked()
{
	QColor newcol=QColorDialog::getColor(colorBackground, this, tr("Background Color"), QColorDialog::DontUseNativeDialog);
	if(newcol.isValid())
	{
		colorBackground=newcol;
		colorSetup();
	}
}

void DlgEditTag::on_pushColorReset_clicked()
{
	colorText=QColor();
	colorBackground=QColor();
	colorSetup();
}

