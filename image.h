#ifndef IMAGE_H
#define IMAGE_H

#include "idxh.h"
#include <QObject>
#include <QPointer>
#include <QDir>
#include <QPixmap>
#include <QDateTime>
#include <QSqlQuery>
// http://techawarey.com/programming/install-opencv-c-c-in-ubuntu-18-04-lts-step-by-step-guide/
#include <opencv2/opencv.hpp> // https://docs.opencv.org/master/d7/d9f/tutorial_linux_install.html /usr/local/include/opencv4
#include "gpscoord.h"

Q_DECLARE_LOGGING_CATEGORY(image)

class QSqlRecord;
// An Image represents parameters and pixel content. For loading pixel data a Source is needed.
// is the front end of the DB-object image.
class Image : public QObject
{
	Q_OBJECT
public:
	enum class ExifOrientation {UP=1, MIRROR_UP=2, DOWN=3, MIRROR_DOWN=4, MIRROR_LEFT=5, LEFT=6, MIRROR_RIGHT=7, RIGHT=8};
	enum class ImageAttr {FILESIZE=1, DESCRIPTION, CAMERA, CAMERA_SETTING, CREATE_DATE_FILE, CREATE_DATE_EXIF, GPS, EXIF_ORIENTATION};
	typedef std::map< Image::ImageAttr, QVariant> ImageAttrs;
	typedef std::map< int, QPixmap * > PixmapMP;
	static QString orientationStr(ExifOrientation ori);
	static bool deleteComplete(int imgPk, QSqlDatabase & db, QWidget * messageParent); // will delete img and drop it from cache ; return false -> still exists
	explicit Image(QObject *parent = 0) : QObject(parent), pingTst(QDateTime::currentMSecsSinceEpoch()) {}
	explicit Image(int imagePk, QObject *parent = 0);
	explicit Image(int sourcePk, const QString &imgeName, QObject *parent = 0);
	~Image();
	bool commit2Db();
	void ping() {pingTst=QDateTime::currentMSecsSinceEpoch();}
	qint64 getPing() const {return pingTst;}
	bool isValid() const {return ok;}
	int getPk() const {return pk;}
	int getSourcePk() const {return sourcePk;}
	std::vector<int> getTagPks() const;
	bool setTagPks(std::vector<int> tagPks);
	QString getInfo(); // will load (and) the fullsize pixmap!
	QString getName() const {return imageName;}
	int getFileSize() const {return fileSize;}
	QString getDesc() const {return desc;}
	bool setDesc(const QString & d, bool autoCommit);
	QString getCam() const {return cam;}
	QString getCamSettings() const {return camSettings;}
	QDateTime getCreateFile() const {return createFile;}
	QDateTime getCreateExif() const {return createExif;}
	const GpsCoord & getCoord() const {return coord;}
	bool setCoord(const GpsCoord & c, bool autoCommit);
	ExifOrientation getOrientation() const {return ori;}
	bool setOrientation(ExifOrientation o, bool autoCommit);
	QPixmap *pixels(int scale2Width = 0); // no scaling if scale2Width==0
	QPixmap process();
	static int cachePxSize; // avoid frquent DCON access
private:
	int pk=-1, sourcePk=-1, fileSize=0;
	bool ok=false, dbDirty=false, pixDirty=false;
	qint64 pingTst;
	QString imageName, desc, cam, camSettings;
	QDateTime createFile, createExif;
	GpsCoord coord;
	ExifOrientation ori=ExifOrientation::UP;
	PixmapMP pixmapMP;
signals:
	void pixChangedSig(int imagePk);
};
typedef std::map< int , QPointer<Image> > ImageMVQ;

#endif // IMAGE_H
