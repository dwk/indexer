BEGIN TRANSACTION;
INSERT INTO `tags` VALUES (10,NULL,10,'Menschen',NULL,'background: #c0c0ff; color: #000060; shape: box');
INSERT INTO `tags` VALUES (20,NULL,20,'Szene','weiter Blick, Panorama (Natur, Stadt, ...)','background: #c0ffc0; color: #004000; shape: rounded-box');
INSERT INTO `tags` VALUES (30,NULL,30,'Tiere',NULL,'background: #aaaa00; color: #000000; shape: rounded-box');
INSERT INTO `tags` VALUES (40,NULL,40,'Pflanzen','einzelne Pflanze (Blume, Baum, Moss, ...)','background: #80ff80; color: #009000; shape: diamond');
INSERT INTO `tags` VALUES (50,NULL,50,'Technik',NULL,NULL);
INSERT INTO `tags` VALUES (60,NULL,60,'Spezial','Tags für besondere Motive',NULL);
INSERT INTO `tags` VALUES (100,NULL,100,'Arbeit',NULL,'background: #ff8844');

INSERT INTO `tags` VALUES (1000,10,1,'Familie','jeder im Stammbaum','background: #c0c0ff; color: #000060; shape: box');
INSERT INTO `tags` VALUES (1001,10,2,'Bekannte','Freunde, Arbeitskollegen - Menschen mit Hintergrundsinfo','background: #c0c0ff; color: #000060; shape: box');
INSERT INTO `tags` VALUES (1002,10,3,'anyone','unbekannte Menschen','background: #c0c0ff; color: #000060; shape: box');

INSERT INTO `tags` VALUES (1010,20,1,'Natur',NULL,'background: #c0ffc0; color: #004000; shape: rounded-box');
INSERT INTO `tags` VALUES (1011,20,2,'Architektur',NULL,'background: #c0ffc0; color: #004000; shape: rounded-box');
INSERT INTO `tags` VALUES (1012,20,3,'Innenraum',NULL,'background: #c0ffc0; color: #004000; shape: rounded-box');

INSERT INTO `tags` VALUES (1020,30,1,'Unsere Tiere','Haustiere der Familie Kieslinger','background: #aaaa00; color: #000000; shape: rounded-box');
INSERT INTO `tags` VALUES (1021,30,2,'Nutztiere','was man so am Bauernhof und dergleichen findet','background: #aaaa00; color: #000000; shape: rounded-box');
INSERT INTO `tags` VALUES (1022,30,3,'Wildtiere','was in freier Wildbahn herumläuft','background: #aaaa00; color: #000000; shape: rounded-box');
INSERT INTO `tags` VALUES (1023,30,4,'Tierisches','Skurrilitäten','background: #aaaa00; color: #000000; shape: rounded-box');

INSERT INTO `tags` VALUES (1040,50,1,'Haus','Bahnstrasse 25 oder etwas mit Bezug dazu','background: rgb(200,200,200); shape: box');
INSERT INTO `tags` VALUES (1041,50,2,'Elektro','Elektrik / Elektronik','background: rgb(200,200,200); shape: box');
INSERT INTO `tags` VALUES (1042,50,3,'Mechanik','da sind Objekte wichtig','background: rgb(200,200,200); shape: box');
INSERT INTO `tags` VALUES (1043,50,4,'es fließt','Flüssigkeiten, Gase, auch in Rohren und Behältern','background: rgb(200,200,200); shape: box');

INSERT INTO `tags` VALUES (1050,60,1,'AEBAES','Auf einem Bein auf einem Stein steht der Löwe gany allein',NULL);
INSERT INTO `tags` VALUES (1051,60,2,'B³','blöd, blöder, am blödesten',NULL);
INSERT INTO `tags` VALUES (1052,60,3,'Hintergrund','für Bildschrimhintergründe gedacht',NULL);
INSERT INTO `tags` VALUES (1053,60,4,'HQ','ein Bild wie es sein soll',NULL);
INSERT INTO `tags` VALUES (1054,60,5,'PVX','Privates - nur für die Augen der Familie',NULL);

INSERT INTO `tags` VALUES (1060,100,1,'ZKW',NULL,NULL);
INSERT INTO `tags` VALUES (1061,100,2,'vor ZKW',NULL,NULL);


INSERT INTO `tags` VALUES (5000,1000,1,'Olga',NULL,'background: #c0c0ff; color: #000060; shape: box');
INSERT INTO `tags` VALUES (5001,1000,2,'Dietmar',NULL,'background: #c0c0ff; color: #000060; shape: box');
INSERT INTO `tags` VALUES (5002,1000,3,'Xenia',NULL,'background: #c0c0ff; color: #000060; shape: box');
INSERT INTO `tags` VALUES (5003,1000,6,'Walter',NULL,'background: #c0c0ff; color: #000060; shape: box');
INSERT INTO `tags` VALUES (5004,1000,7,'Hildegard',NULL,'background: #c0c0ff; color: #000060; shape: box');
INSERT INTO `tags` VALUES (5005,1000,4,'Erich',NULL,'background: #c0c0ff; color: #000060; shape: box');
INSERT INTO `tags` VALUES (5006,1000,5,'Isabella',NULL,'background: #c0c0ff; color: #000060; shape: box');
INSERT INTO `tags` VALUES (5007,1000,8,'Buinitsi''s',NULL,'background: #c0c0ff; color: #000060; shape: box');
INSERT INTO `tags` VALUES (5008,1000,9,'Vorfahren','ältere Vorfahren','background: #c0c0ff; color: #000060; shape: box');

INSERT INTO `tags` VALUES (5010,1020,1,'Kaschara',NULL,NULL);
INSERT INTO `tags` VALUES (5011,1020,2,'Mokka',NULL,NULL);
INSERT INTO `tags` VALUES (5012,1020,3,'Fische',NULL,NULL);

INSERT INTO `tags` VALUES (5020,1042,1,'Fahrzeug',NULL,'background: rgb(200,200,200); shape: box');
COMMIT;
