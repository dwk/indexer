#include "image.h"
#include <QSqlError>
#include <QSqlRecord>
#include "idxi.h"

Q_LOGGING_CATEGORY(image, "image")

//using namespace cv;

int Image::cachePxSize=3;

QString Image::orientationStr(Image::ExifOrientation ori)
{
	switch(ori)
	{
	case ExifOrientation::UP: return QStringLiteral("up");
	case ExifOrientation::MIRROR_UP: return QStringLiteral("(up)");
	case ExifOrientation::DOWN: return QStringLiteral("down");
	case ExifOrientation::MIRROR_DOWN: return QStringLiteral("(down)");
	case ExifOrientation::MIRROR_LEFT: return QStringLiteral("(left)");
	case ExifOrientation::LEFT: return QStringLiteral("left");
	case ExifOrientation::MIRROR_RIGHT: return QStringLiteral("(right)");
	case ExifOrientation::RIGHT: return QStringLiteral("right");
	}
	return QString("(%1)").arg((int)ori);
}

bool Image::deleteComplete(int imgPk, QSqlDatabase &db, QWidget *messageParent)
{
	Q_UNUSED(messageParent)
	QSqlQuery q(QStringLiteral("delete from images where pk=%1").arg(imgPk), db);
	if(q.lastError().isValid())
	{
		qCritical()<<"image delete FAILED"<<imgPk<<q.lastError().text()<<q.lastQuery();
		return false;
	}
	DCON.dropImageCache(imgPk);
	return true;
}

Image::Image(int imagePk, QObject *parent) : QObject(parent), pk(imagePk), pingTst(QDateTime::currentMSecsSinceEpoch())
{
	setObjectName(QString("Image(%1)").arg(pk));
	/* 	`pk`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`source`	INTEGER NOT NULL,
	`val`	TEXT NOT NULL,
	`description`	TEXT,
	`size`	INTEGER NOT NULL,
	`date_file`	TEXT NOT NULL,
	`date_exif`	TEXT,
	`camera`	TEXT,
	`longitude`	REAL,
	`latitude`	REAL,
	`height`	INTEGER,
	`camera_settings`	TEXT,
	orientation INTEGER
								0       1    2            3     4          5          6       7          8         9       10               11  */
	QSqlQuery q(QString("select source, val, description, size, date_file, date_exif, camera, longitude, latitude, height, camera_settings, orientation from images where pk=%1").arg(pk));
	if(q.next())
	{
		bool convok=false;
		sourcePk=q.value(0).toInt(&convok);
		imageName=q.value(1).toString();
		desc=q.value(2).toString();
		fileSize=q.value(3).toInt();
		createFile=q.value(4).toDateTime();
		createExif=q.value(5).toDateTime();
		cam=q.value(6).toString();
		coord=GpsCoord(q.value(7), q.value(8), q.value(9));
		camSettings=q.value(10).toString();
		ori=(ExifOrientation)q.value(11).toInt();
		ok=convok;
		if(!convok)
			qWarning()<<objectName()<<"INVALID source"<<q.value(0);
	}
	else
	{
		qWarning()<<objectName()<<"NO db record";
	}
}

Image::Image(int sourcePk, const QString &imgeName, QObject *parent) :
	QObject(parent), pk(-1), sourcePk(sourcePk), pingTst(QDateTime::currentMSecsSinceEpoch()), imageName(imgeName)
{
	//                          0   1            2     3          4          5       6          7         8       9                10
	QSqlQuery q(QString("select pk, description, size, date_file, date_exif, camera, longitude, latitude, height, camera_settings, orientation from images where source=%1 and val=%2").arg(sourcePk).arg(imageName));
	if(q.next())
	{
		bool convOk=false;
		pk=q.value(0).toInt(&convOk);
		if(convOk)
		{
			desc=q.value(1).toString();
			fileSize=q.value(2).toInt();
			createFile=q.value(3).toDateTime();
			createExif=q.value(4).toDateTime();
			cam=q.value(5).toString();
			coord=GpsCoord(q.value(6), q.value(7), q.value(8));
			camSettings=q.value(9).toString();
			ori=(ExifOrientation)q.value(10).toInt();
			setObjectName(QString("ImageF(%1)").arg(pk));
			ok=true;
			return;
		}
		else
			qWarning()<<"Image: INVALID db record (no pk) for"<<sourcePk<<imgeName;
	}
	// not in DB, so build from scratch
	dbDirty=true;
	setObjectName(QString("Image(%1)").arg(imageName));
	Source & src=DCON.getSource(sourcePk);
	ImageAttrs attrs;
	ok=src.readImageInfo(imageName, attrs);
	for(auto it=attrs.begin(); it!=attrs.end(); ++it)
	{
		switch(it->first)
		{
		case ImageAttr::FILESIZE:
			fileSize=it->second.toInt();
			break;
		case ImageAttr::DESCRIPTION:
			desc=it->second.toString();
			break;
		case ImageAttr::CAMERA:
			cam=it->second.toString();
			break;
		case ImageAttr::CAMERA_SETTING:
			camSettings=it->second.toString();
			break;
		case ImageAttr::CREATE_DATE_FILE:
			createFile=it->second.toDateTime();
			break;
		case ImageAttr::CREATE_DATE_EXIF:
			createExif=it->second.toDateTime();
			break;
		case ImageAttr::GPS:
			coord=it->second.value<GpsCoord>();
			break;
		case ImageAttr::EXIF_ORIENTATION:
			ori=(Image::ExifOrientation)(it->second.toInt()); // enum ExifOrientation is defined according to EXIF definition
			break;
		}
	}
}

Image::~Image()
{
	for(auto it=pixmapMP.begin(); it!=pixmapMP.end(); ++it)
		delete it->second;
}

bool Image::commit2Db()
{
	if(!isValid() || !dbDirty)
		return false;
	QString lonStr, latStr, heiStr;
	if(coord.hasValidLonLat())
	{
		lonStr=QString::number(coord.lon);
		latStr=QString::number(coord.lat);
	}
	else
	{
		lonStr=QStringLiteral("NULL");
		latStr=lonStr;
	}
	if(coord.hasValidHeight())
		heiStr=QString::number(coord.height);
	else
		heiStr=QStringLiteral("NULL");
	if(pk<0)
	{
		QSqlQuery q(QStringLiteral("insert into images "
								   "(source, val, description, size, date_file, date_exif, camera, longitude, latitude, height, camera_settings, orientation) values "
								   "(%1,     %2,  %3,          %4,   %5,        %6,        %7,     %8,        %9,       %10,    %11,             %12)").\
					arg(sourcePk).arg(DataContainer::toSqlString(imageName), DataContainer::toSqlString(desc, true)).\
					arg(fileSize).arg(DataContainer::toSqlString(createFile, true), DataContainer::toSqlString(createExif, true), DataContainer::toSqlString(cam, true)).\
					arg(lonStr, latStr, heiStr).\
					arg(DataContainer::toSqlString(camSettings, true)).arg((int)ori), \
					DCON.db()->db());
		if(q.lastError().isValid())
		{
			qCritical()<<objectName()<<"Image::commit2Db FAILED"<<q.lastError().text()<<q.lastQuery();
			return false;
		}
		dbDirty=false;
		bool ok=false;
		pk=q.lastInsertId().toInt(&ok);
		if(!ok)
		{
			qDebug()<<"Image::commit2Db insert pk FAILED"<<imageName;
			return false;
		}
		qDebug()<<"Image::commit2Db inserted"<<imageName<<pk;
		return true;
	}
	// update
	QSqlQuery q(QString("update images set description=%2, longitude=%3, latitude=%4, height=%5, orientation=%6 where pk=%1").arg(pk).\
				arg(DataContainer::toSqlString(desc, true), lonStr, latStr, heiStr).arg((int)ori), DCON.db()->db());
	if(q.lastError().isValid())
	{
		qCritical()<<"Image::commit2Db update FAILED"<<q.lastError()<<q.lastQuery();
		return false;
	}
	if(pixDirty)
	{
		for(auto it=pixmapMP.begin(); it!=pixmapMP.end(); ++it)
			delete it->second;
		pixmapMP.clear();
		pixDirty=false;
		emit pixChangedSig(pk);
	}
	dbDirty=false;
	return true;
}

std::vector<int> Image::getTagPks() const
{
	vector<int> res;
	QSqlQuery q(QString("select t.pk from tags t inner join taggings tg on t.pk=tg.tag where tg.image=%1").arg(pk), DCON.db()->db());
	if(q.lastError().isValid())
	{
		qCritical()<<"Image::getTagPks FAILED"<<q.lastQuery()<<q.lastError();
		return res;
	}
	while(q.next())
	{
		res.push_back(q.value(0).toInt());
	}
	return res;
}

bool Image::setTagPks(std::vector<int> tagPks)
{
	auto ldb=DCON.db()->db();
	ldb.transaction();
	QSqlQuery qd(QString("delete from taggings where image=%1").arg(pk));
	if(qd.lastError().isValid())
	{
		qCritical()<<objectName()<<"setTagPks delete FAILED"<<qd.lastError().text()<<qd.lastQuery();
		ldb.rollback();
		return false;
	}
	QString t("(%1,%2)");
	QStringList vls;
	for(auto pit=tagPks.begin(); pit!=tagPks.end(); ++pit)
	{
		vls<<t.arg(pk).arg(*pit);
	}
	QSqlQuery qi(QString("insert into taggings (image, tag) values %1").arg(vls.join(',')), ldb);
	if(qi.lastError().isValid())
	{
		qCritical()<<"Image::getTagPks FAILED"<<qi.lastQuery()<<qi.lastError();
		ldb.rollback();
		return false;
	}
	ldb.commit();
	return true;
}

QString Image::getInfo()
{
	if(!ok)
		return tr("invalid image %1 (%2,%3)").arg(imageName).arg(pk).arg(sourcePk);
	QPixmap *px=pixels();
	if(px && !px->isNull())
	{
		QSize sz=px->size();
		return QString("%5: %1 x %2 (%3 bit%4)").arg(sz.width()).arg(sz.height()).arg(px->depth()).\
				arg(px->hasAlpha()?", alpha":"").arg(imageName);
	}
	else
		return tr("invalid pixmap (%1,%2)").arg(imageName).arg(sourcePk);
}

bool Image::setDesc(const QString &d, bool autoCommit)
{
	if(desc==d)
		return true;
	desc=d;
	dbDirty=true;
	if(autoCommit)
		return commit2Db();
	//DCON.dropImageCache(pk); nope, this should be a cached object if used correctly
	return true;
}

bool Image::setCoord(const GpsCoord &c, bool autoCommit)
{
	if(coord==c)
		return true;
	coord=c;
	dbDirty=true;
	if(autoCommit)
		return commit2Db();
	return true;
}

bool Image::setOrientation(Image::ExifOrientation o, bool autoCommit)
{
	if(ori==o)
		return true;
	ori=o;
	dbDirty=true;
	pixDirty=true;
	if(autoCommit)
		return commit2Db();
	return true;
}

QPixmap *Image::pixels(int scale2Width)
{
	if(!ok)
		return nullptr;
	ping();
	auto it=pixmapMP.find(scale2Width);
	if(it==pixmapMP.end())
	{
		QPixmap * px=nullptr;
		auto zit=pixmapMP.find(0);
		if(zit==pixmapMP.end())
		{
			Source &src=DCON.getSource(sourcePk);
			px=src.readImagePixels(imageName, scale2Width, ori);
		}
		else
		{
			px=new QPixmap(zit->second->scaledToWidth(scale2Width));
		}
		if(px)
		{
			if(static_cast<int>(pixmapMP.size())==cachePxSize)
			{ // limit cache size
				qDebug()<<objectName()<<"cache drop"<<pixmapMP.rbegin()->first;
				delete pixmapMP.rbegin()->second;
				pixmapMP.erase(std::prev(pixmapMP.end()));
			}
			pixmapMP.insert(PixmapMP::value_type(scale2Width, px));
		}
		return px;
	}
	return it->second;
}

QPixmap Image::process()
{
	if(!ok)
		return QPixmap();
	ping();
	Source &src=DCON.getSource(sourcePk);
	cv::Mat image, mDisp;
	std::string utf8_filename=src.getFullUrl(imageName).toUtf8().constData();
	image = cv::imread( utf8_filename, 1 );
	if(!image.data)
	{
		qDebug()<<"No image data";
		return QPixmap();
	}
	qDebug()<<"openCV Version"<<cv::getVersionString().c_str()<<cv::getVersionRevision();
	//cv::namedWindow("Image A", cv::WINDOW_NORMAL );
	//cv::namedWindow("Image B", cv::WINDOW_NORMAL );
	cv::Mat ig, padded;                            //expand input image to optimal size
	cv::cvtColor(image, ig, cv::COLOR_RGB2GRAY);
	int m = cv::getOptimalDFTSize(ig.rows);
	int n = cv::getOptimalDFTSize(ig.cols); // on the border add zero values
	cv::copyMakeBorder(ig, padded, 0, m - ig.rows, 0, n - ig.cols, cv::BORDER_CONSTANT, cv::Scalar::all(0));
	//qDebug()<<image.rows<<m<<image.cols<<n<<padded.data;

	cv::Mat planes[2];// = {Mat_<float>(padded), Mat::zeros(padded.size(), CV_32F)};
	padded.convertTo(planes[0], CV_32F); //, 1./256.
	planes[1]=cv::Mat(padded.size(), CV_32F, 128);
	//planes[1]+=Scalar::all(128);
	cv::Mat complexI;
	cv::merge(planes, 2, complexI);         // Add to the expanded another plane with zeros
	cv::dft(complexI, complexI);            // this way the result may fit in the source matrix

	// compute the magnitude and switch to logarithmic scale
	// => log(1 + sqrt(Re(DFT(I))^2 + Im(DFT(I))^2))
	cv::split(complexI, planes);                   // planes[0] = Re(DFT(I), planes[1] = Im(DFT(I))
	cv::Mat magI;
	cv::magnitude(planes[0], planes[1], magI);
	// switch to logarithmic scale
	magI+=cv::Scalar::all(1);
	cv::log(magI, magI);
	// crop the spectrum, if it has an odd number of rows or columns
	magI = magI(cv::Rect(0, 0, magI.cols & -2, magI.rows & -2));
	// rearrange the quadrants of Fourier image  so that the origin is at the image center
	int cx = magI.cols/2;
	int cy = magI.rows/2;

	cv::Mat q0(magI, cv::Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
	cv::Mat q1(magI, cv::Rect(cx, 0, cx, cy));  // Top-Right
	cv::Mat q2(magI, cv::Rect(0, cy, cx, cy));  // Bottom-Left
	cv::Mat q3(magI, cv::Rect(cx, cy, cx, cy)); // Bottom-Right

	cv::Mat tmp;                           // swap quadrants (Top-Left with Bottom-Right)
	q0.copyTo(tmp);
	q3.copyTo(q0);
	tmp.copyTo(q3);
	q1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
	q2.copyTo(q1);
	tmp.copyTo(q2);

	double vmin, vmax;
	cv::minMaxLoc(magI, &vmin, &vmax);
	double alpha=(vmax-vmin<1e-8?1.:1./(vmax-vmin));
	double beta=-vmin*alpha;
	cv::convertScaleAbs(magI, mDisp, 256.*alpha, beta);
	//qDebug()<<planes[0].at<float>(42, 24)<<planes[1].at<float>(42, 24)<<magI.at<float>(42, 24);
	qDebug()<<"Image::process"<<mDisp.cols<<mDisp.rows;
	QImage img((unsigned char*) mDisp.data, mDisp.cols, mDisp.rows, QImage::Format_Grayscale8);
	return QPixmap::fromImage(img.rgbSwapped());
	//cv::imshow("Image A", mDisp);

/*	cv::Size gs(300,200);
	cv::Mat graph(gs, CV_8UC3,cv:: Scalar(80,0,0));
	double scy=(double)magI.rows/(double)magI.cols;
	double scx=(double)gs.width/(double)magI.cols;
	int vold=(magI.at<float>(0,0)*alpha+beta)*200;
	int xold=0;
	for(int i=1; i<magI.cols; i++)
	{
		int r=(double)i*scy;
		int v=(magI.at<float>(r,i)*alpha+beta)*gs.height;
		int ix=(double)i*scx;
		line(graph, cv::Point(xold, vold), cv::Point(ix, v), cv::Scalar(0, 0, 255));
		vold=v;
		xold=ix;
	}*/
	//cv::imshow("Image B", graph);
	/*Mat dx, dy;
	Sobel( image, dx, CV_32F, 1, 0, 3 );
	Sobel( image, dy, CV_32F, 0, 1, 3 );
	magnitude( dx, dy, dx );
	qDebug()<<"bluriness indicator"<<sum(dx)[0];*/
}

