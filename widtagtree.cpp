#include "widtagtree.h"
#include "dlgedittag.h"
#include "idxi.h"

Q_LOGGING_CATEGORY(widtagtree, "widtagtree")

WidTagTree::WidTagTree(QWidget *parent) : QTreeWidget(parent)
{
	connect(this, &QTreeWidget::itemDoubleClicked, this, &WidTagTree::editSl);
}

void WidTagTree::dropEvent(QDropEvent *event)
{
	Q_UNUSED(event)
	//isDirty=true;
	emit somethingChangedSig();
	QTreeWidget::dropEvent(event);
}

void WidTagTree::editSl(QTreeWidgetItem *item, int column)
{
	Q_UNUSED(column)
	DlgEditTag dlg(item, this);
	if(dlg.exec()==QDialog::Accepted)
	{
		//qDebug()<<"WidTagTree::editSl Accepted";
		emit somethingChangedSig();
	}
	//qDebug()<<"WidTagTree::editSl (end)";
}
