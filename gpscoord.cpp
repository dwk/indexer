#include "gpscoord.h"
#include <QRegularExpression>
#include <QRegularExpressionMatchIterator>
#include "idxi.h"

bool GpsCoord::operator==(const GpsCoord &o)
{
	bool equ;
	if(hasValidLonLat())
	{
		if(o.hasValidLonLat())
			equ=(lon==o.lon && lat==o.lat);
		else
			equ=false;
	}
	else
		equ=(!o.hasValidLonLat());
	if(equ)
	{
		if(hasValidHeight())
		{
			if(o.hasValidHeight())
				equ=(height==o.height);
			else
				equ=false;
		}
		else
			equ=(!o.hasValidHeight());
	}
	return equ;
}

GpsCoord::GpsCoord(const QVariant &longitude, const QVariant &latitude, const QVariant &height) :
	lon(0.), lat(0.), height(0)
{
	bool ok=false;
	lon=longitude.toDouble(&ok);
	if(ok)
	{
		lat=latitude.toDouble(&ok);
		if(!ok)
			lon=std::numeric_limits<double>::quiet_NaN();
	}
	else
		lon=std::numeric_limits<double>::quiet_NaN();
	this->height=height.toInt(&ok);
	if(!ok)
		this->height=-1000;
}

GpsCoord::GpsCoord(const QString &longitude, const QString &longitudeRef, const QString &latitude, const QString &latitudeRef, const QString &heightstr) :
	lon(std::numeric_limits<double>::quiet_NaN()), lat(0.), height(-1000)
{
	QRegularExpression rx("[0-9.]+");
	QRegularExpressionMatchIterator matches = rx.globalMatch(longitude);
	std::vector<double> degs;
	bool ok=false;
	while (matches.hasNext())
	{
		QRegularExpressionMatch match = matches.next();
		double d=match.captured(0).toDouble(&ok);
		if(!ok)
			break;
		degs.push_back(d);
	}
	if(degs.size()==3)
	{
		lon=degs.at(0)+degs.at(1)/60.+degs.at(2)/3600.;
		if(longitudeRef=="W")
			lon*=-1.;
		else if(longitudeRef!="E")
			qWarning()<<"invalid GPS longitude reference"<<longitudeRef;
		matches = rx.globalMatch(latitude);
		degs.clear();
		while (matches.hasNext())
		{
			QRegularExpressionMatch match = matches.next();
			double d=match.captured(0).toDouble(&ok);
			if(!ok)
				break;
			degs.push_back(d);
		}
		if(degs.size()==3)
		{
			lat=degs.at(0)+degs.at(1)/60.+degs.at(2)/3600.;
			if(latitudeRef=="S")
				lat*=-1.;
			else if(latitudeRef!="N")
				qWarning()<<"invalid GPS latitude reference"<<latitudeRef;
		}
		else
		{
			qCritical()<<"invalid GPS latitude"<<latitude;
			lon=std::numeric_limits<double>::quiet_NaN();
		}
	}
	else
	{
		qCritical()<<"invalid GPS longitude"<<longitude;
		lon=std::numeric_limits<double>::quiet_NaN();
	}
	height=heightstr.toInt(&ok);
	if(!ok)
		height=-1000;
}

GpsCoord::GpsCoord(const QString &fromString) :
	lon(std::numeric_limits<double>::quiet_NaN()), lat(0.), height(-1000)
{
	if(fromString.isEmpty())
		return;
	bool ok1=false, ok2=false;
	if(fromString.startsWith(QStringLiteral("H")))
	{
		height=fromString.mid(1).toInt(&ok1);
		if(!ok1)
		{
			qWarning()<<"GpsCoord from string invalid height"<<fromString;
			height=-1000;
		}
		return;
	}
	if(fromString.count(',')==1)
	{ // could be Google maps ? OSM format lat,lon
		QStringList ll=fromString.split(',');
		if(ll.size()==2)
		{
			lon=ll[1].toDouble(&ok1);
			lat=ll[0].toDouble(&ok2);
			if(!ok1 || !ok2)
			{
				qWarning()<<"GpsCoord from string invalid latitude,logintude"<<fromString;
				lon=std::numeric_limits<double>::quiet_NaN();
			}
			return;
		}
		// go on, maybe it meant something different
	}
	QStringList l=fromString.split(QRegExp("[(\\/)]"), Qt::SkipEmptyParts);
	//qDebug()<<"GpsCoord "<<fromString<<l;
	if(l.size()==2 || l.size()==3)
	{
		lon=l[0].toDouble(&ok1);
		lat=l[1].toDouble(&ok2);
		if(!ok1 || !ok2)
		{
			qWarning()<<"GpsCoord from string invalid logintude/latitude"<<fromString;
			lon=std::numeric_limits<double>::quiet_NaN();
		}
	}
	if(l.size()==3)
	{
		height=l[2].toInt(&ok1);
		if(!ok1)
		{
			qWarning()<<"GpsCoord from string invalid height (2)"<<fromString;
			height=-1000;
		}
	}
}

QString GpsCoord::toString() const
{
	if(hasValidLonLat())
	{
		if(hasValidHeight())
			return QStringLiteral("%1/%2(%3)").arg(lon, 0, 'f', 5).arg(lat, 0, 'f', 5).arg(height);
		else
			return QStringLiteral("%1/%2").arg(lon, 0, 'f', 5).arg(lat, 0, 'f', 5);
	}
	else if(hasValidHeight())
		return QStringLiteral("H%1").arg(height);
	return QString();
}

QDebug operator<<(QDebug dbg, const GpsCoord &coord)
{
	if(coord.hasValidHeight())
	{
		if(coord.hasValidLonLat())
			dbg << "GPS(" << coord.lon << coord.lat << coord.height << ")";
		else
			dbg << "GPS(height " << coord.height << ")";
	}
	else
	{
		if(coord.hasValidLonLat())
			dbg << "GPS(" << coord.lon << coord.lat << ")";
		else
			dbg << "GPS(invalid)";
	}
	return dbg.maybeSpace();
}

