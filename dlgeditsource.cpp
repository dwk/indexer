#include "dlgeditsource.h"
#include <QFocusEvent>
#include <QSqlQuery>
#include <QSqlError>
#include <QFileDialog>
#include "ui_dlgeditsource.h"
#include "idxi.h"

Q_LOGGING_CATEGORY(dlgeditsource, "dlgeditsource")


DlgEditSource::DlgEditSource(Source *baseSrc, QWidget *parent) :
	QDialog(parent), ui(new Ui::DlgEditSource), bSrc(baseSrc)
{
	ui->setupUi(this);
	ui->comboSourceType->blockSignals(true);
	QSqlQuery q(QString("select pk, val from source_types"));
	if(q.lastError().isValid())
	{
		qWarning()<<"DlgEditSource: failed query"<<q.lastQuery()<<q.lastError();
	}
	else
	{
		while(q.next())
		{
			QString v=q.value(1).toString();
			ui->comboSourceType->addItem(v, q.value(0));
		}
		ui->comboSourceType->setCurrentIndex(1);
	}
	if(baseSrc)
	{
		ui->lineEdit->setText(bSrc->location());
		ui->label->setText(tr("new location based on %1").arg(bSrc->location()));
	}
	else
		ui->label->setText(tr("new location"));
	ui->comboSourceType->blockSignals(false);
	ui->pushRename->setEnabled(false);
}

DlgEditSource::DlgEditSource(int sourcePk, QWidget *parent) :
	QDialog(parent), ui(new Ui::DlgEditSource)
{
	ui->setupUi(this);
	ui->comboSourceType->blockSignals(true);
	src=&DCON.getSource(sourcePk);
	if(!src)
	{
		qCritical()<<"edit non existing source"<<sourcePk;
		return;
	}
	QSqlQuery q(QString("select val from source_types where pk=%1").arg(1/*src->getType()*/));
	if(q.lastError().isValid())
	{
		qWarning()<<"DlgEditSource: failed query"<<q.lastQuery()<<q.lastError();
	}
	else
	{
		if(q.next())
		{
			ui->comboSourceType->addItem(q.value(0).toString());
		}
	}
	ui->comboSourceType->setEnabled(false);
	ui->lineEdit->setText(src->location());
	ui->lineComment->setText(src->getComment());
	ui->label->setText(QString::number(sourcePk));
	ui->pushRename->setEnabled(false);
}

DlgEditSource::~DlgEditSource()
{
	delete ui;
}

void DlgEditSource::accept()
{
	QString newLoc=ui->lineEdit->text();
	int opk=Source::location2Pk(newLoc);
	if(opk>0 && (!src || opk!=src->getPk()))
	{
		QMessageBox::warning(this, tr("Existing Source"), \
			tr("The selected location (%1) does already exist in the database (id %2). Douplicates not allowed.").arg(newLoc).arg(opk));
		return;
	}
	if(src)
	{
		src->setLocation(newLoc, false);
		src->setComment(ui->lineComment->text(), false);
		src->commit2Db();
		usedParent=&DCON.getSource(src->getParentPk());
		qCDebug(dlgeditsource)<<"DlgEditSource updated source"<<src->getPk();
	}
	else
	{
		Source *trueparent=nullptr;
		if(bSrc) // if no base given it will always become a top level element - unusual, only possible if no selection in tree
		{
			if(newLoc.startsWith(bSrc->location()))
				trueparent=bSrc; // todo check if a child could be used as parent
			else
			{
				trueparent=&DCON.getSource(bSrc->getParentPk());
				if(!newLoc.startsWith(trueparent->location()))
				{ // treeview root has empty location and thus will accept all locations
					QMessageBox::warning(this, tr("Invalid Source location"), \
						tr("The selected location is not part of the parent source (%1). Create at top level or within the appropriate Source.").\
										 arg(trueparent->location()));
					return;
				}
			}
		}
		Source nsrc(trueparent, nullptr);
		nsrc.setLocation(newLoc, false);
		nsrc.setComment(ui->lineComment->text(), false);
		nsrc.commit2Db();
		usedParent=trueparent;
		qCDebug(dlgeditsource)<<"DlgEditSource created source"<<nsrc.getPk()<<nsrc.location();
	}
	QDialog::accept();
}

void DlgEditSource::on_comboSourceType_currentTextChanged(const QString &text)
{
	Q_UNUSED(text)
	if(ui->comboSourceType->currentData().toInt()!=1)
	{
		QMessageBox::warning(this, tr("Unsupported"), tr("Sorry, currently only filesystems supported as source."));
		ui->comboSourceType->setCurrentIndex(1);
	}
}

void DlgEditSource::on_lineEdit_textEdited(const QString &)
{
	ui->pushRename->setEnabled(src!=nullptr);
}

void DlgEditSource::on_pushBrowse_clicked()
{
	QString startdir;
	if(src)
	{
		startdir=src->location();
	}
	else if(bSrc)
	{
		startdir=bSrc->location();
	}
	QString nd=QFileDialog::getExistingDirectory(this, tr("Source"), startdir);
	if(nd.isEmpty())
		return; // Cancel selected
	int opk=Source::location2Pk(nd);
	if(opk>0 && (!src || opk!=src->getPk()))
	{
		QMessageBox::warning(this, tr("Existing Source"), \
			tr("The selected location (%1) does already exist in the database (id %2). Douplicates not allowed.").arg(nd).arg(opk));
		return;
	}
	if(src)
	{ // verify that parent stays parent
		if(nd.size()<=startdir.size() || !nd.startsWith(startdir))
		{
			QMessageBox::warning(this, tr("Invalid Source"), \
				tr("The selected location is not part of the parent source (%1). Move to different position in tree currently not supported.").arg(startdir));
			return;
		}
		// warnings about content
		int newsize=Source::sizeOfDir(nd);
		if(newsize<src->size())
		{
			if(QMessageBox::warning(this, tr("Different Source Content"), \
				tr("The selected location contains less images (%1) than the current source (%2). A merge on the changed source will delete image tags. Proceed?").arg(newsize).arg(src->size()), \
				QMessageBox::Yes | QMessageBox::No, QMessageBox::No)==QMessageBox::No)
				return;
		}
		else if(newsize>src->size())
		{
			if(QMessageBox::warning(this, tr("Different Source Content"), \
				tr("The selected location contains more images (%1) than the current source (%2). You know what you do?").arg(newsize).arg(src->size()), \
				QMessageBox::Yes | QMessageBox::No, QMessageBox::No)==QMessageBox::No)
				return;
		}
		ui->lineEdit->setText(nd);
		ui->label->setText(QString("%1 -> %2").arg(src->location(), nd));
	}
	else
	{ // new entry
		// correct parent will be found in accept
		ui->lineEdit->setText(nd);
	}
}

void DlgEditSource::on_pushRename_clicked()
{
	if(!src)
	{
		QApplication::beep();
		return;
	}
	if(!src->renameLocation(ui->lineEdit->text(), true))
	{
		QMessageBox::warning(this, tr("Renaming FAILED"), tr("Failed to rename %1 to %2").arg(src->location(), ui->lineEdit->text()));
		ui->lineEdit->setText(src->location());
		ui->pushRename->setEnabled(false);
	}
}

