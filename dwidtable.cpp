#include "dwidtable.h"
#include "dwidtable_fac.h"
#include "ui_dwidtable.h"
#include <QSqlRecord>
#include <QSqlError>
#include "idxi.h"

Q_LOGGING_CATEGORY(dwidtable, "dwidtable")

DWidTable::DWidTable(QWidget *parent) :
	QWidget(parent),
	ui(new ::Ui::DWidTable)
{
	setObjectName(techname);
	ui->setupUi(this);
	QStringList tbns=DCON.db()->getAllTableNames();
	ui->comboTableName->blockSignals(true);
	ui->comboTableName->addItem(tr("<no table selected>"));
	//ui->comboTableName->addItem(tr("DUMMY"));
	ui->comboTableName->addItems(tbns);
	ui->comboTableName->setCurrentIndex(0);
	ui->comboTableName->blockSignals(false);
	ui->pushSubmit->setDisabled(true);
	ui->pushNew->setDisabled(true);
	ui->pushDelete->setDisabled(true);
	ui->pushReload->setDisabled(true);

	model = new QSqlTableModel(this, DCON.db()->db());
	connect(model, &QAbstractItemModel::dataChanged, this, &DWidTable::editFinishedSl);

	ui->tableView->setModel(model);
	connect(ui->tableView->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)),
		this, SLOT(tableSelChangedSl(const QItemSelection &, const QItemSelection &)));
	//ui->tableView->show();
}

DWidTable::~DWidTable()
{
	delete ui;
}

void DWidTable::editFinishedSl()
{
	ui->pushSubmit->setEnabled(model->isDirty());
}

void DWidTable::tableSelChangedSl(const QItemSelection &selected, const QItemSelection &deselected)
{
	Q_UNUSED(selected)
	Q_UNUSED(deselected)
	//QModelIndexList li=ui->tableView->selectionModel()->selection().indexes();
	//relevant for delete are only full rows
	QModelIndexList li=ui->tableView->selectionModel()->selectedRows();
	int r=-1;
	foreach(QModelIndex index, li)
	{
		//qDebug()<<"selection"<<index.row()<<index.column();
		if(r==-2)
			continue;
		if(r==-1)
			r=index.row();
		else if(r==index.row())
			continue;
		else
			r=-2;
	}
	ui->pushDelete->setEnabled(r>=0);
}

/*void DWidTable::on_pushTest_clicked()
{
	emit testSig();
}*/

void DWidTable::on_comboTableName_currentIndexChanged(int index)
{
	if(model->isDirty())
	{
		if(QMessageBox::question(this, tr("Unsaved Changes"), tr("There are pending, unsaved chnages. Commit these before (reloading)?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes)==QMessageBox::Yes)
		{
			on_pushSubmit_clicked();
		}
	}
	QString tabName=ui->comboTableName->itemText(index);
	qDebug()<<"selecting table"<<tabName;
	model->setTable(tabName);
	model->setEditStrategy(QSqlTableModel::OnManualSubmit);
	bool success=model->select();
	ui->pushNew->setEnabled(success);
	ui->pushReload->setEnabled(success);
	ui->pushDelete->setDisabled(true);
	ui->pushSubmit->setEnabled(success && model->isDirty());
	if(!success && tabName!=tr("<no table selected>"))
	{
		QMessageBox::critical(nullptr, tr("Error during Select"), model->lastError().text());
	}
	ui->tableView->resizeColumnsToContents();
	ui->tableView->resizeRowsToContents();
}

void DWidTable::on_pushSubmit_clicked()
{
	if(!model->submitAll())
	{
		QMessageBox::critical(nullptr, tr("Error during Commit"), model->lastError().text());
	}
	ui->pushSubmit->setEnabled(model->isDirty());
}

void DWidTable::on_pushNew_clicked()
{
	QSqlRecord newRecord = model->record();
	//newRecord.setValue("id", 0);
	if(!model->insertRecord(model->rowCount(), newRecord))
	{
		QMessageBox::critical(nullptr, tr("Error during Insert"), model->lastError().text());
	}
	else
		qDebug() << "New record inserted";
	ui->tableView->resizeColumnsToContents();
	ui->tableView->resizeRowsToContents();
	ui->pushSubmit->setEnabled(model->isDirty());
}

void DWidTable::on_pushDelete_clicked()
{
	if(!model->removeRow(ui->tableView->currentIndex().row()))
	{
		QMessageBox::critical(nullptr, tr("Error during Delete"), model->lastError().text());
	}
	else
		qDebug() << "Record deleted";
	ui->pushSubmit->setEnabled(model->isDirty());
}

void DWidTable::on_pushReload_clicked()
{
	on_comboTableName_currentIndexChanged(ui->comboTableName->currentIndex());
}
