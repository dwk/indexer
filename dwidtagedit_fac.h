#ifndef DWIDTAGEDITFAC_H
#define DWIDTAGEDITFAC_H

#include "dockwidfactory.h"

namespace
{
QWidget * DockCreator(QWidget * parent)
{
	return new DWidTagEdit(parent);
}
const char *techname="dwid_tagedit";
bool dockWidRegistered=DockWidFactory::get().registerDockWid("TagEditor", techname, Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea, Qt::BottomDockWidgetArea, DockCreator);
}
#endif // DWIDTAGEDITFAC_H
