#ifndef WIDLABEL_H
#define WIDLABEL_H

#include "idxh.h"
#include <QWidget>
#include <QLabel>
#include <QPropertyAnimation>
#include <QColor>
#include "globalenum.h"

Q_DECLARE_LOGGING_CATEGORY(widlabel)

#define MARKERSIZE 8
#define BRASIZE 15

class MarkerWid : public QWidget
{
	Q_OBJECT
public:
	enum class Type { None, MoreAbove, MoreBelow, MoreLeft, MoreRight };
	enum class Style { Hint, Strong, Active };
	explicit MarkerWid(MarkerWid::Type type, QWidget *parent = 0);
	virtual QSize sizeHint() const {return sz;}
	virtual QSize minimumSizeHint() const {return sz;}
	void setType(MarkerWid::Type type) {t=type;}
	void setStyle(MarkerWid::Style style) {s=style;}
protected:
	virtual void enterEvent(QEvent *event);
	virtual void paintEvent(QPaintEvent *event);
signals:
	void markerEnteredSig();
private:
	Type t;
	Style s=Style::Hint;
	QSize sz;
};


class TagWid : public QWidget
{
	Q_OBJECT
public:
	enum class Shape {BOX=0, RBOX, DIAMOND};
	explicit TagWid(int tagPk, const QString & text, const QString & baseStyle, bool hasChildren, int minWidth, int actionIndicator, QWidget *parent = 0);
	virtual QSize sizeHint() const {return sz;}
	virtual QSize minimumSizeHint() const {return minimumSize();}
	void setOperationMode(WidOperationMode newOpMode) {opMode=newOpMode;}
	int getPosHint() const;
	void setPosHint(int value);
	int getPk() const {return pk;}
	bool hasChildren() const {return childMarker!=nullptr;}
	QString toString() {return lab->text();}
public slots:
	void setExpandedDeco(bool isExpanded) {if(childMarker) childMarker->setStyle(isExpanded?MarkerWid::Style::Active:MarkerWid::Style::Strong);}
protected:
	virtual void contextMenuEvent(QContextMenuEvent *event);
	virtual void mouseReleaseEvent(QMouseEvent *event);
	virtual void paintEvent(QPaintEvent *event);
private slots:
	void actionMapSl(LabelAction cmd) {emit labelCmdSig(pk, cmd);}
private:
	QColor cssVal2Color(const QString & value);
	QString cssCompose();
	QLabel *lab, *actInd=nullptr;
	MarkerWid *childMarker=nullptr;
	WidOperationMode opMode=WidOperationMode::DATABASE_UPDATE;
	QString styleBase;
	QColor colBack, colFont;
	Shape shape=Shape::BOX;
	int pk=-1, posHint=0;
	QSize sz;
signals:
	void labelCmdSig(int tagPk, LabelAction command);
};
typedef std::map< int , TagWid* > TagWidMSP; // tag-pk -> tag

#endif // WIDLABEL_H
