#ifndef SOURCE_H
#define SOURCE_H

#include "idxh.h"
#include <QObject>
#include <QSqlDatabase>
#include <QPointer>
#include <QDir>
#include <QPixmap>
#include <QDateTime>
#include <libexif/exif-data.h>  // depends on package libexif-dev
#include "image.h"
//class Image;
class DWidImages;

Q_DECLARE_LOGGING_CATEGORY(source)

// Source encapsulates the access to directories or web sites
// handles image retrieval (and data related to images, like file size) up to and including QPixmap creation
// is the front end of the DB-object source.
// handles DB-reality merges
class Source : public QObject
{
	Q_OBJECT
public:
	friend class Image; // access to createPixmap
	friend class DWidImages; // access to createImage
	static int location2Pk(const QString &urlStr);
	static int sizeOfDir(const QString & dirPath);
	static bool deleteComplete(int srcPk, QSqlDatabase & db, QWidget * messageParent); // will delete src and drop it from cache ; return false -> still exists
	explicit Source(Source * parentForNew, QObject *parent);
	explicit Source(int sourcePk, QObject *parent);
	bool commit2Db();
	bool isValid() const {return ok;}
	int getPk() const {return pk;}
	int getParentPk() const {return parentPk;}
	int getChildCnt();
	int getChildPk(int row);
	void reloadChildren() {childrenLoaded=false; children.clear();}
	int getRow();
	int size() const {return static_cast<int>(files.size());}
	QString location() const {if(ok && pk>0) return dir.path(); return QString();} // pk==-1 shall return empty instead of .
	bool setLocation(const QString &loc, bool autoCommit);
	bool renameLocation(const QString &loc, bool autoCommit); // includes DB update, no need to call setLocation, will not commit on failure
	QString locationShort() const {if(ok) return disp; return QString();}
	QString getFullUrl(const QString & filename) const {if(ok) return dir.absoluteFilePath(filename); return QString();}
	QString getComment()const {return comment;}
	bool setComment(const QString &c, bool autoCommit);
	void imageMergeLists(QStringList &missingInDb, std::vector<int> &missingInSource);
	bool readImageInfo(const QString &imageName, Image::ImageAttrs &attrs);
	QPixmap *readImagePixels(const QString &imageName, int scale2Width = 0, Image::ExifOrientation ori=Image::ExifOrientation::UP); // no scaling if scale2Width==0
	bool deleteImagePermanent(const QString &imageName); // true if success, false if failed
private:
	int getIndexOf(int childPk);
	bool addExifInfo(const QString &imageName, Image::ImageAttrs &attrs);
	int pk=-2; // pk==-1 is valid for the tree-root
	bool ok=false, childrenLoaded=false, dbDirty=false;
	int parentPk=-2; // parentPk==-1 is valid and points to the tree-root
	std::vector<int> children;
	QDir dir;
	QStringList files;
	QString comment, disp;
signals:
};
typedef std::map< int , QPointer<Source> > SourceMVQ;

#endif // SOURCE_H
