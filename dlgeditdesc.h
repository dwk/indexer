#ifndef DLGEDITDESC_H
#define DLGEDITDESC_H

#include "idxh.h"
#include <QDialog>

Q_DECLARE_LOGGING_CATEGORY(dlgeditdesc)

class QLineEdit;
namespace Ui
{
	class DlgEditDesc;
}

class ComboFilter : public QObject
{
	Q_OBJECT
public:
	explicit ComboFilter(QObject * watched, QLineEdit * target, QObject *parent=nullptr) :
		QObject(parent), watched(watched), target(target) {}
	virtual bool eventFilter(QObject *watched, QEvent *event);
private:
	QObject * watched;
	QLineEdit * target;
};

class DlgEditDesc : public QDialog
{
	Q_OBJECT
public:
	explicit DlgEditDesc(const QString &description, int count, bool identical, QWidget *parent);
	~DlgEditDesc();
	QString getText();
	bool append();
public slots:
	virtual void accept();
protected:
	//virtual void focusInEvent(QFocusEvent *event);
private slots:
	void checkChangedSl();
	void on_comboLastDescs_currentTextChanged(const QString &text);
private:
	Ui::DlgEditDesc *ui;
	int count;
};
#endif // DLGEDITDESC_H
