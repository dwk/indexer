#ifndef DWIDSEARCH_H
#define DWIDSEARCH_H

#include "idxh.h"
#include <list>
#include <QWidget>
#include "widactor.h"
#include "sqlobject.h"
#include <QSqlRelationalTableModel>
#include <QItemSelection>
#include <QStandardItem>
#include <QStyledItemDelegate>
#include <QDateTime>
#include "globalenum.h"

Q_DECLARE_LOGGING_CATEGORY(dwidsearch)

class QTreeWidgetItem;

class SearchDelegate: public QStyledItemDelegate
{
	Q_OBJECT
public:
	SearchDelegate(QObject *parent = nullptr) : QStyledItemDelegate(parent) {}
	QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &,
						  const QModelIndex &index) const override;
	void setEditorData(QWidget *editor, const QModelIndex &index) const override;
	void setModelData(QWidget *editor, QAbstractItemModel *model,
					  const QModelIndex &index) const override;
	void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option,
							  const QModelIndex &) const override {editor->setGeometry(option.rect);}
};


namespace Ui {
class DWidSearch;
}

class DWidSearch : public QWidget, public WidActor
{
	Q_OBJECT
public:
	explicit DWidSearch(QWidget *parent = 0);
	~DWidSearch();
// WidActor
public:
	virtual void connectAll(ObjectMSP &otherActors);
public slots:
	virtual void setupSl();
//
	void labelActionGSl(int tagPk, LabelAction la);
private:
	SqlRelation * getAnchorRelation(QTreeWidgetItem **parent = nullptr);
	SqlObject * getCurrentObject(QTreeWidgetItem **item = nullptr);
	void insertNewObject(QTreeWidgetItem *parent, SqlRelation *sqlParent, SqlObject *newObject);
	Ui::DWidSearch *ui;
	SqlObject *topsql;
private slots:
	void on_toolGo_clicked();
	void on_treeQuery_itemDoubleClicked(QTreeWidgetItem * item, int column);
	void on_treeQuery_itemSelectionChanged();
	void on_treeQuery_activated(const QModelIndex &index);
	void pushFilterTagClicked(int tagPk = -1);
	void on_pushFilterDateFile_clicked();
	void on_pushFilterDateExif_clicked();
	void on_pushFilterDesc_clicked();
	void on_pushFilterSource_clicked();
	void on_pushFilterGps_clicked();
	void on_pushRelAnd_clicked();
	void on_pushRelOr_clicked();
	void on_pushEditDelete_clicked();
	void on_pushEditDeleteAll_clicked();
signals:
	void querySelectedSig(void *); //QString query);
};

#endif // DWIDSEARCH_H
