#ifndef SOURCEQUERYMODEL_H
#define SOURCEQUERYMODEL_H

#include "idxh.h"
#include <QAbstractItemModel>
#include <QSqlDatabase>
#include "source.h"
#include "globalenum.h"

Q_DECLARE_LOGGING_CATEGORY(sourcequerymodel)

class SourceQueryModel : public QAbstractItemModel
{
	Q_OBJECT
public:
	explicit SourceQueryModel(QObject *parent = Q_NULLPTR, QSqlDatabase db = QSqlDatabase());
	virtual ~SourceQueryModel();
public:
	// QAbstractItemModel
	virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override {Q_UNUSED(parent) return 2;}
	virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
	virtual QModelIndex parent(const QModelIndex &index) const override;
	virtual Qt::ItemFlags flags(const QModelIndex &index) const override;
	virtual QVariant data(const QModelIndex &index, int role=Qt::DisplayRole) const override;
	virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
	//
	QModelIndex findSource(int sourcePk);
	int index2Pk(const QModelIndex &idx) const;
	void reload(Source * src);
public slots:
signals:
	void imageActionGSig(int imagePk, ImageAction action);
private:
	Source * index2Src(const QModelIndex &idx) const;
	QSqlDatabase db;
};

#endif // SOURCEQUERYMODEL_H
