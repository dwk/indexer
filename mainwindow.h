#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "idxh.h"
#include <QMainWindow>
#include <QSignalMapper>
#include "globalenum.h"

Q_DECLARE_LOGGING_CATEGORY(mainwindow)

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
public slots:
	void triggerAction(ImageAction action);
protected:
	virtual void closeEvent(QCloseEvent *event);
private slots:
	void imageActionGSl(int imagePk, ImageAction action);
	void about();
	void fromCamera();
	void globalShortcutSl();
	void globalAmbShortcutSl();
	void sourceSelSl(int sourcePk);
private:
	Ui::MainWindow *ui;
	ObjectMSP actors;
	QSignalMapper *tagMapper;
	bool singleImageSelected=false;
};

#endif // MAINWINDOW_H
