#ifndef DWIDTABLE_H
#define DWIDTABLE_H

#include "idxh.h"
#include <list>
#include <QWidget>
#include <QSqlTableModel>
#include <QItemSelection>
#include <QStandardItem>

Q_DECLARE_LOGGING_CATEGORY(dwidtable)

namespace Ui {
class DWidTable;
}

class DWidTable : public QWidget
{
	Q_OBJECT
public:
	explicit DWidTable(QWidget *parent = 0);
	~DWidTable();
public slots:
private:
	Ui::DWidTable *ui;
	QSqlTableModel *model;
private slots:
	//void on_pushTest_clicked();
	void editFinishedSl();
	void tableSelChangedSl(const QItemSelection & selected, const QItemSelection & deselected);
	void on_comboTableName_currentIndexChanged(int index);
	void on_pushSubmit_clicked();
	void on_pushNew_clicked();
	void on_pushDelete_clicked();
	void on_pushReload_clicked();
signals:
	//void testSig();
};

#endif // DWIDTABLE_H
