#ifndef WIDTAGTREE_H
#define WIDTAGTREE_H

#include "idxh.h"
#include <QTreeWidget>

Q_DECLARE_LOGGING_CATEGORY(widtagtree)

class WidTagTree : public QTreeWidget
{
	Q_OBJECT
public:
	WidTagTree(QWidget *parent = Q_NULLPTR);
	//bool dirty() const {return isDirty;}
protected:
	virtual void dropEvent(QDropEvent *event);
	//bool isDirty=false;
private slots:
	void editSl(QTreeWidgetItem *item, int column);
signals:
	void somethingChangedSig();
};

#endif // WIDTAGTREE_H
