#ifndef WIDACTOR_H
#define WIDACTOR_H

#include "idxh.h"
#include <QObject>

class WidActor
{
public:
	virtual void connectAll(ObjectMSP & otherActors) =0;
public slots:
	virtual void setupSl() =0;
};

#endif // WIDACTOR_H
