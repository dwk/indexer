#ifndef DLGIMAGEEDIT_H
#define DLGIMAGEEDIT_H

#include "idxh.h"
#include "globalenum.h"
#include "image.h"
#include <QDialog>

Q_DECLARE_LOGGING_CATEGORY(dlgimageedit)

namespace Ui
{
	class DlgImageEdit;
}
class QClipboard;

class DlgImageEdit : public QDialog
{
	Q_OBJECT
public:
	explicit DlgImageEdit(int imagePk, QWidget *parent);
	~DlgImageEdit();
public slots:
	virtual void accept() override;
protected:
private slots:
	void on_toolTurnRight_clicked();
	void on_toolTurnLeft_clicked();
	void on_toolTurnUpDown_clicked();
	void checkGpsPasteOptionSl();
	void on_toolGpsPaste_clicked();
	void on_checkGpsLonLat_stateChanged(int state);
	void on_checkGpsHeight_stateChanged(int state);
	void on_pushDelete_clicked();
	void touchSl();
private:
	Ui::DlgImageEdit *ui;
	Image & img;
	QClipboard *clipboard;
signals:
};
#endif // DLGIMAGEEDIT_H
