#ifndef DLGSEARCHCLAUSEGPS_H
#define DLGSEARCHCLAUSEGPS_H

#include "idxh.h"
#include <QDialog>
#include "gpscoord.h"

Q_DECLARE_LOGGING_CATEGORY(dlgsearchclausegps)

class SqlGpsClause;

namespace Ui
{
	class DlgSearchclauseGps;
}

class DlgSearchclauseGps : public QDialog
{
	Q_OBJECT
public:
	explicit DlgSearchclauseGps(SqlGpsClause *gpsc);
public slots:
	virtual void accept();
private slots:
	void on_checkLon_stateChanged(int state);
	void on_checkLat_stateChanged(int state);
	void on_checkHei_stateChanged(int state);
	void on_spinHeiMin_valueChanged(int val);
	void on_spinHeiMax_valueChanged(int val);
private:
	Ui::DlgSearchclauseGps *ui;
	SqlGpsClause *gpsc;
	GpsCoord gps;
};
#endif // DLGSEARCHCLAUSEGPS_H
