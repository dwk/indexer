#ifndef GPSCOORD_H
#define GPSCOORD_H

#include "idxh.h"
#include <cmath>
#include <QObject>

class GpsCoord
{
public:
	GpsCoord() : lon(std::numeric_limits<double>::quiet_NaN()), lat(0.), height(-1000) {}
	~GpsCoord() = default;
	GpsCoord(const GpsCoord &) = default;
	GpsCoord &operator=(const GpsCoord &) = default;
	bool operator==(const GpsCoord &o);

	GpsCoord(const double & longitude, const double & latitude, int height) : lon(longitude), lat(latitude), height(height) {}
	GpsCoord(const QVariant & longitude, const QVariant & latitude, const QVariant & height);
	GpsCoord(const QString & longitude, const QString & longitudeRef, const QString & latitude, const QString & latitudeRef, const QString & heightstr);
	GpsCoord(const QString & fromString);
	bool hasValidLonLat() const {return !std::isnan(lon);}
	void setLonLatInvalid() {lon=std::numeric_limits<double>::quiet_NaN(); lat=0.;}
	bool hasValidHeight() const {return height>-1000;}
	void setHeightInvalid() {height=-1000;}
	QString toString() const;
	double lon, lat; // lon==quiet_NaN means invalid lon / lat
	int height; // -1000 means invalid height
};
Q_DECLARE_METATYPE(GpsCoord)
QDebug operator<<(QDebug dbg, const GpsCoord &coord);

#endif // GPSCOORD_H
