#ifndef DLGSEARCHCLAUSEDATE_H
#define DLGSEARCHCLAUSEDATE_H

#include "idxh.h"
#include <QDialog>

Q_DECLARE_LOGGING_CATEGORY(dlgsearchclausedate)

class SqlDateTimeClause;

namespace Ui
{
	class DlgSearchclauseDate;
}

class DlgSearchclauseDate : public QDialog
{
	Q_OBJECT
public:
	explicit DlgSearchclauseDate(SqlDateTimeClause *dtc);
public slots:
	virtual void accept();
private slots:
	void on_radioTimeStart_toggled(bool checked);
	void on_radioTimeEnd_toggled(bool checked);
	void on_radioTime_toggled(bool checked);
private:
	Ui::DlgSearchclauseDate *ui;
	SqlDateTimeClause *dtc;
};
#endif // DLGSEARCHCLAUSEDATE_H
