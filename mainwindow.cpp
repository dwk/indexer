#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPointer>
#include <QDockWidget>
#include <QShortcut>
#include <QKeySequence>
#include "dlgabout.h"
#include "dlgfromcam.h"
#include "dockwidfactory.h"
#include "widactor.h"
#include "idxi.h"

Q_LOGGING_CATEGORY(mainwindow, "mainwindow")

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow), tagMapper(new QSignalMapper(this))
{
	ui->setupUi(this);
	DCON.setup("", this);

	QSettings settings;
	QString centrName=settings.value("layout/central", "dwid_image").toString();
	QWidget *central=nullptr;
	QStringList dws=DockWidFactory::get().allDockWids();
	foreach(QString dwname, dws)
	{
		DockWidFactory::DWInfo *dwi=DockWidFactory::get().produce(dwname);
		if(!dwi)
			continue;
		if(dwname==centrName)
		{
			qCDebug(mainwindow)<<"central widget"<<dwname;
			central=dwi->factory(nullptr);
			actors.insert(ObjectMSP::value_type(dwname, central));
			setCentralWidget(central);
			continue;
		}
		qCDebug(mainwindow)<<"dock widget"<<dwname;
		QDockWidget *dock = new QDockWidget(dwi->guiName, this);
		dock->setObjectName(dwi->techName);
		dock->setAllowedAreas(dwi->allowedAreas);
		QWidget *dw = dwi->factory(dock);
		dock->setWidget(dw);
		actors.insert(ObjectMSP::value_type(dwname, dw));
		ui->menuWindows->addAction(dock->toggleViewAction());
		/* bool restore=restoreDockWidget(dock);
		if(!restore)
		{
			qWarning()<<"unable to restore layout of dock widget"<<dwname;
			addDockWidget(dwi->defaultArea, dock);
		} */
	}
	if(!central)
		qWarning()<<"no central widget"<<centrName;
	bool restOkRead;
	restOkRead=restoreGeometry(settings.value("layout/geometry").toByteArray());
	restOkRead=restoreState(settings.value("layout/windowState").toByteArray()) && restOkRead;
	if(!restOkRead)
		qWarning()<<"unable to restore layout of dock widgets";

	connect(ui->actionAbout, SIGNAL(triggered(bool)), this, SLOT(about()));
	connect(ui->actionFromCamera, SIGNAL(triggered(bool)), this, SLOT(fromCamera()));
	auto itImages=actors.find(QString("dwid_images"));
	if(itImages!=actors.end())
	{
		connect(ui->actionNextImage, SIGNAL(triggered(bool)), itImages->second, SLOT(nextImageSl()));
		connect(ui->actionPrevImage, SIGNAL(triggered(bool)), itImages->second, SLOT(prevImageSl()));
		connect(ui->actionEditDescription, SIGNAL(triggered(bool)), itImages->second, SLOT(editDescriptionSl()));
		connect(ui->actionEditImage, SIGNAL(triggered(bool)), itImages->second, SLOT(editImageSl()));
		connect(ui->actionOpenGimp, SIGNAL(triggered(bool)), itImages->second, SLOT(openGimpSl()));
		connect(ui->actionDeleteImage, SIGNAL(triggered(bool)), itImages->second, SLOT(deleteImageSl()));
	}
	else
		qWarning()<<"unable to locate dwid_images - some signals may remain unconnected";
	auto itImage=actors.find(QString("dwid_image"));
	if(itImage!=actors.end())
	{
		connect(ui->actionTagAgain, SIGNAL(triggered(bool)), itImage->second, SLOT(tagAgainSl()));
	}
	else
		qWarning()<<"unable to locate dwid_image - some signals may remain unconnected";
	auto itSources=actors.find(QString("dwid_sources"));
	if(itSources!=actors.end())
	{
		connect(ui->actionOpenSource, SIGNAL(triggered(bool)), itSources->second, SLOT(openSourceSl()));
		ui->actionOpenSource->setEnabled(false);
		connect(itSources->second, SIGNAL(sourceSelectedSig(int)), this, SLOT(sourceSelSl(int)));
	}
	else
		qWarning()<<"unable to locate dwid_sources - some signals may remain unconnected";
	auto itTags=actors.find(QString("dwid_tags"));
	if(itTags!=actors.end())
	{
		connect(ui->actionSelectTag1, SIGNAL(triggered()), tagMapper, SLOT(map()));
		tagMapper->setMapping(ui->actionSelectTag1, 1);
		connect(ui->actionExpandTag1, SIGNAL(triggered()), tagMapper, SLOT(map()));
		tagMapper->setMapping(ui->actionExpandTag1, -1);
		connect(ui->actionSelectTag2, SIGNAL(triggered()), tagMapper, SLOT(map()));
		tagMapper->setMapping(ui->actionSelectTag2, 2);
		connect(ui->actionExpandTag2, SIGNAL(triggered()), tagMapper, SLOT(map()));
		tagMapper->setMapping(ui->actionExpandTag2, -2);
		connect(ui->actionSelectTag3, SIGNAL(triggered()), tagMapper, SLOT(map()));
		tagMapper->setMapping(ui->actionSelectTag3, 3);
		connect(ui->actionExpandTag3, SIGNAL(triggered()), tagMapper, SLOT(map()));
		tagMapper->setMapping(ui->actionExpandTag3, -3);
		connect(ui->actionSelectTag4, SIGNAL(triggered()), tagMapper, SLOT(map()));
		tagMapper->setMapping(ui->actionSelectTag4, 4);
		connect(ui->actionExpandTag4, SIGNAL(triggered()), tagMapper, SLOT(map()));
		tagMapper->setMapping(ui->actionExpandTag4, -4);
		connect(ui->actionSelectTag5, SIGNAL(triggered()), tagMapper, SLOT(map()));
		tagMapper->setMapping(ui->actionSelectTag5, 5);
		connect(ui->actionExpandTag5, SIGNAL(triggered()), tagMapper, SLOT(map()));
		tagMapper->setMapping(ui->actionExpandTag5, -5);
		connect(ui->actionSelectTag6, SIGNAL(triggered()), tagMapper, SLOT(map()));
		tagMapper->setMapping(ui->actionSelectTag6, 6);
		connect(ui->actionExpandTag6, SIGNAL(triggered()), tagMapper, SLOT(map()));
		tagMapper->setMapping(ui->actionExpandTag6, -6);
		connect(ui->actionSelectTag7, SIGNAL(triggered()), tagMapper, SLOT(map()));
		tagMapper->setMapping(ui->actionSelectTag7, 7);
		connect(ui->actionExpandTag7, SIGNAL(triggered()), tagMapper, SLOT(map()));
		tagMapper->setMapping(ui->actionExpandTag7, -7);
		connect(ui->actionSelectTag8, SIGNAL(triggered()), tagMapper, SLOT(map()));
		tagMapper->setMapping(ui->actionSelectTag8, 8);
		connect(ui->actionExpandTag8, SIGNAL(triggered()), tagMapper, SLOT(map()));
		tagMapper->setMapping(ui->actionExpandTag8, -8);
		connect(ui->actionSelectTag9, SIGNAL(triggered()), tagMapper, SLOT(map()));
		tagMapper->setMapping(ui->actionSelectTag9, 9);
		connect(ui->actionExpandTag9, SIGNAL(triggered()), tagMapper, SLOT(map()));
		tagMapper->setMapping(ui->actionExpandTag9, -9);
		connect(tagMapper, SIGNAL(mapped(int)), itTags->second, SLOT(selectTagSl(int)));
		connect(ui->actionUnExpandTag, SIGNAL(triggered()), itTags->second, SLOT(loadTagsSl()));
	}
	else
		qWarning()<<"unable to locate dwid_tags - some signals may remain unconnected";
	// make sure DCON connects first so that it will receive signals first - undocumented Qt behavior?
	actors.insert(ObjectMSP::value_type(QStringLiteral("000_DataContainer"), &DCON));
	for(auto it=actors.begin(); it!=actors.end(); ++it)
	{
		WidActor * act=dynamic_cast<WidActor *>(it->second);
		qCDebug(mainwindow)<<"connectAll"<<it->second->objectName();
		if(act)
		{
			act->connectAll(actors);
			act->setupSl();
			const QMetaObject* mo=it->second->metaObject();
			if(mo->indexOfSignal("imageActionGSig(int,ImageAction)")>=0)
			{
				qDebug()<<"mw: "<<it->second->objectName()<<"imageActionGSig(int,ImageAction)";
				connect(it->second, SIGNAL(imageActionGSig(int,ImageAction)), this, SLOT(imageActionGSl(int,ImageAction)));
			}
		}
	}
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::triggerAction(ImageAction action)
{
	switch(action)
	{
	case ImageAction::SELECT:
	case ImageAction::SELECT_MULTI:
	case ImageAction::MARK:
	case ImageAction::UNMARK:
	case ImageAction::MARK_MULTI:
	case ImageAction::DELETE:
		ui->actionDeleteImage->trigger();
		break;
	case ImageAction::SAVE_AS:
	case ImageAction::LABELS_CHANGED:
	case ImageAction::ATTRIB_CHANGED:
	case ImageAction::PIXEL_CHANGED:
		break;
	}
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	QSettings settings;
	settings.setValue("layout/geometry", saveGeometry());
	settings.setValue("layout/windowState", saveState());
	DCON.shutdown();
	qInfo()<<"closing MainWindow";
	QMainWindow::closeEvent(event);
}

void MainWindow::imageActionGSl(int imagePk, ImageAction action)
{
	switch(action)
	{
	case ImageAction::SELECT:
	{
		bool ena=(imagePk>0);
		singleImageSelected=ena;
		ui->actionTagAgain->setEnabled(singleImageSelected);
		ui->actionEditDescription->setEnabled(ena);
		ui->actionEditImage->setEnabled(ena);
		ui->actionDeleteImage->setEnabled(ena);
		ui->actionOpenGimp->setEnabled(ena);
		break;
	}
	case ImageAction::SELECT_MULTI:
		singleImageSelected=false;
		ui->actionTagAgain->setEnabled(false);
		ui->actionEditDescription->setEnabled(true);
		ui->actionEditImage->setEnabled(false);
		ui->actionDeleteImage->setEnabled(false);
		ui->actionOpenGimp->setEnabled(false);
		break;
	case ImageAction::MARK:
	case ImageAction::UNMARK:
	case ImageAction::MARK_MULTI:
	case ImageAction::DELETE:
	case ImageAction::SAVE_AS:
	case ImageAction::LABELS_CHANGED:
	case ImageAction::ATTRIB_CHANGED:
	case ImageAction::PIXEL_CHANGED:
		break;
	}
}

void MainWindow::about()
{
	DlgAbout dlg(this);
	dlg.exec();
}

void MainWindow::fromCamera()
{
	DlgFromCam dlg(this);
	dlg.exec();
}

void MainWindow::globalShortcutSl()
{
	QShortcut * sc=qobject_cast< QShortcut * >(sender());
	if(sc)
		qCDebug(mainwindow)<<"global Shortcut"<<sc->id();
	else
		qCDebug(mainwindow)<<"global anything";
}

void MainWindow::globalAmbShortcutSl()
{
	QShortcut * sc=qobject_cast< QShortcut * >(sender());
	if(!sc)
		return;
	qCDebug(mainwindow)<<"global ambigous Shortcut"<<sc->id();
}

void MainWindow::sourceSelSl(int sourcePk)
{
	ui->actionOpenSource->setEnabled(sourcePk>0);
}

