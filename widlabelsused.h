#ifndef WIDLABELSUSED_H
#define WIDLABELSUSED_H

#include "idxh.h"
#include <QWidget>
#include <QLabel>
#include <QPropertyAnimation>
#include <QColor>
#include "globalenum.h"
#include "widlabel.h"

Q_DECLARE_LOGGING_CATEGORY(widlabelsused)

class WidLabelsUsed : public QWidget
{
	Q_OBJECT
public:
	explicit WidLabelsUsed(QWidget *parent = 0);
	~WidLabelsUsed();
	virtual QSize sizeHint() const {return QSize(w, h);}
	virtual QSize minimumSizeHint() const {return minimumSize();}
public slots:
	void setImagePkSl(int imagePk);
protected:
	//virtual void paintEvent(QPaintEvent *event);
	virtual void resizeEvent(QResizeEvent *event);
	virtual void wheelEvent(QWheelEvent *event);
private:
	void calcSize();
	void reposChildren();
private slots:
	void labelCmdSl(int tagPk, LabelAction cmd);
	void animSl() {reposChildren();}
signals:
	void dispOffsetSig(int); // used for animated scroll
	void imageTagsChangedSig(int imagePk);
	void tagUseAsSearchSig(int tagPk);
private:
	Q_PROPERTY(int dOffset MEMBER dispOffset NOTIFY dispOffsetSig)
	int dispOffset=0, w=100, h=30, firstTag=0, pk=-1;
	TagWidMSP tags, orderedTags;
	MarkerWid *markUnder=nullptr, *markOver=nullptr;
	QLabel *markEmpty=nullptr;
	QPropertyAnimation *anim=nullptr;
};

#endif // WIDLABELSUSED_H
