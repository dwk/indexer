#ifndef DWIDIMAGESFAC_H
#define DWIDIMAGESFAC_H

#include "dockwidfactory.h"

namespace
{
QWidget * DockCreator(QWidget * parent)
{
	return new DWidImages(parent);
}
const char *techname="dwid_images";
bool dockWidRegistered=DockWidFactory::get().registerDockWid("Images", techname, Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea, Qt::BottomDockWidgetArea, DockCreator);
}
#endif // DWIDIMAGESFAC_H
