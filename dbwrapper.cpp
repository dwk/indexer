#include "dbwrapper.h"
#include <QSqlError>
#include <QSqlDriver>
#include <QSqlQuery>
#include "idxi.h"

Q_LOGGING_CATEGORY(dbwrapper, "dbwrapper")

DbWrapper::DbWrapper(const QString &dbName, QObject *parent) : QObject(parent), db_(QSqlDatabase::addDatabase("QSQLITE"))
{
	db_.setDatabaseName(dbName);
	if(db_.open())
	{
		running=true;
		QSqlQuery query;
		query.exec("PRAGMA foreign_keys = ON;");
		qCDebug(dbwrapper) << dbName <<"successfully opened, QuerySize ="<<db_.driver()->hasFeature(QSqlDriver::QuerySize);
		QStringList tbs=db_.driver()->tables(QSql::Tables);
		qCDebug(dbwrapper) << tbs;
	}
	else
		qCritical() << db_.lastError().text();
}

DbWrapper::~DbWrapper()
{
	qCDebug(dbwrapper)<<"closing db";
}

QString DbWrapper::getVersion()
{
	QString v="?";
	QSqlQuery q("select sqlite_version()");
	if(q.next())
		v=q.value(0).toString();
	return db_.driverName()+" v "+v;
}

QStringList DbWrapper::getAllTableNames() const
{
	return db_.driver()->tables(QSql::Tables);
}
