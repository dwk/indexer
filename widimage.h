#ifndef WIDIMAGE_H
#define WIDIMAGE_H

#include "idxh.h"
#include <QLabel>
#include "globalenum.h"

Q_DECLARE_LOGGING_CATEGORY(widimage)

class WidImage : public QLabel
{
	Q_OBJECT
public:
	explicit WidImage(QWidget *parent=0);
	explicit WidImage(const QString &text, QWidget *parent=0);
	void setPixmapExt(int imagePk, QPixmap *fullSize, const QPixmap *forDisplay = nullptr); // fullSize==nullptr enables thumbnail mode
	void setMark(bool mark);
	bool isMarked() const {return marked;}
public slots:
	void setPixmap(const QPixmap &pixmap); // will start thumbnail mode
protected:
	virtual void contextMenuEvent(QContextMenuEvent *event);
	virtual void mouseDoubleClickEvent(QMouseEvent *event);
	virtual void mouseMoveEvent(QMouseEvent *event);
	virtual void mousePressEvent(QMouseEvent *event);
	virtual void mouseReleaseEvent(QMouseEvent *event);
	void updateMagnified(const QPoint &rawPos);
	int pk=-1;
	bool marked=false;
	QPixmap origPix; // implicitly shared, isNull == thumbnail mode
	double scale=1.;
	QLabel *magnified=nullptr;
	bool firstever=true;
private slots:
	void imageActSl(ImageAction act, int actExt);
signals:
	void imageActionSig(int imagePk, ImageAction action);
};

#endif // WIDIMAGE_H
