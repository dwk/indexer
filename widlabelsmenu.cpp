#include "widlabelsmenu.h"
#include <QWheelEvent>
#include <QPainter>
#include <QPainterPath>
#include <QLabel>
#include <QMenu>
#include <QContextMenuEvent>
#include <QSqlQuery>
#include <QSqlError>
#include "idxi.h"

Q_LOGGING_CATEGORY(widlabelsmenu, "widlabelsmenu")

TagBand::TagBand(QWidget *parent) : QWidget(parent)
{
	markUnder=new MarkerWid(MarkerWid::Type::MoreAbove, this);
	markOver=new MarkerWid(MarkerWid::Type::MoreBelow, this);
	markEmpty=new QLabel("?", this);
	QSizePolicy sp(QSizePolicy::Fixed, QSizePolicy::Expanding);
	//sp.setHorizontalStretch(1);
	sp.setVerticalStretch(1);
	setSizePolicy(sp);
	anim=new QPropertyAnimation(this, "dOffset");
	anim->setDuration(500);
	anim->setLoopCount(1);
	anim->setEasingCurve(QEasingCurve::InOutCubic);
	connect(this, &TagBand::dispOffsetSig, this, &TagBand::animSl);
}

QSize TagBand::sizeHint() const
{
	//qDebug()<<objectName()<<"sizeHint"<<w<<h;
	return QSize(w, h);
}

void TagBand::setOperationMode(WidOperationMode newOpMode)
{
	opMode=newOpMode;
	for(auto it=tags.begin(); it!=tags.end(); it++)
	{
		it->second->setOperationMode(newOpMode);
	}
	calcSize();
}

/*
void TagBand::setOrientation(Qt::Orientation newOrientation)
{
	ori=newOrientation;
	if(ori==Qt::Horizontal)
	{
		QSizePolicy sp(QSizePolicy::Expanding, QSizePolicy::Fixed);
		sp.setHorizontalStretch(1);
		sp.setVerticalStretch(0);
		setSizePolicy(sp);
		markUnder->setType(MarkerWid::Type::MoreLeft);
		markOver->setType(MarkerWid::Type::MoreRight);
	}
	else
	{
		QSizePolicy sp(QSizePolicy::Fixed, QSizePolicy::Expanding);
		sp.setHorizontalStretch(0);
		sp.setVerticalStretch(0);
		setSizePolicy(sp);
		markUnder->setType(MarkerWid::Type::MoreAbove);
		markOver->setType(MarkerWid::Type::MoreBelow);
	}
	calcSize();
} */

void TagBand::setParentTagPk(int tagPk)
{
	for(auto it=tags.begin(); it!=tags.end(); it++)
	{
		it->second->deleteLater();
	}
	tags.clear();
	orderedTags.clear();
	firstTag=0;
	dispOffset=0;
	pk=tagPk;
	markUnder->hide();
	markOver->hide();
	if(!pk)
	{
		markEmpty->setText("---");
		markEmpty->raise();
		markEmpty->show();
		braced=false;
		calcSize();
		return;
	}
	QSqlQuery q(QString("select pk, val, description, deco, sequ from tags where parent%1 order by sequ asc").arg(pk<0?" is null":QString("=")+QString::number(pk)));
	//qDebug()<<objectName()<<"query"<<q.lastQuery()<<q.lastError();
	int ai=1;
	braced=false;
	while(q.next())
	{
		int tpk=q.value(0).toInt();
		bool hasChildren=false;
		QSqlQuery q2(QString("select count(1) from tags where parent=%1").arg(tpk));
		if(q2.next())
		{
			hasChildren=(q2.value(0).toInt()>0);
		}
		TagWid *tw=new TagWid(tpk, q.value(1).toString(), q.value(3).toString(), hasChildren, DCON.getMinTreeTagWidth(), ai, this);
		tw->setToolTip(q.value(2).toString());
		tw->show();
		connect(tw, &TagWid::labelCmdSig, this, &TagBand::labelCmdSl);
		tags.insert(TagWidMSP::value_type(tpk, tw));
		orderedTags.insert(TagWidMSP::value_type(q.value(4).toInt(), tw));
		++ai;
		braced=true; // true if at least on element
	}
	if(tags.size())
	{
		markEmpty->hide();
		markUnder->raise();
		markOver->raise();
	}
	else
	{
		//qDebug()<<objectName()<<"no tags";
		markEmpty->setText(tr("<no tags>"));
		markEmpty->raise();
		markEmpty->show();
	}
	calcSize();
}

int TagBand::getTagIndex2TagPk(int tagIndex) const
{
	if(tagIndex<0)
		return -1;
	auto it=orderedTags.begin();
	for(int i=0; i!=tagIndex-1 && it!=orderedTags.end(); ++it, ++i)
	if(it==orderedTags.end())
		return -1;
	return it->second->getPk();
}

void TagBand::expandedSubgroupInfo(int tagPk, bool expanded)
{
	auto it=tags.find(tagPk);
	if(it!=tags.end())
	{
		it->second->setExpandedDeco(expanded);
		braced=(!expanded);
		update(); // will update children anyway
	}
}

void TagBand::paintEvent(QPaintEvent *event)
{
	Q_UNUSED(event)
	if(braced)
	{
		QPainter painter(this);
		QPainterPath path;
		painter.setPen(Qt::red);
		path.moveTo(0, BRASIZE);
		path.lineTo(0, 0);
		path.lineTo(width()-1, 0);
		path.lineTo(width()-1, BRASIZE);
		path.moveTo(0, height()-BRASIZE-1);
		path.lineTo(0, height()-1);
		path.lineTo(width()-1, height()-1);
		path.lineTo(width()-1, height()-BRASIZE-1);
		painter.drawPath(path);
	}
}

void TagBand::resizeEvent(QResizeEvent *event)
{
	Q_UNUSED(event)
	//qDebug()<<"resizeEvent"<<event->oldSize()<<event->size();
	reposChildren();
	//qDebug()<<"resized"<<w<<h;
}

void TagBand::wheelEvent(QWheelEvent *event)
{
	if(!tags.size())
	{
		event->ignore();
		return;
	}
	//qDebug()<<"do wheelEvent"<<event->orientation()<<event->delta();
	firstTag=firstTag+(event->angleDelta().y()>0?1:-1)*(DCON.getWheelInverted(Qt::Vertical)?1:-1);
	if(firstTag<0)
		firstTag=0;
	else if(firstTag>=(int)tags.size())
		firstTag=tags.size()-1;
	auto it=orderedTags.begin();
	for(int i=0; i<firstTag; ++i)
		++it;
	if(anim->state()!=QAbstractAnimation::Running)
		anim->setStartValue(dispOffset);
	anim->setEndValue(-it->second->getPosHint()+1);
	anim->start();
	if(!firstTag)
		markUnder->hide();
	else
		markUnder->show();
}

void TagBand::animSl()
{
	//qDebug()<<"animSl"<<dispOffset;
	reposChildren();
}

void TagBand::labelCmdSl(int tagPk, LabelAction cmd)
{
	switch(cmd) // NONE=0, SELECTION_APPLY, SELECTION_FORCESET, SELECTION_FORCECLEAR, SOURCE_FORCESET, SOURCE_FORCECLEAR, MARKED_FORCESET, MARKED_FORCECLEAR, EXPAND, SEARCH
	{
	case LabelAction::NONE:
		qDebug()<<objectName()<<"TagBand::labelCmdSl NONE"<<tagPk<<enumLabelActionStr(cmd)<<pk;
		QApplication::beep();
		return;
	case LabelAction::EXPAND: // just forward expand requests
	{
		auto tit=tags.find(tagPk);
		if(tit!=tags.end() && tit->second->hasChildren())
		{
			//qDebug()<<"TagBand::labelCmdSl expanding"<<tagPk;
			emit tagSubgroupExpandSig(tagPk);
		}
		else
			qDebug()<<"TagBand::labelCmdSl invalid expanding request for"<<tagPk;
		return;
	}
	case LabelAction::SEARCH: // just forward
		emit labelActionGSig(tagPk, cmd);
		return;
	default:
		break;
	}
	if(opMode==WidOperationMode::APPLY_SIG)
	{
		if(cmd==LabelAction::SELECTION_APPLY)
		{
			emit labelActionGSig(tagPk, cmd);
			return;
		}
		qWarning()<<"TagBand::labelCmdSl invalid action in mode APPLY_SIG"<<enumLabelActionStr(cmd);
		return;
	}
	if(cmd==LabelAction::SELECTION_APPLY || cmd==LabelAction::SELECTION_FORCESET || cmd==LabelAction::SELECTION_FORCECLEAR)
	{ // based on selection
		const QList<int> & ipks=DCON.getSelectedImages();
		if(ipks.size()==0)
		{
			qWarning()<<objectName()<<"TagBand::labelCmdSl no selection"<<tagPk<<enumLabelActionStr(cmd);
			QApplication::beep();
			return;
		}
		QString pklistStr;
		{
			QStringList pklist;
			foreach(int ipk, ipks)
			{
				pklist<<QString::number(ipk);
			}
			pklistStr=pklist.join(',');
		}
		bool doDelete=false;
		if(cmd==LabelAction::SELECTION_APPLY)
		{ // toggle existing state, if homogeneus
			QSqlQuery qs(QString("select count(1) from taggings where image in (%1) and tag=%2").arg(pklistStr).arg(tagPk));
			if(qs.next())
			{
				int cnt=qs.value(0).toInt();
				if(!cnt)
					doDelete=false;
				else if(cnt==ipks.size())
				{
					if(DCON.getTagDoubleInsertIsDelete())
						doDelete=true;
					else
					{
						qWarning()<<objectName()<<"double tag insert ignored"<<qs.lastError().text()<<qs.lastQuery();
						QApplication::beep();
						return;
					}
				}
				else
				{
					QMessageBox::warning(this, tr("Inconsitent Tags"), tr("The selection contains images with and without this tag. It is unclear what you intend to do. Use forced apply / remove from context menu!"));
					return;
				}
			}
			else
			{
				qCritical()<<objectName()<<"tag check FAILED"<<qs.lastError().text()<<qs.lastQuery();
				return;
			}
		}
		else
			doDelete=(cmd==LabelAction::SELECTION_FORCECLEAR);
		if(doDelete)
		{
			QSqlQuery qd(QString("delete from taggings where image in (%1) and tag=%2").arg(pklistStr).arg(tagPk));
			if(qd.lastError().isValid())
			{
				qCritical()<<objectName()<<"tag delete selection FAILED"<<qd.lastError().text()<<qd.lastQuery();
				return;
			}
			//qDebug()<<objectName()<<"imageTagsChangedSig (double insert=delete)"<<ciPk;
			emit imageTagsChangedSig(ipks.size()==1?ipks.first():-1);
		}
		else
		{
			if(insertTagsSafe(ipks, pklistStr, tagPk))
			{
				//qDebug()<<objectName()<<"imageTagsChangedSig";
				emit imageTagsChangedSig(ipks.size()==1?ipks.first():-1);
			}
		}
		return;
	}
	if(cmd==LabelAction::SOURCE_FORCESET || cmd==LabelAction::SOURCE_FORCECLEAR)
	{ // based on source
		int spk=DCON.getCurrentSourcePk();
		if(spk<0)
		{
			qWarning()<<objectName()<<"TagBand::labelCmdSl no current source"<<tagPk<<enumLabelActionStr(cmd);
			QApplication::beep();
			return;
		}
		// sources may contain many already tagged images so ask again with facts
		QSqlQuery qc(QString("select count(1) from images where source=%1").arg(spk));
		if(!qc.next())
		{
			qCritical()<<objectName()<<"image count FAILED"<<qc.lastError().text()<<qc.lastQuery();
			return;
		}
		auto tag=tags.find(tagPk);
		if(tag==tags.end())
			return;
		if(QMessageBox::question(this, tr("Tag or Un-tag All"),
			tr("%4 %3 %5 all %1 images in %2?").arg(qc.value(0).toInt()).arg(DCON.getSource(spk).location(), tag->second->toString(),
			cmd==LabelAction::SOURCE_FORCESET?tr("Apply"):tr("Remove"), cmd==LabelAction::SOURCE_FORCESET?tr("to"):tr("from")), QMessageBox::No | QMessageBox::Yes, QMessageBox::Yes)==QMessageBox::No)
		{
			return;
		}
		if(cmd==LabelAction::SOURCE_FORCESET)
		{
			QSqlQuery qi(QString("insert into taggings(image, tag) \
					select i.pk, %2 from images i where i.source=%1 and i.pk not in \
					(select i.pk from images i inner join taggings t on i.pk=t.image where i.source=%1 and t.tag = %2)").arg(spk).arg(tagPk));
			if(qi.lastError().isValid())
			{
				qCritical()<<objectName()<<"tag source insert FAILED"<<qi.lastError().text()<<qi.lastQuery();
				return;
			}
		}
		else
		{
			QSqlQuery qd(QString("delete from taggings where tag = %2 and image in \
					(select i.pk from images i inner join taggings t on i.pk=t.image where i.source=%1 and t.tag = %2)").arg(spk).arg(tagPk));
			if(qd.lastError().isValid())
			{
				qCritical()<<objectName()<<"tag source delete FAILED"<<qd.lastError().text()<<qd.lastQuery();
				return;
			}
		}
		emit imageTagsChangedSig(-1); // not exactely true but we may assume that the selction is a subset of the current source
		return;
	}
	if(cmd==LabelAction::MARKED_FORCESET || cmd==LabelAction::MARKED_FORCECLEAR)
	{ // based on mark
		const QList<int> & ipks=DCON.getMarkedImages();
		if(ipks.size()==0)
		{
			qWarning()<<objectName()<<"TagBand::labelCmdSl no marks"<<tagPk<<enumLabelActionStr(cmd);
			QApplication::beep();
			return;
		}
		QString pklistStr;
		{
			QStringList pklist;
			foreach(int ipk, ipks)
			{
				pklist<<QString::number(ipk);
			}
			pklistStr=pklist.join(',');
		}
		if(cmd==LabelAction::MARKED_FORCECLEAR)
		{
			QSqlQuery qd(QString("delete from taggings where image in (%1) and tag=%2").arg(pklistStr).arg(tagPk));
			if(qd.lastError().isValid())
			{
				qCritical()<<objectName()<<"tag delete marks FAILED"<<qd.lastError().text()<<qd.lastQuery();
				return;
			}
			//qDebug()<<objectName()<<"imageTagsChangedSig (double insert=delete)"<<ciPk;
			emit imageTagsChangedSig(-2);
		}
		else
		{
			if(insertTagsSafe(ipks, pklistStr, tagPk))
			{
				//qDebug()<<objectName()<<"imageTagsChangedSig";
				emit imageTagsChangedSig(-2);
			}
		}
		return;
	}
}

bool TagBand::insertTagsSafe(const QList<int> &ipks, const QString &pklistStr, int tagPk)
{
	//avoid double inserts
	QSqlQuery qs2(QString("select image from taggings where image in (%1) and tag=%2").arg(pklistStr).arg(tagPk));
	if(qs2.lastError().isValid())
	{
		qCritical()<<objectName()<<"tag check2 FAILED"<<qs2.lastError().text()<<qs2.lastQuery();
		return false;
	}
	QList<int> ipksRed=ipks;
	while(qs2.next())
	{
		ipksRed.removeAll(qs2.value(0).toInt());
	}
	//qDebug()<<objectName()<<"reduced ipks"<<ipks<<"to"<<ipksRed;
	foreach(int ipk, ipksRed)
	{
		QSqlQuery qi(QString("insert into taggings (image, tag) values (%1, %2)").arg(ipk).arg(tagPk));
		if(qi.lastError().isValid())
		{
			qCritical()<<objectName()<<"tag insert FAILED"<<qi.lastError().text()<<qi.lastQuery();
			return false;
		}
	}
	return true;
}

void TagBand::calcSize()
{
	if(!tags.size())
	{
		if(markEmpty)
		{
			w=markEmpty->width()+2;
			h=markEmpty->height()*2;
		}
		else
		{
			w=10;
			h=10;
		}
		setMinimumSize(w,h);
		return;
	}
	int nh=tags.begin()->second->height()+1; // all tags must have same height
	h=nh*tags.size()+1;
	int nw=0;
	int i=0;
	for(auto it=orderedTags.begin(); it!=orderedTags.end(); ++it, ++i)
	{
		it->second->setPosHint(nh*i+1); // simple because of constant height...
		if(nw<it->second->width())
			nw=it->second->width();
	}
	w=nw+1;
	setMinimumSize(w, opMode==WidOperationMode::DATABASE_UPDATE?std::min(h, 3*nh+1):h);
	//qDebug()<<objectName()<<"setMinimumSize"<<minimumSize();
	reposChildren();
	updateGeometry();
}

void TagBand::reposChildren()
{
	markUnder->move(w/2-MARKERSIZE, 0);
	markOver->move(w/2-MARKERSIZE, height()-(MARKERSIZE+1));
	for(auto it=tags.begin(); it!=tags.end(); ++it)
	{
		it->second->move(1, dispOffset+it->second->getPosHint());
	}
	if(h+dispOffset>height())
	{
		//qDebug()<<"show!"<<h<<dispOffset<<height();
		markOver->show();
	}
	else
	{
		//qDebug()<<"hide!"<<h<<dispOffset<<height();
		markOver->hide();
	}
}

// ------------------------

WidLabelsMenu::WidLabelsMenu(QWidget *parent) : QWidget(parent)
{
	QSizePolicy sp;
	sp.setHeightForWidth(false);
	sp.setHorizontalPolicy(QSizePolicy::MinimumExpanding);
	//sp.setHorizontalStretch(2);
	sp.setVerticalPolicy(QSizePolicy::Expanding);
	setSizePolicy(sp);
	horizontalLayout=new QHBoxLayout(this);
	horizontalLayout->setSpacing(2);
	horizontalLayout->setMargin(0);
	horizontalLayout->setSizeConstraint(QLayout::SetMinimumSize);
	for(int i=0; i<LABELS_MENU_DEPTH; ++i)
	{
		TagBand * tb=new TagBand(this);
		tbs.push_back(tb);
		tb->setParentTagPk(i?0:-1); // empty or top level
		horizontalLayout->addWidget(tb);
		connect(tb, &TagBand::tagSubgroupExpandSig, this, [this, i](int tagPk) -> void {tagExpandSl(tagPk, i);});
		connect(tb, &TagBand::imageTagsChangedSig, this, [this](int imagePk) -> void {emit imageActionGSig(imagePk, ImageAction::LABELS_CHANGED);});
		connect(tb, &TagBand::labelActionGSig, this, &WidLabelsMenu::labelActionGSl); //[this](int labelPk) -> void {emit labelActionGSig(labelPk, LabelAction::SEARCH);});
	}
}

void WidLabelsMenu::setOperationMode(WidOperationMode newOpMode)
{
	opMode=newOpMode;
	/* does not work for vertical size
	switch(opMode)
	{
	case WidOperationMode::DATABASE_UPDATE:
		horizontalLayout->setSizeConstraint(QLayout::SetMinimumSize);
		break;
	case WidOperationMode::APPLY_SIG:
		horizontalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
		break;
	}*/
	for(auto it=tbs.begin(); it!=tbs.end(); ++it)
		(*it)->setOperationMode(newOpMode);
}

/*QSize WidLabelsMenu::sizeHint() const
{
	int w=0, h=0;
	for(auto it=tbs.begin(); it!=tbs.end(); ++it)
	{
		QSize s=(*it)->sizeHint();
		w+=s.width() + 2;
		h=max(h, s.height());
	}
	return QSize(w, h);
}*/

void WidLabelsMenu::resetSl()
{
	for(int i=tbs.size()-1; i>0; --i)
	{
		//int oldSubPk=;
		if(tbs.at(i)->getParentTagPk()>0)
		{
			//if(i==1) // otherwise we don't care
			//	tbs.at(i-1)->expandedSubgroupInfo(oldSubPk, false); we reload top anyway
			tbs.at(i)->setParentTagPk(0);
		}
	}
	actionBand=0;
	tbs.at(0)->setParentTagPk(-1); // reload top level
}

void WidLabelsMenu::selectTagSl(int tagIndex)
{
	int tagPk=tbs.at(actionBand)->getTagIndex2TagPk(abs(tagIndex));
	//qDebug()<<objectName()<<"selectTagSl"<<tagIndex<<tagPk;
	if(tagPk>0)
	{
		if(tagIndex>0)
			tbs.at(actionBand)->labelCmdSl(tagPk, LabelAction::SELECTION_APPLY);
		else
			tbs.at(actionBand)->labelCmdSl(tagPk, LabelAction::EXPAND);
	}
}

void WidLabelsMenu::tagExpandSl(int tagPk, int bandIndex)
{
	//qDebug()<<"WidLabelsMenu::tagExpandSl"<<tagPk<<bandIndex;
	if(bandIndex<0 || bandIndex>=(int)tbs.size()-1)
	{
		qWarning()<<objectName()<<"tagExpandSl invalid band"<<tagPk<<bandIndex;
		return;
	}
	actionBand=bandIndex+1;
	TagBand *tb=tbs.at(actionBand);
	for(int i=tbs.size()-1; i>bandIndex; --i)
	{
		int oldSubPk=tbs.at(i)->getParentTagPk();
		//qDebug()<<"EXPAND test"<<i<<oldSubPk;
		if(oldSubPk>=0)
		{
			tbs.at(i-1)->expandedSubgroupInfo(oldSubPk, false);
			//qDebug()<<"EXPAND revoke pk"<<i-1<<oldSubPk;
			if(i>actionBand)
			{
				//qDebug()<<"EXPAND empty level"<<i;
				tbs.at(i)->setParentTagPk(0);
			}
		}
	}
	tb->setParentTagPk(tagPk);
	tbs.at(bandIndex)->expandedSubgroupInfo(tagPk, true);
}

void WidLabelsMenu::labelActionGSl(int labelPk, LabelAction action)
{
	if(action==LabelAction::SELECTION_APPLY)
		lastSelectedLabelPk=labelPk;
	emit labelActionGSig(labelPk, action); // re-emit for the rest of the world
	if(action==LabelAction::SELECTION_APPLY)
		clearFocus();
}

