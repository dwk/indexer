#include "dlgsearchclausegps.h"
#include "sqlobject.h"
#include "idxi.h"
#include "ui_dlgsearchclausegps.h"

Q_LOGGING_CATEGORY(dlgsearchclausegps, "dlgsearchclausegps")

DlgSearchclauseGps::DlgSearchclauseGps(SqlGpsClause *gpsc) :
	QDialog(nullptr), ui(new Ui::DlgSearchclauseGps), gpsc(gpsc)
{
	ui->setupUi(this);
	GpsCoord gps=gpsc->val.value<GpsCoord>();
	ui->doubleLon->setValue(gps.lon);
	ui->doubleLat->setValue(gps.lat);
	if(gpsc->heiTol>=0)
	{
		ui->spinHeiMin->setValue(gps.height-gpsc->heiTol);
		ui->spinHeiMax->setValue(gps.height+gpsc->heiTol);
		ui->checkHei->setChecked(true);
	}
	else
	{
		ui->spinHeiMin->setValue(gps.height>100?gps.height-100:0);
		ui->spinHeiMax->setValue(gps.height+100);
		ui->checkHei->setChecked(false);
		ui->spinHeiMin->setEnabled(false);
		ui->spinHeiMax->setEnabled(false);
	}
	if(gpsc->lonTol>=0.)
	{
		ui->doubleLonTol->setValue(gpsc->lonTol);
		ui->checkLon->setChecked(true);
	}
	else
	{
		ui->doubleLonTol->setValue(100.);
		ui->checkLon->setChecked(false);
		ui->doubleLon->setEnabled(false);
		ui->doubleLonTol->setEnabled(false);
	}
	if(gpsc->latTol>=0.)
	{
		ui->doubleLatTol->setValue(gpsc->latTol);
		ui->checkLat->setChecked(true);
	}
	else
	{
		ui->doubleLatTol->setValue(100.);
		ui->checkLat->setChecked(false);
		ui->doubleLat->setEnabled(false);
		ui->doubleLatTol->setEnabled(false);
	}
}

void DlgSearchclauseGps::accept()
{
	GpsCoord gps(ui->doubleLon->value(), ui->doubleLat->value(), (ui->spinHeiMin->value()+ui->spinHeiMax->value())/2);
	gpsc->val.setValue(gps);
	gpsc->lonTol=(ui->checkLon->isChecked()?ui->doubleLonTol->value():-1.);
	gpsc->latTol=(ui->checkLat->isChecked()?ui->doubleLatTol->value():-1.);
	gpsc->heiTol=(ui->checkHei->isChecked()?(ui->spinHeiMax->value()-ui->spinHeiMin->value())/2:-1);
	QDialog::accept();
}

void DlgSearchclauseGps::on_checkLon_stateChanged(int state)
{
	ui->doubleLon->setEnabled(state!=Qt::Unchecked);
	ui->doubleLonTol->setEnabled(state!=Qt::Unchecked);
}

void DlgSearchclauseGps::on_checkLat_stateChanged(int state)
{
	ui->doubleLat->setEnabled(state!=Qt::Unchecked);
	ui->doubleLatTol->setEnabled(state!=Qt::Unchecked);
}

void DlgSearchclauseGps::on_checkHei_stateChanged(int state)
{
	ui->spinHeiMin->setEnabled(state!=Qt::Unchecked);
	ui->spinHeiMax->setEnabled(state!=Qt::Unchecked);
}

void DlgSearchclauseGps::on_spinHeiMin_valueChanged(int val)
{
	if(ui->spinHeiMax->value()<val)
		ui->spinHeiMax->setValue(val);
}

void DlgSearchclauseGps::on_spinHeiMax_valueChanged(int val)
{
	if(val<ui->spinHeiMin->value())
		ui->spinHeiMin->setValue(val);
}

