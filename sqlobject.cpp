#include "sqlobject.h"
#include <QLineEdit>
#include <QComboBox>
#include <QDateTimeEdit>
#include <QSqlQuery>
#include <QSqlError>
#include "dlgsearchclausedate.h"
#include "dlgsearchclausegps.h"
#include "widlabelsmenu.h"
#include "idxi.h"

Q_LOGGING_CATEGORY(sqlobject, "sqlobject")

// 0        1        2     3          4        5             6     7          8         9
// EQUAL=0, UNEQUAL, LESS, LESSEQUAL, GREATER, GREATEREQUAL, LIKE, LOGIC_AND, LOGIC_OR, SUBSETOF};
QString SqlObject::templRelation[]={QStringLiteral(u"%1 = %2"), QStringLiteral(u"%1 <> %2"),
									QStringLiteral(u"%1 < %2"), QStringLiteral(u"%1 <= %2"),
									QStringLiteral(u"%1 > %2"), QStringLiteral(u"%1 >= %2"), \
									QStringLiteral(u"%1 like %2"),
									QStringLiteral(u"%1 and %2"), QStringLiteral(u"%1 or %2"),
									QStringLiteral(u"%1 CCC %2")};
QString SqlObject::templRelationText[]={QStringLiteral(u"=="), QStringLiteral(u"<>"),
									QStringLiteral(u"<"), QStringLiteral(u"<="),
									QStringLiteral(u">"), QStringLiteral(u">="), \
									QStringLiteral(u"like"),
									QStringLiteral(u"AND"), QStringLiteral(u"OR"),
									QStringLiteral(u"⊆")};

SqlClause::SqlClause(SqlObject::TableRef tableRef, const QString &field, const QString &value, SqlObject::Relation relation) :
	field(field), val(value), relation(relation), quoteIt(true)
{
	commonCtor(tableRef);
	// let the user do that
	//if(relation==SqlObject::Relation::EQUAL && this->sval.contains('%'))
	//	relation=SqlObject::Relation::LIKE;
}

SqlClause::SqlClause(SqlObject::TableRef tableRef, const QString & field, const QVariant & value, SqlObject::Relation relation) :
	field(field), val(value), relation(relation), quoteIt(false)
{
	commonCtor(tableRef);
}

QString SqlClause::toSqlString(int &joinCardTag, int &joinCardTagMax)
{
	if(tagClause && !joinCardTagMax)
		joinCardTagMax=1;
	return templRelation[(int)relation].arg(field.arg((tagClause && joinCardTag)?(joinCardTag++):1),
			quoteIt?DataContainer::toSqlString(val.toString()):val.toString());
}

QWidget *SqlClause::createEditor(QWidget *parent, int col)
{
	if(col==1)
	{
		QComboBox *editor = new QComboBox(parent);
		editor->setFrame(false);
		for(int idx=0; idx<7; ++idx)
			editor->addItem(SqlObject::templRelationText[idx], idx);
		return editor;
	}
	else if(col==2)
	{
		QLineEdit *editor=new QLineEdit(parent);
		editor->setFrame(false);
		return editor;
	}
	return nullptr;
}

void SqlClause::setEditorData(QWidget *editor, int col)
{
	if(col==1)
	{
		QComboBox *ed=dynamic_cast<QComboBox *>(editor);
		if(!ed)
			return;
		ed->setCurrentText(templRelationText[(int)relation]);
	}
	else if(col==2)
	{
		QLineEdit *ed=dynamic_cast<QLineEdit *>(editor);
		if(!ed)
			return;
		ed->setText(val.toString());
	}
}

QVariant SqlClause::setModelData(QWidget *editor, int col)
{
	if(col==1)
	{
		QComboBox *ed=dynamic_cast<QComboBox *>(editor);
		if(!ed)
			return QVariant();
		relation=(SqlObject::Relation)ed->currentData().toInt();
		return QVariant(ed->currentText());
	}
	else if(col==2)
	{
		QLineEdit *ed=dynamic_cast<QLineEdit *>(editor);
		if(!ed)
			return QVariant();
		val=ed->text();
		return val;
	}
	return QVariant();
}

void SqlClause::commonCtor(SqlObject::TableRef tableRef)
{
	switch(tableRef)
	{
	case SqlObject::TableRef::IMAGES:
		imageClause=true;
		this->field=QStringLiteral(u"img%1.")+field;
		break;
	case SqlObject::TableRef::TAGGINGS:
		tagClause=true;
		this->field=QStringLiteral(u"tg%1.")+field;
		break;
	}
}



SqlTagClause::SqlTagClause(int tagPk) :
	SqlClause(SqlObject::TableRef::TAGGINGS, "tag", QVariant(tagPk))
{
	dispField=QStringLiteral(u"label");
	QSqlQuery q(QStringLiteral("select val from tags where pk=%1").arg(tagPk));
	if(q.next())
		dispVal=q.value(0).toString();
	else
		dispVal=QStringLiteral("<invalid>");
}

QString SqlTagClause::toSqlString(int &joinCardTag, int &joinCardTagMax)
{
	if(relation==SqlObject::Relation::SUBSETOF)
	{
		QStringList pks, toCheck;
		pks<<val.toString();
		toCheck=pks;
		while(toCheck.size())
		{
			QString pk=toCheck.first();
			qCDebug(sqlobject)<<"SqlTagClause is subset testing"<<pk;
			toCheck.erase(toCheck.begin());
			QSqlQuery q(QString("select pk from tags where parent=%1").arg(pk));
			while(q.next())
			{
				QString npk=q.value(0).toString();
				pks<<npk;
				toCheck<<npk;
			}
		}
		qCDebug(sqlobject)<<"SqlTagClause::toSqlString (is subset) used"<<pks;
		QStringList res;
		if(tagClause && !joinCardTagMax)
			joinCardTagMax=1;
		res<<field.arg((tagClause && joinCardTag)?(joinCardTag++):1);
		res<<" in (";
		res<<pks.join(", ");
		res<<")";
		return res.join("");
	}
	else
		return SqlClause::toSqlString(joinCardTag, joinCardTagMax);
}

QWidget *SqlTagClause::createEditor(QWidget *parent, int col)
{
	if(col==1)
	{
		QComboBox *editor = new QComboBox(parent);
		editor->setFrame(false);
		int idx=(int)SqlObject::Relation::EQUAL;
		editor->addItem(SqlObject::templRelationText[idx], idx);
		idx=(int)SqlObject::Relation::UNEQUAL;
		editor->addItem(SqlObject::templRelationText[idx], idx);
		idx=(int)SqlObject::Relation::SUBSETOF;
		editor->addItem(SqlObject::templRelationText[idx], idx);
		return editor;
	}
	else if(col==2)
	{
		WidLabelsMenu * wid=new WidLabelsMenu(parent);
		wid->setOperationMode(WidOperationMode::APPLY_SIG);
		//connect(wid, &WidLabelsMenu::imageActionGSig, this, &) delegates are not signal driven...
		return wid;
	}
	return nullptr;
}

void SqlTagClause::setEditorData(QWidget *editor, int col)
{
	if(col==1)
	{
		SqlClause::setEditorData(editor, 1);
	}
	else if(col==2)
	{
		WidLabelsMenu *ed=dynamic_cast<WidLabelsMenu *>(editor);
		if(!ed)
			return;
		ed->setLastSelectedLabelPk(val.toInt());
	}
}

QVariant SqlTagClause::setModelData(QWidget *editor, int col)
{
	if(col==1)
	{
		return SqlClause::setModelData(editor, 1);
	}
	else if(col==2)
	{
		WidLabelsMenu *ed=dynamic_cast<WidLabelsMenu *>(editor);
		if(!ed)
			return QVariant();
		val=ed->getLastSelectedLabelPk();
		QSqlQuery q(QStringLiteral("select val from tags where pk=%1").arg(ed->getLastSelectedLabelPk()));
		if(q.next())
		{
			dispVal=q.value(0).toString();
		}
		else
			dispVal=QStringLiteral("<db error>");
		return QVariant(dispVal);
	}
	return QVariant();
}


QString SqlDateTimeClause::dtformat(QStringLiteral(u"yyyy-MM-dd HH:mm"));

SqlDateTimeClause::SqlDateTimeClause(bool useFileDate) :
	SqlClause(SqlObject::TableRef::IMAGES, useFileDate?QStringLiteral(u"date_file"):QStringLiteral(u"date_exif"),
		QVariant(), SqlObject::Relation::GREATEREQUAL)
{
	quoteIt=true;
	QDateTime tst=QDateTime::currentDateTime();
	tst.setTime(QTime(0, 0));
	val=tst;
	updateDisp();
}

QString SqlDateTimeClause::toSqlString(int & , int &)
{ // we know that we are not a tag clause...
	return templRelation[(int)relation].arg(field.arg(1), DataContainer::toSqlString(val.toDateTime().toString(Qt::ISODate)));
}

bool SqlDateTimeClause::modalDialogUpdate()
{
	DlgSearchclauseDate dlg(this);
	return dlg.exec()==QDialog::Accepted;
}

void SqlDateTimeClause::updateDisp()
{
	dispField=field.contains(QStringLiteral(u"exif"))?QStringLiteral(u"exif date"):QStringLiteral(u"file date");
	dispVal=val.toDateTime().toString(dtformat);
}


SqlDescClause::SqlDescClause() :
	SqlClause(SqlObject::TableRef::IMAGES, "description", QString("text"))
{
	dispField=QStringLiteral(u"description");
}

QWidget *SqlDescClause::createEditor(QWidget *parent, int col)
{
	if(col==1)
	{
		QComboBox *editor = new QComboBox(parent);
		editor->setFrame(false);
		int idx=(int)SqlObject::Relation::EQUAL;
		editor->addItem(SqlObject::templRelationText[idx], idx);
		idx=(int)SqlObject::Relation::UNEQUAL;
		editor->addItem(SqlObject::templRelationText[idx], idx);
		idx=(int)SqlObject::Relation::LIKE;
		editor->addItem(SqlObject::templRelationText[idx], idx);
		return editor;
	}
	return SqlClause::createEditor(parent, col);
}


SqlSourceClause::SqlSourceClause() :
	SqlClause(SqlObject::TableRef::IMAGES, "source", QVariant(0))
{
	dispField=QStringLiteral("source");
	dispVal=QStringLiteral("<invalid>");
}

QWidget *SqlSourceClause::createEditor(QWidget *parent, int col)
{
	if(col==1)
	{
		QComboBox *editor = new QComboBox(parent);
		editor->setFrame(false);
		int idx=(int)SqlObject::Relation::EQUAL;
		editor->addItem(SqlObject::templRelationText[idx], idx);
		idx=(int)SqlObject::Relation::UNEQUAL;
		editor->addItem(SqlObject::templRelationText[idx], idx);
		return editor;
	}
	else if(col==2)
	{
		QComboBox *editor = new QComboBox(parent);
		editor->setFrame(false);
		QSqlQuery q(QStringLiteral("select pk, val from sources order by val"));
		qCDebug(sqlobject)<<"SqlSourceClause::createEditor"<<q.lastQuery()<<q.lastError();
		while(q.next())
		{
			editor->addItem(q.value(1).toString(), q.value(0));
		}
		return editor;
	}
	return SqlClause::createEditor(parent, col);
}

void SqlSourceClause::setEditorData(QWidget *editor, int col)
{
	if(col==1)
	{
		SqlClause::setEditorData(editor, 1);
	}
	else if(col==2)
	{
		QComboBox *ed=dynamic_cast<QComboBox *>(editor);
		if(!ed)
			return;
		int cs=ed->findText(dispVal);
		if(cs>=0)
			ed->setCurrentIndex(cs);
		else
			ed->setCurrentIndex(0);
	}
}

QVariant SqlSourceClause::setModelData(QWidget *editor, int col)
{
	if(col==1)
	{
		return SqlClause::setModelData(editor, 1);
	}
	else if(col==2)
	{
		QComboBox *ed=dynamic_cast<QComboBox *>(editor);
		if(!ed)
			return QVariant();
		val=ed->currentData();
		dispVal=ed->currentText();
		return QVariant(dispVal);
	}
	return QVariant();
}


SqlGpsClause::SqlGpsClause() :
	SqlClause(SqlObject::TableRef::IMAGES, QStringLiteral(""), QVariant())
{
	val.setValue(GpsCoord(16.24, 47.861, 282));
	dispField=QStringLiteral(u"GPS coord.");
	dispVal=QStringLiteral("(dialog)");
}

QString SqlGpsClause::toSqlString(int & , int &)
{ // we know that we are not a tag clause...
	QString lon, lat, hei;
	GpsCoord gps=val.value<GpsCoord>();
	double avglat=45.;
	int terms=0;
	if(latTol>=0.)
	{
		++terms;
		avglat=gps.lat;
		lat=QStringLiteral("(img1.latitude>%1 and img1.latitude<%2)").arg(gps.lat-latTol/111.).arg(gps.lat+latTol/111.);
	}
	if(lonTol>=0.)
	{
		++terms;
		if(avglat>89.9) avglat=89.9;
		if(avglat<-89.9) avglat=-89.9;
		double t=lonTol/(111.*cos(avglat/180.*3.1415927));
		lon=QStringLiteral("(img1.longitude>%1 and img1.longitude<%2)").arg(gps.lon-t).arg(gps.lon+t);
	}
	if(heiTol>=0.)
	{
		++terms;
		hei=QStringLiteral("(img1.height>%1 and img1.height<%2)").arg(gps.height-heiTol).arg(gps.height+heiTol);
	}
	if(!terms)
		return QStringLiteral("");
	else if(terms==1)
		return lon+lat+hei; // one of them will contain a valid term
	QString res("(");
	if(!lon.isEmpty())
		res+=lon;
	if(!lat.isEmpty())
	{
		if(res.size()>1)
			res+=QStringLiteral(" and ");
		res+=lat;
	}
	if(!hei.isEmpty())
	{
		if(res.size()>1)
			res+=QStringLiteral(" and ");
		res+=hei;
	}
	res+=QStringLiteral(")");
	return res;
}

bool SqlGpsClause::modalDialogUpdate()
{
	DlgSearchclauseGps dlg(this);
	return dlg.exec()==QDialog::Accepted;
}



SqlRelation::~SqlRelation()
{
	for(auto it=objs.begin(); it!=objs.end(); ++it)
	{
		delete *it;
	}
}

QString SqlRelation::toSqlString(int &, int &joinCardTagMax)
{
	bool cardCnt=(relation==SqlObject::Relation::LOGIC_AND);
	int lJCT=(cardCnt?1:0);
	size_t sz=objs.size();
	if(!sz)
		return QString();
	else if(sz==1)
	{
		lJCT=0; // doesn"t show as AND
		return (*(objs.begin()))->toSqlString(lJCT, joinCardTagMax);
	}
	auto it=objs.begin();
	QString res=(*it)->toSqlString(lJCT, joinCardTagMax);
	++it;
	for(; it!=objs.end(); ++it)
	{
		res=templRelation[(int)relation].arg(res, (*it)->toSqlString(lJCT, joinCardTagMax));
	}
	--lJCT; // undo last postincrment
	if(cardCnt && lJCT>joinCardTagMax)
		joinCardTagMax=lJCT;
	return QStringLiteral(u"(")+res+QStringLiteral(u")");
}

QStringList SqlRelation::toDisplayStrings()
{
	return QStringList()<<QStringLiteral(u"logic")<<templRelationText[(int)relation]<<"";
}

QWidget *SqlRelation::createEditor(QWidget *parent, int col)
{
	if(col==1)
	{
		QComboBox *editor = new QComboBox(parent);
		editor->setFrame(false);
		int idx=(int)SqlObject::Relation::LOGIC_AND;
		editor->addItem(SqlObject::templRelationText[idx], idx);
		idx=(int)SqlObject::Relation::LOGIC_OR;
		editor->addItem(SqlObject::templRelationText[idx], idx);
		return editor;
	}
	return nullptr;
}

void SqlRelation::setEditorData(QWidget *editor, int col)
{
	if(col==1)
	{
		QComboBox *ed=dynamic_cast<QComboBox *>(editor);
		if(!ed)
			return;
		ed->setCurrentText(templRelationText[(int)relation]);
	}
	/*QComboBox *ed=dynamic_cast<QComboBox *>(editor);
	if(!ed)
		return;
	switch(relation)
	{
	case Relation::LOGIC_AND:
		ed->setCurrentIndex(0);
		break;
	case Relation::LOGIC_OR:
		ed->setCurrentIndex(1);
		break;
	default:
		ed->setCurrentIndex(-1);
		break;
	}*/
}

QVariant SqlRelation::setModelData(QWidget *editor, int col)
{
	if(col==1)
	{
		QComboBox *ed=dynamic_cast<QComboBox *>(editor);
		if(!ed)
			return QVariant();
		relation=(SqlObject::Relation)(ed->currentData().toInt());
		return QVariant(ed->currentText());
	}
	return QVariant();
}

void SqlRelation::addObject(SqlObject *obj)
{
	objs.insert(obj);
}

void SqlRelation::removeObject(SqlObject *obj)
{
	objs.erase(obj);
	delete obj;
}

void SqlRelation::removeAllObjects()
{
	for(auto it=objs.begin(); it!=objs.end(); ++it)
	{
		delete *it;
	}
	objs.clear();
}

