#include "widlabelsused.h"
#include <QWheelEvent>
#include <QLabel>
#include <QMenu>
#include <QContextMenuEvent>
#include <QSqlQuery>
#include <QSqlError>
#include "idxi.h"

Q_LOGGING_CATEGORY(widlabelsused, "widlabelsused")

WidLabelsUsed::WidLabelsUsed(QWidget *parent) : QWidget(parent)
{
	markUnder=new MarkerWid(MarkerWid::Type::MoreLeft, this);
	markOver=new MarkerWid(MarkerWid::Type::MoreRight, this);
	markEmpty=new QLabel(tr("<no tags>"), this);
	anim=new QPropertyAnimation(this, "dOffset", this);
	anim->setDuration(500);
	anim->setLoopCount(1);
	anim->setEasingCurve(QEasingCurve::InOutCubic);
	connect(this, &WidLabelsUsed::dispOffsetSig, this, &WidLabelsUsed::animSl);
}

WidLabelsUsed::~WidLabelsUsed()
{

}

void WidLabelsUsed::setImagePkSl(int imagePk)
{
	for(auto it=tags.begin(); it!=tags.end(); it++)
	{
		it->second->deleteLater();
	}
	tags.clear();
	orderedTags.clear();
	firstTag=0;
	dispOffset=0;
	markUnder->hide();
	markOver->hide();
	pk=imagePk;
	if(pk<0)
	{
		//qDebug()<<objectName()<<pk;
		markEmpty->setText(pk==-1?tr("<no image>"):(pk==-2?tr("<multiple images>"):""));
		markEmpty->raise();
		markEmpty->show();
		calcSize();
		return;
	}
	QSqlQuery q(QString("select t.pk, t.val, t.description, t.deco from tags t inner join taggings tg on t.pk=tg.tag where tg.image=%1").arg(pk));
	//qDebug()<<objectName()<<"query"<<q.lastQuery()<<q.lastError();
	while(q.next())
	{
		//qDebug()<<objectName()<<"setSource adding TagWid"<<q.value(0).toInt()<<q.value(1).toString()<<q.value(2).toInt()<<q.value(3).toString();
		int tpk=q.value(0).toInt();
		TagWid *tw=new TagWid(tpk, q.value(1).toString(), q.value(3).toString(), false, 10, -1, this);
		tw->setToolTip(q.value(2).toString());
		tw->show();
		connect(tw, &TagWid::labelCmdSig, this, &WidLabelsUsed::labelCmdSl);
		tags.insert(TagWidMSP::value_type(tpk, tw));
		orderedTags.insert(TagWidMSP::value_type(tpk, tw));
	}
	if(tags.size())
	{
		markEmpty->hide();
		markUnder->raise();
		markOver->raise();
	}
	else
	{
		//qDebug()<<objectName()<<"no tags";
		markEmpty->setText(tr("<no tags>"));
		markEmpty->raise();
		markEmpty->show();
	}
	calcSize();
}

void WidLabelsUsed::resizeEvent(QResizeEvent *event)
{
	Q_UNUSED(event)
	//qDebug()<<"resizeEvent"<<event->oldSize()<<event->size();
	reposChildren();
	//qDebug()<<"resized"<<w<<h;
}

void WidLabelsUsed::wheelEvent(QWheelEvent *event)
{
	if(!tags.size())
	{
		event->ignore();
		return;
	}
	//qDebug()<<"do wheelEvent"<<event->orientation()<<event->delta();
	firstTag=firstTag+(event->angleDelta().y()>0?1:-1)*(DCON.getWheelInverted(Qt::Horizontal)?1:-1);
	if(firstTag<0)
		firstTag=0;
	else if(firstTag>=(int)tags.size())
		firstTag=tags.size()-1;
	auto it=orderedTags.begin();
	for(int i=0; i<firstTag; ++i)
		++it;
	if(anim->state()!=QAbstractAnimation::Running)
		anim->setStartValue(dispOffset);
	anim->setEndValue(-it->second->getPosHint()+1);
	anim->start();
	if(!firstTag)
		markUnder->hide();
	else
		markUnder->show();
}

void WidLabelsUsed::calcSize()
{
	if(!tags.size())
	{
		if(markEmpty)
		{
			w=markEmpty->width()+2;
			h=markEmpty->height()*2;
		}
		else
		{
			w=10;
			h=10;
		}
		setMinimumSize(w,h);
		return;
	}
	h=tags.begin()->second->height()+2; // all tags must have same height
	w=1;
	for(auto it=orderedTags.begin(); it!=orderedTags.end(); ++it)
	{
		//qDebug()<<objectName()<<"calcSize"<<it->second->toString()<<w<<it->second->width();
		it->second->setPosHint(w);
		w+=it->second->width();
		w+=1;
	}
	setMinimumSize(std::min(w, 2*w/(int)tags.size()+1), h);
	//qDebug()<<objectName()<<"calcSize"<<w<<h;
	reposChildren();
	updateGeometry();
}

void WidLabelsUsed::reposChildren()
{
	for(auto it=tags.begin(); it!=tags.end(); ++it)
	{
		//qDebug()<<objectName()<<"reposChildren"<<dispOffset<<it->second->getPosHint();
		it->second->move(dispOffset+it->second->getPosHint(), 1);
	}
	markUnder->move(0, h/2-MARKERSIZE);
	markOver->move(width()-(MARKERSIZE+1), h/2-MARKERSIZE);
	if(w+dispOffset>width())
	{
		//qDebug()<<"show!"<<h<<dispOffset<<height();
		markOver->show();
	}
	else
	{
		//qDebug()<<"hide!"<<h<<dispOffset<<height();
		markOver->hide();
	}
}

void WidLabelsUsed::labelCmdSl(int tagPk, LabelAction cmd)
{
	switch(cmd)
	{
	case LabelAction::SELECTION_APPLY:
	{
		QSqlQuery q(QString("delete from taggings where image=%1 and tag=%2").arg(pk).arg(tagPk));
		if(q.lastError().isValid())
			qCritical()<<objectName()<<"tag delete FAILED"<<q.lastError().text()<<q.lastQuery();
		//qDebug()<<objectName()<<"imageTagsChangedSig (delete)"<<pk;
		setImagePkSl(pk); // trigger reload - simpler than rearranging all the wids
		emit imageTagsChangedSig(pk);
		return;
	}
	case LabelAction::NONE:
	case LabelAction::SELECTION_FORCESET:
	case LabelAction::SELECTION_FORCECLEAR:
	case LabelAction::SOURCE_FORCESET:
	case LabelAction::SOURCE_FORCECLEAR:
	case LabelAction::MARKED_FORCESET:
	case LabelAction::MARKED_FORCECLEAR:
	case LabelAction::EXPAND:
		qDebug()<<objectName()<<"WidLabelsUsed::labelCmdSl invalid command"<<tagPk<<enumLabelActionStr(cmd)<<pk;
		QApplication::beep();
		return;
	case LabelAction::SEARCH:
		emit tagUseAsSearchSig(tagPk);
		return;
	}
}

