#ifndef IMAGEQUERYMODEL_H
#define IMAGEQUERYMODEL_H

#include "idxh.h"
#include <QAbstractTableModel>
#include <QSqlDatabase>
#include "sqlobject.h"
#include "globalenum.h"

Q_DECLARE_LOGGING_CATEGORY(imagequerymodel)

// todo concurrent inserts / deletes in images
// Changes in the db may invalidate the sortvec unnoticed.
// row2ImagePk may map to an image-pk which does not match
// the displayed data. Never use such a pk for write or delete
// operations.
// View-triggered actions should use the pk stored in a hidden
// column - a single row in the table should always show consistent
// data. --> save imagePk in row 0 and hide it with setColumnHidden
//
// An alternative would be to check for table modifications for every
// cache data read and reset the model if a change is detected.
class ImageQueryModel : public QAbstractTableModel
{
	Q_OBJECT
public:
	enum class Mode {EMPTY=0, SOURCE, SEARCH};
	explicit ImageQueryModel(QObject *parent = Q_NULLPTR, QSqlDatabase db = QSqlDatabase());
	virtual ~ImageQueryModel();
public:
	// QAbstractItemModel / QAbstractTableModel
	virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override {Q_UNUSED(parent) return mode==Mode::SEARCH?9:8;}
	virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override {Q_UNUSED(parent) return querySize;}
	virtual Qt::ItemFlags flags(const QModelIndex &index) const override;
	virtual QVariant data(const QModelIndex &index, int role=Qt::DisplayRole) const override;
	virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
	virtual void sort(int column, Qt::SortOrder order = Qt::AscendingOrder) override;
	//
	ImageQueryModel::Mode getMode() const {return mode;}
	void reloadData(const QModelIndex &index);
	void reload();
	bool checkSize(); // true if reload performed
	void setQuery(int sourceId);
	int getSourcePk() const {if(mode==Mode::SOURCE) return lastSourceId; return -1;}
	void setQuery(SqlObject *sql);
	int row2ImagePk(int row) const;
	int imagePk2Row(int imagePk) const;
	bool isDirty() const {return false;} // currently all edits commit instantly
public slots:
signals:
	void imageActionGSig(int imagePk, ImageAction action);
private:
	struct ImgData
	{
		//ImgData();
		int pk;
		QString filename, description, timestamp, GPS, camera, settings, orientation, size, source;
	};
	typedef std::vector<int> SortVec; // int at row = pk
	typedef std::map<int,ImgData> DataMap; // key = pk
	static int cacheMaxSize; // if sortVec gets larger than cacheMaxSize, part of the dataMap is dropped
	static int cacheChunkSize; // when starting a query at least cacheChunkSize are fetched (if available)
	bool isDataIndex(const QModelIndex &idx) const;
	void cacheDataUp(int upToRow);
	void cacheDataDown(int downToRow);
	Mode mode=Mode::EMPTY;
	QSqlDatabase db;
	int querySize=0; // total size of full query
	int querySkip=0; // true row of first entry in sortVec
	QString queryFields, query, sizeQuery, orderFieldStr, orderDirStr;
	SortVec sortVec;
	DataMap dataMap;
	int lastSourceId=-1;
};

#endif // IMAGEQUERYMODEL_H
