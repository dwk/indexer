#ifndef DWIDIMAGE_H
#define DWIDIMAGE_H

#include "idxh.h"
#include <list>
#include <QWidget>
#include <QPixmap>
#include "widactor.h"
#include "globalenum.h"

Q_DECLARE_LOGGING_CATEGORY(dwidimage)

class Image;
namespace Ui {
class DWidImage;
}

class DWidImage : public QWidget, public WidActor
{
	Q_OBJECT
public:
	explicit DWidImage(QWidget *parent = 0);
	~DWidImage();
	// WidActor
	virtual void connectAll(ObjectMSP &otherActors);
public slots:
	virtual void setupSl() {}
	//
public slots:
	void imageActionGSl(int imagePk, ImageAction action);
	void imageTagsChangedSl(int imagePk);
	void tagAgainSl();
protected:
	virtual void resizeEvent(QResizeEvent *event);
private:
	void fit2Size(int imagePk, QPixmap *pixmap, const QSize & sz);
	void changeLayout(bool thumbs);
	Ui::DWidImage *ui;
	int imagePk=-1, imgPkLastUpdated=-1;
	QList<int> markedPks;
	QPixmap brokenImage, noImage;
private slots:
	void scalePx();
	void theimageActionSl(int imagePk, ImageAction action);
	void on_radioScNo_toggled(bool checked);
	void on_radioScMatch_toggled(bool checked);
	void radioScMatchSl(DisplayPurpose oldPurpose, DisplayPurpose newPurpose);
	void on_radioScTiny_toggled(bool checked);
	void on_radioFullScreen_toggled(bool checked);
	void on_toolOpenMap_clicked();
	void on_toolOpenCv_clicked();
signals:
	void imageActionGSig(int imagePk, ImageAction action);
	//void imagesMarkedSig(QList< int > imagePks);
};

#endif // DWIDIMAGE_H
