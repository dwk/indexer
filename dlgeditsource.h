#ifndef DLGEDITSCOURCE_H
#define DLGEDITSCOURCE_H

#include "idxh.h"
#include <QDialog>

Q_DECLARE_LOGGING_CATEGORY(dlgeditsource)

class Source;
namespace Ui
{
	class DlgEditSource;
}

class DlgEditSource : public QDialog
{
	Q_OBJECT
public:
	explicit DlgEditSource(Source * baseSrc, QWidget *parent);
	explicit DlgEditSource(int sourcePk, QWidget *parent);
	~DlgEditSource();
	Source * getParent() const {return usedParent;}
public slots:
	virtual void accept();
private slots:
	void on_comboSourceType_currentTextChanged(const QString &text);
	void on_lineEdit_textEdited(const QString &);
	void on_pushBrowse_clicked();
	void on_pushRename_clicked();
private:
	Ui::DlgEditSource *ui;
	Source *src=nullptr; // null means create new, !null is edit
	Source *bSrc=nullptr;
	Source *usedParent=nullptr;
};
#endif // DLGEDITSCOURCE_H
