#ifndef DATACONTAINER_H
#define DATACONTAINER_H

#include "idxh.h"
#include <QObject>
#include "widactor.h"
#include <QMutex>
#include "globalenum.h"
#include "dbwrapper.h"
#include "source.h"
#include "image.h"
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/mem_fun.hpp>
#include <boost/multi_index/member.hpp>

Q_DECLARE_LOGGING_CATEGORY(datacontainer)

class MainWindow;
class DlgDisp;

class DataContainer : public QObject, public WidActor
{
	Q_OBJECT
public:
	static QString toSqlString(const QString &text, bool empty2Null = false);
	static QString toSqlString(const QDateTime &dt, bool invalid2Null = false);
	explicit DataContainer(QObject *parent = nullptr);
	~DataContainer();
	DataContainer *self() {return this;}
	// WidActor
	void connectAll(ObjectMSP &otherActors);
	void setupSl() {}
	//
	void setup(const QString configFile, MainWindow *mwin);
	void shutdown();
	MainWindow *getMainWindow() const {return mw;}
	bool getTagDoubleInsertIsDelete() const {return tagDoubleInsertIsDelete;}
	bool getVertWheelInverted() const {return vertWheelInverted;}
	bool getHorizWheelInverted() const {return horizWheelInverted;}
	bool getWheelInverted(Qt::Orientation orientation) const {return orientation==Qt::Vertical?vertWheelInverted:horizWheelInverted;}
	int getMinTreeTagWidth() const {return minTreeTagWidth;}
	int getThumbSize() const {return thumbSize;}
	int getTinySize() const {return tinySize;}
	int getMagnifierMag() const {return magMag;}
	int getMagnifierWidth() const {return magWidth;}
	const QString & getCmdGimp() const {return cmdGimp;}
	const QString & getCmdFiles() const {return cmdFiles;}
	DbWrapper * db() {return dbw;}
	Source & getSource(int sourcePk);
	void selectSource(const QString & fullpath);
	void selectSource(int sourcePk);
	int getCurrentSourcePk() {return currentSource;} // will return -1 if no source currently in use
	void dropSourceCache(int sourcePk);
	void dropSourceCacheAll();
	Image & getImage(int imagePk); // may return invalid Image
	Image & getImage(int sourcePk, const QString &imageName); // may return invalid Image, will always reload & re-cache = slow!
	void dropImageCache(int imagePk);
	const QList<int> & getSelectedImages() const {return selectedImages;}
	const QList<int> & getMarkedImages() const {return markedImages;}
	DlgDisp * getDisplayDialog();
public slots:
	void readPrefsSl();
	void sourceSelectedSl(int sourcePk);
	void querySelectedSl(void *v);
	void imageActionGSl(int imagePk, ImageAction action);
	void imageMultiSelection(QList<int> ipks);
	void imagesMarkedSl(QList<int> ipks);
signals:
	void imageActionGSig(int imagePk, ImageAction action);
	void sourceSelectedSig(int sourcePk);
protected:
	virtual void timerEvent(QTimerEvent *event);
private:
	void add2ImageCache(Image * img);
	struct SourceCache
	{
		SourceCache(int sourcePk, int parentPk, QPointer<Source> source) : tst(QDateTime::currentMSecsSinceEpoch()), pk(sourcePk), parent(parentPk), src(source) {}
		void ping() {tst=QDateTime::currentMSecsSinceEpoch();}
		qint64 tst;
		int pk, parent;
		QPointer<Source> src;
	};
	struct SourcePk{};
	struct ParentPk{};
	struct SourceTst{};
	typedef boost::multi_index::multi_index_container< SourceCache, boost::multi_index::indexed_by<
		boost::multi_index::ordered_unique< boost::multi_index::tag<SourcePk>, boost::multi_index::member< SourceCache, int, &SourceCache::pk> >,
		boost::multi_index::ordered_non_unique< boost::multi_index::tag<ParentPk>, boost::multi_index::member< SourceCache, int, &SourceCache::parent> >,
		boost::multi_index::ordered_non_unique< boost::multi_index::tag<SourceTst>, boost::multi_index::member< SourceCache, qint64, &SourceCache::tst> >
		>
	> SourceCaches;
	struct ImageCache
	{
		ImageCache(int imagePk, Image * image) : pk(imagePk), img(image) {}
		void ping() {img->ping();}
		qint64 getPing() const {return img->getPing();}
		int pk;
		QSharedPointer<Image> img;
	};
	struct ImagePk{};
	struct ImageTst{};
	typedef boost::multi_index::multi_index_container< ImageCache, boost::multi_index::indexed_by<
		boost::multi_index::ordered_unique< boost::multi_index::tag<ImagePk>, boost::multi_index::member< ImageCache, int, &ImageCache::pk> >,
		boost::multi_index::ordered_non_unique< boost::multi_index::tag<ImageTst>, boost::multi_index::const_mem_fun< ImageCache, qint64, &ImageCache::getPing> >
		>
	> ImageCaches;
	MainWindow *mw=nullptr;
	bool tagDoubleInsertIsDelete, vertWheelInverted, horizWheelInverted;
	int minTreeTagWidth, thumbSize, tinySize, magMag, magWidth;
	int cacheSizeSource, cacheSizeImage, cacheSizePixmap;
	QString cmdGimp, cmdFiles;
	DbWrapper *dbw=nullptr;
	SourceCaches sourceCaches;
	int currentSource=-1;
	ImageCaches imageCaches;
	QList<int> selectedImages;
	QList<int> markedImages;
	DlgDisp *dlgDisp=nullptr;
};

#endif // DATACONTAINER_H
