#include "dlgimageedit.h"
#include <QFocusEvent>
#include <QTimer>
#include <QScreen>
#include <QClipboard>
#include "ui_dlgimageedit.h"
#include "mainwindow.h"
#include "idxi.h"

Q_LOGGING_CATEGORY(dlgimageedit, "dlgimageedit")

DlgImageEdit::DlgImageEdit(int imagePk, QWidget *parent) :
	QDialog(parent), ui(new Ui::DlgImageEdit), img(DCON.getImage(imagePk)), clipboard(QGuiApplication::clipboard())
{
	ui->setupUi(this);
	ui->labelFileName->setText(img.getName());
	if(img.isValid())
	{
		ui->lineDescription->setText(img.getDesc());
		int idx=0;
		switch(img.getOrientation())
		{
		case Image::ExifOrientation::UP:
			idx=0;
			break;
		case Image::ExifOrientation::MIRROR_UP:
			idx=4;
			break;
		case Image::ExifOrientation::DOWN:
			idx=2;
			break;
		case Image::ExifOrientation::MIRROR_DOWN:
			idx=6;
			break;
		case Image::ExifOrientation::MIRROR_LEFT:
			idx=7;
			break;
		case Image::ExifOrientation::LEFT:
			idx=3;
			break;
		case Image::ExifOrientation::MIRROR_RIGHT:
			idx=5;
			break;
		case Image::ExifOrientation::RIGHT:
			idx=1;
			break;
		}
		ui->comboBox->setCurrentIndex(idx);
	}
	const GpsCoord &gps=img.getCoord();
	if(gps.hasValidLonLat())
	{
		ui->doubleGpsLon->setValue(gps.lon);
		ui->doubleGpsLat->setValue(gps.lat);
		ui->checkGpsLonLat->setChecked(true);
	}
	else
	{
		ui->doubleGpsLon->setEnabled(false);
		ui->doubleGpsLat->setEnabled(false);
		ui->checkGpsLonLat->setChecked(false);
	}
	if(gps.hasValidHeight())
	{
		ui->spinGpsHeight->setValue(gps.height);
		ui->checkGpsHeight->setChecked(true);
	}
	else
	{
		ui->spinGpsHeight->setEnabled(false);
	}
	ui->labelPk->setText(QString::number(imagePk));
	connect(ui->lineDescription, SIGNAL(textChanged(const QString)), this, SLOT(touchSl()));
	connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(touchSl()));
	connect(ui->doubleGpsLon, SIGNAL(valueChanged(double)), this, SLOT(touchSl()));
	connect(ui->doubleGpsLat, SIGNAL(valueChanged(double)), this, SLOT(touchSl()));
	connect(ui->spinGpsHeight, SIGNAL(valueChanged(int)), this, SLOT(touchSl()));
	ui->pushOk->setEnabled(false);
	connect(clipboard, &QClipboard::dataChanged, this, &DlgImageEdit::checkGpsPasteOptionSl);
	checkGpsPasteOptionSl();
}

DlgImageEdit::~DlgImageEdit()
{
	delete ui;
}

void DlgImageEdit::accept()
{
	img.setDesc(ui->lineDescription->text(), false);
	switch(ui->comboBox->currentIndex())
	{
	case 0:
		img.setOrientation(Image::ExifOrientation::UP, false);
		break;
	case 1:
		img.setOrientation(Image::ExifOrientation::RIGHT, false);
		break;
	case 2:
		img.setOrientation(Image::ExifOrientation::DOWN, false);
		break;
	case 3:
		img.setOrientation(Image::ExifOrientation::LEFT, false);
		break;
	case 4:
		img.setOrientation(Image::ExifOrientation::MIRROR_UP, false);
		break;
	case 5:
		img.setOrientation(Image::ExifOrientation::MIRROR_RIGHT, false);
		break;
	case 6:
		img.setOrientation(Image::ExifOrientation::MIRROR_DOWN, false);
		break;
	case 7:
		img.setOrientation(Image::ExifOrientation::MIRROR_LEFT, false);
		break;
	}
	GpsCoord c(ui->doubleGpsLon->value(), ui->doubleGpsLat->value(), ui->spinGpsHeight->value());
	if(!ui->checkGpsLonLat->isChecked())
		c.setLonLatInvalid();
	if(!ui->checkGpsHeight->isChecked())
		c.setHeightInvalid();
	img.setCoord(c, false);
	img.commit2Db();
	QDialog::accept();
}

void DlgImageEdit::on_toolTurnRight_clicked()
{
	int idx=ui->comboBox->currentIndex();
	//qDebug()<<"on_toolTurnRight_clicked"<<idx;
	--idx;
	if(idx<0 || idx==3)
		idx+=4;
	ui->comboBox->setCurrentIndex(idx);
	touchSl();
}

void DlgImageEdit::on_toolTurnLeft_clicked()
{
	int idx=ui->comboBox->currentIndex();
	//qDebug()<<"on_toolTurnLeft_clicked"<<idx;
	++idx;
	if(!(idx%4))
		idx-=4;
	ui->comboBox->setCurrentIndex(idx);
	touchSl();
}

void DlgImageEdit::on_toolTurnUpDown_clicked()
{
	int idx=ui->comboBox->currentIndex();
	int m=idx/4;
	//qDebug()<<"on_toolTurnUpDown_clicked"<<idx<<m;
	idx=(idx%4)+2;
	if(idx>3)
		idx-=4;
	ui->comboBox->setCurrentIndex(idx+m*4);
	touchSl();
}

void DlgImageEdit::checkGpsPasteOptionSl()
{
	QString t=clipboard->text();
	GpsCoord gps(t);
	qDebug()<<"checkGpsPasteOptionSl"<<t<<gps.hasValidLonLat();
	ui->toolGpsPaste->setEnabled(gps.hasValidLonLat());
}

void DlgImageEdit::on_toolGpsPaste_clicked()
{
	QString t=clipboard->text();
	GpsCoord gps(t);
	qDebug()<<"checkGpsPasteOptionSl"<<t<<gps.hasValidLonLat();
	if(gps.hasValidLonLat())
	{
		ui->checkGpsLonLat->setChecked(true);
		ui->doubleGpsLon->setValue(gps.lon);
		ui->doubleGpsLat->setValue(gps.lat);
		touchSl();
	}
}

void DlgImageEdit::on_checkGpsLonLat_stateChanged(int state)
{
	//qDebug()<<"on_checkGpsLonLat_stateChanged"<<state;
	bool st=(state==Qt::Checked);
	ui->doubleGpsLon->setEnabled(st);
	ui->doubleGpsLat->setEnabled(st);
	touchSl();
}

void DlgImageEdit::on_checkGpsHeight_stateChanged(int state)
{
	//qDebug()<<"on_checkGpsHeight_stateChanged"<<state;
	ui->spinGpsHeight->setEnabled(state==Qt::Checked);
	touchSl();
}

void DlgImageEdit::on_pushDelete_clicked()
{
	//qDebug()<<"on_pushDelete_clicked";
	MainWindow *mw=DCON.getMainWindow();
	if(mw)
	{
		mw->triggerAction(ImageAction::DELETE); // we boldly assume that the selecetd image is the one which underlies this dialog...
		QDialog::reject();
	}
}

void DlgImageEdit::touchSl()
{
	ui->pushOk->setEnabled(true);
}


