#include "mainwindow.h"
#include <QApplication>

// TODO bugs
//   * Search: label not equal does not work - you can't NOT on cartesian products
//   * ctrl-A does block the select all function in table views
// TODO features
//   * search for file name with wildcards
//   * check for undiscovered sub-folders, unmerged sources, images without tag
//   * progress indicator merge / tag all in source / ... (what is the slow step? DB / query execution?)
//	   see https://forum.qt.io/topic/118552/progress-indicator-for-slow-widget-creation/3
//	   DbWrapper should keep separate connections for GUI and each (named) worker thread
//   * use /proc/self/statm (1st entry = 6th number = data + stack in pages) in relation to /proc/meminfo (MemFree in kB) to propose cache size
//   * deep merge / compare / update: read all attributes from file and compare to db, related to Source::imageMergeLists
//   * Source::writeImageInfo, mostly write EXIF data, consider migrating to libexiv2, see https://exiv2.org/doc/exifcomment_8cpp-example.html
int main(int argc, char *argv[])
{
	QCoreApplication::setOrganizationName("dwk");
	QCoreApplication::setOrganizationDomain("effektlict.at");
	QCoreApplication::setApplicationName("Indexer");
	QApplication a(argc, argv);
	QLoggingCategory::setFilterRules(QStringLiteral(
//			"*=false\n"
			"datacontainer=false\n"
			"dbwrapper=false\n"
			"dlgabout=false\n"
			"dlgdisp=false\n"
			"dlgeditdesc=false\n"
			"dlgedittag=false\n"
			"dlgsearchclausedate=false\n"
			"dlgsearchclausegps=false\n"
			"dlgeditsource=false\n"
			"dockwidfactory=false\n" // won"t suppress "registering ..." infos - too early during init
			"dwidimage=false\n"
			"dwidimages=false\n"
			"dwidsearch=false\n"
			"dwidsources=false\n"
			"dwidtable=false\n"
			"dwidtagedit=false\n"
			"dwidtags=false\n"
			"image=false\n"
			"imagequerymodel=false\n"
			"sourcequerymodel=false\n"
//			"mainwindow=false\n"
			"source=false\n"
			"sqlobject=false\n"
			"widimage=false\n"
			"widlabel=false\n"
			"widlabelsmenu=false\n"
			"widlabelsused=false\n"
			"widtagtree=false\n"
										 ));
	MainWindow w;
	w.show();
	return a.exec();
}
