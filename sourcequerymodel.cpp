#include "sourcequerymodel.h"
#include <QSqlQuery>
#include <QSqlError>
#include "image.h"
#include "idxi.h"

Q_LOGGING_CATEGORY(sourcequerymodel, "sourcequerymodel")


SourceQueryModel::SourceQueryModel(QObject *parent, QSqlDatabase db) :
	QAbstractItemModel(parent), db(db)
{
}

SourceQueryModel::~SourceQueryModel()
{
}

int SourceQueryModel::rowCount(const QModelIndex &parent) const
{
	int ppk=-1;
	if(parent.isValid())
		ppk=parent.internalId();
	Source &src=DCON.getSource(ppk);
	int res=src.getChildCnt();
	//qCDebug(sourcequerymodel)<<"rowCount:"<<parent<<ppk<<res;
	return res;
}

QModelIndex SourceQueryModel::index(int row, int column, const QModelIndex &parent) const
{
	int ppk=-1;
	if(parent.isValid())
		ppk=parent.internalId();
	Source &src=DCON.getSource(ppk);
	if(!src.isValid())
	{
		qCDebug(sourcequerymodel)<<"index"<<ppk<<row<<column<<"invalid";
		return QModelIndex();
	}
	int cpk=src.getChildPk(row);
	//qCDebug(sourcequerymodel)<<"index"<<ppk<<row<<column<<cpk;
	return createIndex(row, column, (quintptr)cpk);
}

QModelIndex SourceQueryModel::parent(const QModelIndex &index) const
{
	int pk=-1;
	if(index.isValid())
		pk=index.internalId();
	Source &src=DCON.getSource(pk);
	if(!src.isValid())
		return QModelIndex();
	return createIndex(src.getRow(), 0, src.getParentPk());
}

Qt::ItemFlags SourceQueryModel::flags(const QModelIndex &index) const
{
	if(!index.isValid() || index.column()>=columnCount())
		return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
	if(index.column()==0)
		return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
	return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

QVariant SourceQueryModel::data(const QModelIndex &index, int role) const
{
	if(role!=Qt::DisplayRole && role!=Qt::EditRole && role!=Qt::ToolTipRole)
		return QVariant();
	Source * src=index2Src(index);
	if(!src)
		return QVariant();
	switch(role)
	{
	case Qt::DisplayRole:
	case Qt::EditRole:
	{
		switch(index.column())
		{
		case 0:
			return src->locationShort();
		case 1:
			return src->getComment();
		default:
			return QStringLiteral("col %1?").arg(index.column());
		}
		break;
	}
	case Qt::ToolTipRole:
		return QVariant(src->getPk());
	default:
		return QVariant();
	}
	return QVariant();
}

QVariant SourceQueryModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if(role!=Qt::DisplayRole)
		return QVariant();
	if(orientation==Qt::Horizontal)
		switch(section)
		{
		case 0:
			return tr("location");
		case 1:
			return tr("comment");
		default:
			return QStringLiteral("column %1").arg(section);
		}
	else
		return QString::number(section+1); // row numbers shall start at 1
}

bool SourceQueryModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	Source *src=index2Src(index);
	if(!src || role!=Qt::EditRole)
		return false;
	switch(index.column())
	{
	case 0:
	{
		// handeled through separate dialog
		// src->setLocation(value.toString());
		// emit imageActionGSig(pk, ImageAction::ATTRIB_CHANGED);
		break;
	}
	case 1:
	{
		src->setComment(value.toString(), true);
		break;
	}
	default:
		return false;
	}
	emit dataChanged(index, index, {role});
	return true;
}

QModelIndex SourceQueryModel::findSource(int sourcePk)
{
	Source &src=DCON.getSource(sourcePk);
	return createIndex(src.getRow(), 0, sourcePk);
}

int SourceQueryModel::index2Pk(const QModelIndex &idx) const
{
	if(idx.isValid())
		return idx.internalId();
	return -1;
}

void SourceQueryModel::reload(Source * src)
{
	beginResetModel();
	if(src)
		src->reloadChildren();
	else
		DCON.getSource(-1).reloadChildren();
	endResetModel();
}

Source *SourceQueryModel::index2Src(const QModelIndex &idx) const
{
	if(idx.isValid() && idx.column()<columnCount())
	{
		int spk=idx.internalId();
		return &DCON.getSource(spk);
	}
	return nullptr;
}
