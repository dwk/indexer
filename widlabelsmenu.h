#ifndef WIDLABELSMENU_H
#define WIDLABELSMENU_H

#include "idxh.h"
#include <QWidget>
#include <QLabel>
#include <QPropertyAnimation>
#include <QColor>
#include <QHBoxLayout>
#include "globalenum.h"
#include "widlabel.h"

Q_DECLARE_LOGGING_CATEGORY(widlabelsmenu)

class TagBand : public QWidget
{
	Q_OBJECT
public:
	explicit TagBand(QWidget *parent);
	virtual QSize sizeHint() const;
	virtual QSize minimumSizeHint() const {return minimumSize();}
	void setOperationMode(WidOperationMode newOpMode);
	int getParentTagPk() const {return (pk);}
	void setParentTagPk(int tagPk); // pk<0 loads top level, pk==0 clears band
	int getTagIndex2TagPk(int tagIndex) const;
	void expandedSubgroupInfo(int tagPk, bool expanded);
public slots:
	void labelCmdSl(int tagPk, LabelAction cmd);
protected:
	virtual void paintEvent(QPaintEvent *event);
	virtual void resizeEvent(QResizeEvent *event);
	virtual void wheelEvent(QWheelEvent *event);
signals:
	void dispOffsetSig(int); // used for animated scroll
	void imageTagsChangedSig(int imagePkOrSet); // -1= selected image(s), -2= marked image(s)
	void tagSubgroupExpandSig(int tagPk);
	void labelActionGSig(int labelPk, LabelAction action);
private slots:
	void animSl();
private:
	bool insertTagsSafe(const QList<int> &ipks, const QString &pklistStr, int tagPk);
	void calcSize();
	void reposChildren();
	Q_PROPERTY(int dOffset MEMBER dispOffset NOTIFY dispOffsetSig)
	WidOperationMode opMode=WidOperationMode::DATABASE_UPDATE;
	bool braced=false;
	int dispOffset=0, w=100, h=30, firstTag=0, pk=-1;
	TagWidMSP tags, orderedTags;
	MarkerWid *markUnder=nullptr, *markOver=nullptr;
	QLabel *markEmpty=nullptr;
	QPropertyAnimation *anim=nullptr;
};
typedef std::vector< TagBand * > TagBandVP;


#define LABELS_MENU_DEPTH 3

class WidLabelsMenu : public QWidget
{
	Q_OBJECT
public:
	explicit WidLabelsMenu(QWidget *parent = 0);
	//virtual QSize sizeHint() const {return QSize(400,400);}
	virtual QSize minimumSizeHint() const {return minimumSize();} // return QSize(200,200);
	//virtual QSize minimumSize() const {return QSize(200,200);}
	void setOperationMode(WidOperationMode newOpMode);
	int getLastSelectedLabelPk() const {return lastSelectedLabelPk;}
	void setLastSelectedLabelPk(int labelPk) {lastSelectedLabelPk=labelPk;}
public slots:
	void resetSl(); // close all sub-levels and reload top-level
	void selectTagSl(int tagIndex); // tagIndex is 1-based!!! tagIndex>0 selects, <0 expands
private slots:
	void tagExpandSl(int tagPk, int bandIndex);
	void labelActionGSl(int labelPk, LabelAction action);
private:
	QHBoxLayout *horizontalLayout;
	TagBandVP tbs;
	int actionBand=0;
	WidOperationMode opMode=WidOperationMode::DATABASE_UPDATE;
	int lastSelectedLabelPk=-1;
signals:
	void labelActionGSig(int labelPk, LabelAction action);
	void imageActionGSig(int imagePk, ImageAction action);
};

#endif // WIDLABELSMENU_H
