#include "dwidimage.h"
#include "dwidimage_fac.h"
#include "ui_dwidimage.h"
#include <QDesktopServices>
#include <QUrl>
#include <QFileDialog>
#include <QAction>
#include "source.h"
#include "dlgdisp.h"
#include "idxi.h"

Q_LOGGING_CATEGORY(dwidimage, "dwidimage")

DWidImage::DWidImage(QWidget *parent) :
	QWidget(parent),
	ui(new ::Ui::DWidImage), brokenImage(":/ic/res/BrokenImage.JPG"), noImage(":/ic/res/NoImage_en.jpg")
{
	setObjectName(techname);
	ui->setupUi(this);
	ui->frameMultiImage->hide();
	ui->progressBar->hide();
	ui->labelsUsed->setImagePkSl(-1);
	connect(ui->labelsUsed, &WidLabelsUsed::imageTagsChangedSig, this, &DWidImage::imageTagsChangedSl);
	connect(ui->theImage, &WidImage::imageActionSig, this, &DWidImage::theimageActionSl);
}

DWidImage::~DWidImage()
{
	delete ui;
}

void DWidImage::connectAll(ObjectMSP &otherActors)
{
	qDebug()<<"connectAll"<<objectName();
	for(auto it=otherActors.begin(); it!=otherActors.end(); ++it)
	{
		QObject *obj=it->second;
		if(obj->objectName()==objectName())
			continue;
		const QMetaObject* mo = obj->metaObject();
		/*if(obj->objectName()=="dwid_images")
		{
			for(int i=0; i<mo->methodCount(); ++i)
			{
				QMetaMethod mm=mo->method(i);
				qDebug()<<mm.typeName()<<mm.name()<<mm.methodSignature();
			}
		}*/
		if(mo->indexOfSignal("imageActionGSig(int,ImageAction)")>=0)
		{
			qDebug()<<"    "<<obj->objectName()<<"imageActionGSig(int,ImageAction)";
			connect(obj, SIGNAL(imageActionGSig(int,ImageAction)), this, SLOT(imageActionGSl(int,ImageAction)));
		}
	}
}

void DWidImage::imageActionGSl(int imagePk, ImageAction action)
{
	//qDebug()<<objectName()<<"imageActionGSl"<<imagePk<<enumImageActionStr(ia);
	switch(action)
	{
	case ImageAction::SELECT:
	{
		this->imagePk=imagePk;
		markedPks.clear();
		DCON.imagesMarkedSl(markedPks);
		changeLayout(false);
		if(imagePk<0)
		{
			ui->labelDesc->setText(tr("(no image selected)"));
			ui->labelInfo->setText("");
			ui->labelsUsed->setImagePkSl(imagePk);
			ui->toolOpenMap->setEnabled(false);
			ui->toolOpenCv->setEnabled(false);
		}
		else
		{
			Image & img=DCON.getImage(imagePk);
			ui->labelDesc->setText(img.getDesc());
			ui->labelInfo->setText(img.getInfo());
			ui->labelsUsed->setImagePkSl(imagePk);
			ui->toolOpenMap->setEnabled(img.getCoord().hasValidLonLat());
			ui->toolOpenCv->setEnabled(img.isValid());
		}
		//qDebug()<<"imageSelectedSl"<<imagePk;
		scalePx();
		break;
	} // SELECT
	case ImageAction::SELECT_MULTI:
	{
		int startTime=QTime::currentTime().msecsSinceStartOfDay();
		this->imagePk=-2;
		auto imagePks=DCON.getSelectedImages();
		if(imagePks.size()>64)
		{
			if(QMessageBox::question(this, tr("Large Selection"),
									 tr("You selected many (%1) images for mosaic display. It may take quite a while to load.").arg(imagePks.size()),
									 QMessageBox::Open | QMessageBox::Cancel, QMessageBox::Cancel)==QMessageBox::Cancel)
				return;
		}
		markedPks.clear();
		DCON.imagesMarkedSl(markedPks);
		changeLayout(true);
		ui->labelsUsed->setImagePkSl(-2);
		ui->labelDesc->setText("");
		ui->labelInfo->setText(tr("(multi select)"));
		int i=0;
		QList<WidImage *> existingLabels = ui->frameMultiImage->findChildren<WidImage *>(QString(), Qt::FindDirectChildrenOnly);
		//qDebug()<<"frameMultiImage has labels:"<<existingLabels.size();
		int thumbsPerLine=std::max(1, ui->frameMultiImage->width()/DCON.getThumbSize());
		int thumbWidth=DCON.getThumbSize();
		foreach(int ipk, imagePks)
		{
			if(!ui->progressBar->isVisible() && QTime::currentTime().msecsSinceStartOfDay()-startTime>1000)
				ui->progressBar->show();
			ui->progressBar->setValue(100*i/imagePks.size());
			Image &img=DCON.getImage(ipk);
			QPixmap *pxi=img.pixels(thumbWidth);
			if(!pxi)
				continue;
			WidImage *labelImg=nullptr;
			if(i>=existingLabels.size())
			{
				labelImg=new WidImage(ui->frameMultiImage);
				labelImg->setObjectName(QString("labelImg%1").arg(i));
				connect(labelImg, &WidImage::imageActionSig, this, &DWidImage::theimageActionSl);
			}
			else
			{
				labelImg=existingLabels.at(i);
				labelImg->setMark(false);
				ui->gridLayout->removeWidget(labelImg);
			}
			ui->gridLayout->addWidget(labelImg, i/thumbsPerLine, i%thumbsPerLine, 1, 1);
			labelImg->setPixmapExt(ipk, nullptr, pxi);
			labelImg->setToolTip(img.getInfo());
			// delete pxi; NO, is cached, don't delete
			++i;
		}
		for(; i<existingLabels.size(); ++i)
			delete existingLabels.at(i);
		ui->progressBar->hide();
		break;
	} // SELECT_MULTI
	case ImageAction::MARK:
	case ImageAction::UNMARK:
	case ImageAction::MARK_MULTI:
	case ImageAction::DELETE:
	case ImageAction::SAVE_AS:
		break;
	case ImageAction::LABELS_CHANGED:
	{
		if(imagePk>0 && (this->imagePk==imagePk || imagePk<0)) // act only if in single image mode
		{
			ui->labelsUsed->setImagePkSl(imagePk);
			imgPkLastUpdated=imagePk;
			qCDebug(dwidimage)<<"last updated image"<<imgPkLastUpdated;
		}
		break;
	}
	case ImageAction::ATTRIB_CHANGED:
	{
		if(imagePk>0 && this->imagePk==imagePk) // act only if in single image mode
			ui->labelDesc->setText(DCON.getImage(imagePk).getDesc());
		break;
	}
	case ImageAction::PIXEL_CHANGED:
		if(imagePk>0 && this->imagePk==imagePk) // act only if in single image mode
			scalePx();
		break;
	}
}

void DWidImage::imageTagsChangedSl(int imagePk)
{
	//qDebug()<<objectName()<<"imageTagsChangedSl"<<imagePk;
	emit imageActionGSl(imagePk, ImageAction::LABELS_CHANGED);
}

void DWidImage::tagAgainSl()
{
	if(imgPkLastUpdated<0 || imagePk<0)
		return;
	Image &refimg=DCON.getImage(imgPkLastUpdated);
	Image &img=DCON.getImage(imagePk);
	if(!refimg.isValid() || !img.isValid())
	{
		imgPkLastUpdated=-1;
		return;
	}
	qCDebug(dwidimage)<<objectName()<<"copying desc & tags"<<imgPkLastUpdated<<imagePk;
	img.setDesc(refimg.getDesc(), false);
	img.setTagPks(refimg.getTagPks());
	ui->labelsUsed->setImagePkSl(imagePk); // local update
	ui->labelDesc->setText(img.getDesc());
	emit imageActionGSig(imagePk, ImageAction::LABELS_CHANGED);
	emit imageActionGSig(imagePk, ImageAction::ATTRIB_CHANGED); // because we changed the description!
}

void DWidImage::resizeEvent(QResizeEvent *event)
{
	Q_UNUSED(event)
	scalePx();
}

void DWidImage::fit2Size(int imagePk, QPixmap *pixmap, const QSize &sz)
{
	const int border=12;
	int saw=sz.width()-border;
	int sah=sz.height()-border;
	if(pixmap->width()>saw || pixmap->height()>sah)
	{
		QPixmap scpx;
		if(pixmap->width()/saw > pixmap->height()/sah)
			scpx=pixmap->scaledToWidth(saw);
		else
			scpx=pixmap->scaledToHeight(sah);
		ui->theImage->setPixmapExt(imagePk, pixmap, &scpx);
	}
	else
		ui->theImage->setPixmapExt(imagePk, pixmap, nullptr);
}

void DWidImage::changeLayout(bool thumbs)
{
	if(thumbs)
	{
		DCON.getDisplayDialog()->hide();
		ui->theImage->setPixmap(QPixmap());
		ui->theImage->hide();
		ui->frameMultiImage->show();
		ui->radioScMatch->setEnabled(false);
		ui->radioScNo->setEnabled(false);
		ui->radioScTiny->setEnabled(false);
		ui->radioFullScreen->setEnabled(false);
		ui->toolOpenMap->setEnabled(false);
		ui->toolOpenCv->setEnabled(false);
	}
	else
	{
		ui->frameMultiImage->hide();
		ui->theImage->show();
		ui->radioScMatch->setEnabled(true);
		ui->radioScNo->setEnabled(true);
		ui->radioScTiny->setEnabled(true);
		ui->radioFullScreen->setEnabled(true);
	}
}

void DWidImage::scalePx()
{
	if(imagePk<0)
	{
		fit2Size(-1, &noImage, ui->scrollArea->size());
		return;
	}
	QPixmap *px=DCON.getImage(imagePk).pixels();
	if(!px)
	{
		ui->theImage->setPixmapExt(-2, &brokenImage);
		return;
	}
	if(ui->radioScMatch->isChecked())
	{ // never cache variable zoom factors - create it on the fly
		fit2Size(imagePk, px, ui->scrollArea->viewport()->size());
	}
	else if(ui->radioScTiny->isChecked() && px->width()>320)
	{
		ui->theImage->setPixmapExt(imagePk, px, DCON.getImage(imagePk).pixels(320));
	}
	else if(ui->radioFullScreen->isChecked())
	{
		ui->theImage->setPixmapExt(imagePk, nullptr);
		DCON.getDisplayDialog()->setPixmap(*px, true, DisplayPurpose::IMAGE);
	}
	else
		ui->theImage->setPixmapExt(imagePk, px);
}

void DWidImage::theimageActionSl(int imagePk, ImageAction action)
{
	switch(action)
	{
	case ImageAction::SELECT: // select this one
		emit imageActionGSig(imagePk, ImageAction::SELECT); // will update e.g. DCON
		imageActionGSl(imagePk, ImageAction::SELECT); // we are not connected to our own signal!
		break;
	case ImageAction::MARK: // mark
		if(markedPks.indexOf(imagePk)<0)
		{
			markedPks<<imagePk;
			DCON.imagesMarkedSl(markedPks);
		}
		break;
	case ImageAction::UNMARK: // unmark
	{
		if(markedPks.removeAll(imagePk))
			DCON.imagesMarkedSl(markedPks);
		break;
	}
	case ImageAction::MARK_MULTI: // "pk" denotes action (-2=none, -3=selection)
	{
		if(imagePk==-2)
		{
			markedPks.clear();
			QList<WidImage *> allThumbs = ui->frameMultiImage->findChildren<WidImage *>(QString(), Qt::FindDirectChildrenOnly);
			foreach(WidImage *th, allThumbs)
			{
				th->setMark(false);
			}
			DCON.imagesMarkedSl(markedPks);
		}
		else if(imagePk==-3)
		{
			markedPks=DCON.getSelectedImages();
			QList<WidImage *> allThumbs = ui->frameMultiImage->findChildren<WidImage *>(QString(), Qt::FindDirectChildrenOnly);
			foreach(WidImage *th, allThumbs)
			{
				th->setMark(true);
			}
			DCON.imagesMarkedSl(markedPks);
		}
		break;
	}
	case ImageAction::DELETE:
	{
		emit imageActionGSig(imagePk, ImageAction::DELETE);
		break;
	}
	case ImageAction::SAVE_AS:
	{
		Image & img=DCON.getImage(imagePk);
		QPixmap *pix=img.pixels();
		if(!img.isValid() || !pix || pix->isNull())
		{
			QMessageBox::warning(this, tr("Image Action failed"), tr("Invalid image %1 - unanble to save.").arg(img.getName()));
			return;
		}
		QSettings settings;
		QString defdir=settings.value("pref/localImageSaveDir", "~").toString();
		QString fn=QFileDialog::getSaveFileName(this, tr("Save Image File"), defdir+QDir::separator()+img.getName());
		if(fn.isEmpty())
			return;
		if(pix->save(fn))
		{
			QFileInfo fi(fn);
			settings.setValue("pref/localImageSaveDir", QVariant(fi.path()));
		}
		else
			QMessageBox::warning(this, tr("Image Action failed"), tr("Image %1 could not be saved to %2.").arg(img.getName(), fn));
		break;
	}
	default:
		qWarning()<<"DWidImage::theimageActionSl unhandled action"<<enumImageActionStr(action);
		break;
	}
}

void DWidImage::on_radioScNo_toggled(bool checked)
{
	if(!checked)
		return;
	scalePx();
}

void DWidImage::on_radioScMatch_toggled(bool checked)
{
	if(!checked)
		return;
	scalePx();
}

void DWidImage::radioScMatchSl(DisplayPurpose oldPurpose, DisplayPurpose newPurpose)
{
	qDebug()<<"DWidImage::radioScMatchSl"<<(int)oldPurpose<<(int)newPurpose;
	Q_UNUSED(newPurpose)
	if(oldPurpose!=DisplayPurpose::IMAGE || !ui->radioFullScreen->isChecked())
		return; // not our business
	ui->radioScMatch->setChecked(true);
}

void DWidImage::on_radioScTiny_toggled(bool checked)
{
	if(!checked)
		return;
	scalePx();
}

void DWidImage::on_radioFullScreen_toggled(bool checked)
{
	if(!checked)
		return;
	DlgDisp *d=DCON.getDisplayDialog();
	connect(d, &DlgDisp::purposeChangedSig, this, &DWidImage::radioScMatchSl, Qt::UniqueConnection);
	scalePx();
}

void DWidImage::on_toolOpenMap_clicked()
{
	if(imagePk<0)
		return;
	const GpsCoord c=DCON.getImage(imagePk).getCoord();
	if(!c.hasValidLonLat())
		return;
	// https://www.openstreetmap.org/?mlat=42.0368&mlon=-75.8127#map=15/42.0368/-75.8127&layers=N
	QString u("https://www.openstreetmap.org/?mlat=%1&mlon=%2#map=17/%1/%2&layers=N");
	//qDebug()<<"DWidImage::on_toolOpenMap_clicked longitude"<<c.lon<<"Latitude"<<c.lat;
	QDesktopServices::openUrl(QUrl(u.arg(c.lat).arg(c.lon)));
}

void DWidImage::on_toolOpenCv_clicked()
{
	if(imagePk<0)
		return;
	Image & img=DCON.getImage(imagePk);
	if(img.isValid())
	{
		DCON.getDisplayDialog()->setPixmap(img.process(), false, DisplayPurpose::PROCRES1);
	}
}
