#ifndef DLGDISP_H
#define DLGDISP_H

#include "idxh.h"
#include "globalenum.h"
#include <QDialog>

Q_DECLARE_LOGGING_CATEGORY(dlgdisp)

namespace Ui
{
	class DlgDisp;
}

class DlgDisp : public QDialog
{
	Q_OBJECT
public:
	explicit DlgDisp(QWidget *parent);
	~DlgDisp();
	void setPixmap(const QPixmap &pixmap, bool scale2Size, DisplayPurpose purpose);
	DisplayPurpose getPurpose() const {return p;}
public slots:
	virtual void accept() override;
protected:
	virtual void hideEvent(QHideEvent *event) override;
	virtual void mouseDoubleClickEvent(QMouseEvent *event) override;
	virtual void keyPressEvent(QKeyEvent *e) override;
private slots:
	void startFullScreen();
	void stopFullScreen();
private:
	Ui::DlgDisp *ui;
	bool fullScreen=false, fullScreenMagic=false;
	DisplayPurpose p=DisplayPurpose::UNDEF;
	QPoint oldPos;
	QSize oldSize;
signals:
	void purposeChangedSig(DisplayPurpose oldPurpose, DisplayPurpose newPurpose);
};
#endif // DLGDISP_H
