#include "dlgeditdesc.h"
#include <QFocusEvent>
#include "ui_dlgeditdesc.h"
#include "idxi.h"

Q_LOGGING_CATEGORY(dlgeditdesc, "dlgeditdesc")

bool ComboFilter::eventFilter(QObject *watched, QEvent *event)
{
	if(watched==this->watched && event->type()==QEvent::FocusIn)
	{
		QComboBox *cb=qobject_cast<QComboBox *>(watched);
		if(cb)
			target->setText(cb->currentText());
	}
	return false; // always foward
}



DlgEditDesc::DlgEditDesc(const QString &description, int count, bool identical, QWidget *parent) :
	QDialog(parent), ui(new Ui::DlgEditDesc), count(count)
{
	ui->setupUi(this);
	QSettings settings;
	ui->comboLastDescs->blockSignals(true);
	ComboFilter *filter=new ComboFilter(ui->comboLastDescs, ui->lineEdit, this);
	ui->comboLastDescs->installEventFilter(filter);
	for(int i=0; i<10; ++i)
	{
		QVariant val=settings.value(QStringLiteral("data/lastDescription%1").arg(i));
		if(!val.isValid())
			break;
		ui->comboLastDescs->addItem(val.toString());
	}
	ui->comboLastDescs->blockSignals(false);
	ui->lineEdit->setText(description);
	if(count>1)
	{
		ui->checkBox->setEnabled(true);
		if(identical)
		{
			ui->checkBox->setDisabled(true);
			ui->label->setText(tr("Changes apply to %1 images!").arg(count));
		}
		else
		{
			ui->checkBox->setEnabled(true);
			ui->checkBox->setChecked(true);
			checkChangedSl();
		}
	}
	else
	{
		ui->checkBox->setDisabled(true);
		ui->label->setText("");
	}
	connect(ui->checkBox, SIGNAL(stateChanged(int)), this, SLOT(checkChangedSl()));
}

DlgEditDesc::~DlgEditDesc()
{
	delete ui;
}

QString DlgEditDesc::getText()
{
	return ui->lineEdit->text();
}

bool DlgEditDesc::append()
{
	return ui->checkBox->isChecked();
}

void DlgEditDesc::accept()
{
	QSettings settings;
	int dataI=0;
	QString active=ui->lineEdit->text();
	settings.setValue(QStringLiteral("data/lastDescription%1").arg(dataI), active);
	++dataI;
	for(int i=0; i<ui->comboLastDescs->count() && dataI<10; ++i)
	{
		QString ct=ui->comboLastDescs->itemText(i);
		if(ct==active)
			continue;
		settings.setValue(QStringLiteral("data/lastDescription%1").arg(dataI), ct);
		++dataI;
	}
	QDialog::accept();
}

void DlgEditDesc::checkChangedSl()
{
	if(ui->checkBox->isChecked())
		ui->label->setText(tr("Text will be appended to %1 image descriptions!").arg(count));
	else
		ui->label->setText(tr("Text will OVERWRITE all %1 descriptions!").arg(count));
}

void DlgEditDesc::on_comboLastDescs_currentTextChanged(const QString &text)
{
	ui->lineEdit->setText(text);
}

