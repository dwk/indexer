#ifndef DLGEDITTAG_H
#define DLGEDITTAG_H

#include "idxh.h"
#include <QDialog>
#include <QColor>

Q_DECLARE_LOGGING_CATEGORY(dlgedittag)

class QTreeWidgetItem;
namespace Ui
{
	class DlgEditTag;
}

class DlgEditTag : public QDialog
{
	Q_OBJECT
public:
	explicit DlgEditTag(QTreeWidgetItem * twi, QWidget *parent);
	~DlgEditTag();
	//QString getText();
public slots:
	virtual void accept();
private:
	void colorSetup();
	Ui::DlgEditTag *ui;
	QTreeWidgetItem *twi;
	QColor colorText, colorBackground;
private slots:
	void on_pushColorText_clicked();
	void on_pushColorBackground_clicked();
	void on_pushColorReset_clicked();
};
#endif // DLGEDITTAG_H
