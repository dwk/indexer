#-------------------------------------------------
#
# Project created by QtCreator 2019-11-11T21:07:40
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

unix:!macx: LIBS += -lexif

INCLUDEPATH += /usr/include/opencv4
INCLUDEPATH += /usr/local/share/boost_1_72_0
LIBS += -lopencv_core -lopencv_imgcodecs -lopencv_imgproc
# -lopencv_highgui
# uneccessary -lopencv_ml -lopencv_gapi -lopencv_calib3d -lopencv_dnn -lopencv_features2d -lopencv_flann \
# -lopencv_objdetect -lopencv_photo -lopencv_stitching \
# -lopencv_videoio -lopencv_video

TARGET = Indexer
TEMPLATE = app

CONFIG(release, debug|release):QMAKE_POST_LINK = cp $$OUT_PWD/$$TARGET ~/bin

SOURCES += main.cpp\
	dlgdisp.cpp \
	dlgeditsource.cpp \
	dlgfromcam.cpp \
	dlgimageedit.cpp \
        mainwindow.cpp \
    dlgabout.cpp \
    dockwidfactory.cpp \
    dwidtable.cpp \
    dwidimage.cpp \
    datacontainer.cpp \
    dbwrapper.cpp \
    dwidsources.cpp \
    source.cpp \
    dwidimages.cpp \
    dwidtags.cpp \
    dwidsearch.cpp \
    image.cpp \
	sourcequerymodel.cpp \
    widimage.cpp \
    dlgeditdesc.cpp \
    dwidtagedit.cpp \
    widtagtree.cpp \
    dlgedittag.cpp \
    imagequerymodel.cpp \
    sqlobject.cpp \
    globalenum.cpp \
    dlgsearchclausedate.cpp \
    dlgsearchclausegps.cpp \
    gpscoord.cpp \
    widlabel.cpp \
    widlabelsused.cpp \
    widlabelsmenu.cpp

HEADERS  += mainwindow.h \
    dlgabout.h \
    dlgdisp.h \
    dlgeditsource.h \
    dlgfromcam.h \
    dlgimageedit.h \
    dockwidfactory.h \
    dwidtable_fac.h \
    dwidtable.h \
    idxh.h \
    idxi.h \
    sourcequerymodel.h \
    widactor.h \
    dwidimage_fac.h \
    dwidimage.h \
    datacontainer.h \
    singleton.h \
    dbwrapper.h \
    dwidsources_fac.h \
    dwidsources.h \
    source.h \
    dwidimages.h \
    dwidimages_fac.h \
    dwidtags_fac.h \
    dwidtags.h \
    dwidsearch_fac.h \
    dwidsearch.h \
    image.h \
    widimage.h \
    dlgeditdesc.h \
    dwidtagedit_fac.h \
    dwidtagedit.h \
    widtagtree.h \
    dlgedittag.h \
    imagequerymodel.h \
    sqlobject.h \
    globalenum.h \
    dlgsearchclausedate.h \
    dlgsearchclausegps.h \
    gpscoord.h \
    widlabel.h \
    widlabelsused.h \
    widlabelsmenu.h

FORMS    += mainwindow.ui \
    dlgabout.ui \
    dlgdisp.ui \
    dlgeditsource.ui \
    dlgfromcam.ui \
    dlgimageedit.ui \
    dwidtable.ui \
    dwidimage.ui \
    dwidsources.ui \
    dwidimages.ui \
    dwidtags.ui \
    dwidsearch.ui \
    dlgeditdesc.ui \
    dwidtagedit.ui \
    dlgedittag.ui \
    dlgsearchclausedate.ui \
    dlgsearchclausegps.ui

DISTFILES += \
    indexer.db.sql \
    res/icons8-camera-48.png \
    source_types.sql \
    tags.sql

RESOURCES += \
	resf.qrc
