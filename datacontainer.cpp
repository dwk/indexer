#include "datacontainer.h"
#include "dlgdisp.h"
#include "idxi.h"

Q_LOGGING_CATEGORY(datacontainer, "datacontainer")

using namespace ::boost;
using namespace ::boost::multi_index;

QString DataContainer::toSqlString(const QString &text, bool empty2Null)
{
	if(text.contains('\''))
	{
		QString v=text;
		v.replace('\'', QStringLiteral(u"''"));
		return QStringLiteral(u"'")+v+QStringLiteral(u"'");
	}
	if(empty2Null && (text.isEmpty() || text.isNull()))
		return QStringLiteral("NULL");
	return QStringLiteral(u"'")+text+QStringLiteral(u"'");
}

QString DataContainer::toSqlString(const QDateTime &dt, bool invalid2Null)
{
	if(invalid2Null && !dt.isValid())
		return QStringLiteral("NULL");
	return QStringLiteral(u"'")+dt.toString(Qt::ISODate)+QStringLiteral(u"'");
}

DataContainer::DataContainer(QObject *parent) : QObject(parent)
{
	qCDebug(datacontainer)<<"DataContainer ctor";
	setObjectName(QStringLiteral("DataContainer"));
}

DataContainer::~DataContainer()
{
}

void DataContainer::connectAll(ObjectMSP &otherActors)
{
	qDebug()<<"connectAll"<<objectName();
	for(auto it=otherActors.begin(); it!=otherActors.end(); ++it)
	{
		QObject *obj=it->second;
		if(obj->objectName()==objectName())
			continue;
		const QMetaObject* mo = obj->metaObject();
		/*if(obj->objectName()=="dwid_images")
		{
			for(int i=0; i<mo->methodCount(); ++i)
			{
				QMetaMethod mm=mo->method(i);
				qDebug()<<mm.typeName()<<mm.name()<<mm.methodSignature();
			}
		}*/
		if(mo->indexOfSignal("sourceSelectedSig(int)")>=0)
		{
			qDebug()<<"    "<<obj->objectName()<<"sourceSelectedSig(int)";
			connect(obj, SIGNAL(sourceSelectedSig(int)), this, SLOT(sourceSelectedSl(int)));
		}
		if(mo->indexOfSignal("querySelectedSig(void*)")>=0) // just used to clear the current source
		{
			qDebug()<<"    "<<obj->objectName()<<"querySelectedSig(void*)";
			connect(obj, SIGNAL(querySelectedSig(void*)), this, SLOT(querySelectedSl(void*)));
		}
		if(mo->indexOfSignal("imageActionGSig(int,ImageAction)")>=0)
		{
			qDebug()<<"    "<<obj->objectName()<<"imageActionGSig(int,ImageAction)";
			connect(obj, SIGNAL(imageActionGSig(int,ImageAction)), this, SLOT(imageActionGSl(int,ImageAction)));
		}
	}
}

void DataContainer::setup(const QString configFile, MainWindow *mwin)
{
	Q_UNUSED(configFile)
	mw=mwin;
	readPrefsSl();
	dbw=new DbWrapper("/home/dwk/data/Indexer/indexer.db", this);
	imageCaches.insert(ImageCache(-1, new Image(this))); // have an invalid Image at hand
	//images.insert(ImageMVQ::value_type(-1, new Image(this)));
}

void DataContainer::shutdown()
{
	if(dlgDisp)
		delete dlgDisp;
	delete dbw;
	dbw=nullptr;
	//submit srcs?
}

Source & DataContainer::getSource(int sourcePk)
{
	auto &pkIndex=sourceCaches.get<SourcePk>();
	auto bit=pkIndex.find(sourcePk);
	if(bit!=pkIndex.end())
	{
		if(bit->src)
		{
			//qCDebug(datacontainer)<<"getSource cache hit"<<sourcePk<<bit->tst;
			SourceCache sc=*bit;
			sc.ping();
			pkIndex.replace(bit, sc);
			//qCDebug(datacontainer)<<"getSource cache hit ping update"<<bit->tst;
			return *(bit->src);
		}
		qWarning()<<"cache hit on deleted Source"<<sourcePk;
		sourceCaches.erase(bit);
	}
	if((int)sourceCaches.size()==cacheSizeSource)
	{
		auto &tstIndex=sourceCaches.get<SourceTst>();
		qCDebug(datacontainer)<<"getSource cache drop"<<sourcePk<<sourceCaches.size()<<tstIndex.begin()->pk<<tstIndex.begin()->tst;
		tstIndex.erase(tstIndex.begin());
	}
	Source * src=new Source(sourcePk, this);
	sourceCaches.insert(SourceCache(sourcePk, src->getParentPk(), src));
	qCDebug(datacontainer)<<"getSource cache insert"<<sourcePk<<(src->isValid()?"":"INVALID");
	return *src;
}

void DataContainer::selectSource(const QString &fullpath)
{
	int sourcePk=Source::location2Pk(fullpath);
	if(sourcePk<=0)
		return;
	selectSource(sourcePk);
}

void DataContainer::selectSource(int sourcePk)
{
	sourceSelectedSl(sourcePk); // we are not connected to our own signal - explicit call
	emit sourceSelectedSig(sourcePk);
}

void DataContainer::dropSourceCache(int sourcePk)
{
	auto &pkIndex=sourceCaches.get<SourcePk>();
	auto bit=pkIndex.find(sourcePk);
	if(bit==pkIndex.end())
		return;
	qCDebug(datacontainer)<<"dropping source from chache"<<sourcePk;
	delete bit->src;
	pkIndex.erase(bit);
}

void DataContainer::dropSourceCacheAll()
{
	for(auto it=sourceCaches.begin(), eit=sourceCaches.end(); it!=eit; ++it)
		delete it->src;
	sourceCaches.clear();
}

Image & DataContainer::getImage(int imagePk)
{
	auto &pkIndex=imageCaches.get<ImagePk>();
	auto bit=pkIndex.find(imagePk);
	if(bit!=pkIndex.end())
	{
		if(bit->img)
		{
			ImageCache im=*bit;
			qCDebug(datacontainer)<<"getImage cache hit"<<imagePk<<im.img->getPing();
			im.img->ping();
			pkIndex.replace(bit, im);
			qCDebug(datacontainer)<<"getImage cache hit ping update"<<im.img->getPing();
			return *(bit->img);
		}
		qWarning()<<"cache hit on deleted Image"<<imagePk;
		imageCaches.erase(bit);
	}
	Image * img=new Image(imagePk, this);
	add2ImageCache(img);
	return *img;
}

Image &DataContainer::getImage(int sourcePk, const QString &imageName)
{
	Image * img=new Image(sourcePk, imageName, this);
	if(img->getPk()>=0)
	{
		dropImageCache(img->getPk());
		add2ImageCache(img);
	}
	return *img;
}

void DataContainer::dropImageCache(int imagePk)
{
	auto &pkIndex=imageCaches.get<ImagePk>();
	auto bit=pkIndex.find(imagePk);
	if(bit==pkIndex.end())
		return;
	qCDebug(datacontainer)<<"dropping image from chache"<<imagePk;
	pkIndex.erase(bit);
}

DlgDisp *DataContainer::getDisplayDialog()
{
	if(!dlgDisp)
	{
		dlgDisp=new DlgDisp((QWidget *)mw); // ugly cast just to silence the typecheck - don't want to include mainwindow.h
	}
	dlgDisp->show();
	return dlgDisp;
}

void DataContainer::readPrefsSl()
{
	QSettings settings;
	tagDoubleInsertIsDelete=settings.value("pref/tagDoubleInsertIsDelete", true).toBool();
	vertWheelInverted=settings.value("pref/vertWheelInverted", true).toBool();
	horizWheelInverted=settings.value("pref/horizWheelInverted", true).toBool();
	minTreeTagWidth=settings.value("pref/minTreeTagWidth", 80).toInt();
	thumbSize=settings.value("pref/thumbSize", 200).toInt();
	tinySize=settings.value("pref/tinySize", 320).toInt();
	magMag=settings.value("pref/magMag", 5).toInt();
	magWidth=settings.value("pref/magWidth", 60).toInt();
	cacheSizeSource=settings.value("perf/cacheSizeSource", 32).toInt();
	cacheSizeImage=settings.value("perf/cacheSizeImage", 64).toInt();
	cacheSizePixmap=settings.value("perf/cacheSizePixmap", 3).toInt();
	cmdGimp=settings.value("cmd/cmdGimp", QStringLiteral("gimp")).toString();
	cmdFiles=settings.value("cmd/cmdFiles", QStringLiteral("nautilus")).toString();
	Image::cachePxSize=cacheSizePixmap;
}

void DataContainer::sourceSelectedSl(int sourcePk)
{
	currentSource=sourcePk;
	qCDebug(datacontainer)<<"currentSource updated"<<sourcePk;
}

void DataContainer::querySelectedSl(void*v)
{
	Q_UNUSED(v)
	currentSource=-1;
	qCDebug(datacontainer)<<"currentSource cleared";
}

void DataContainer::imageActionGSl(int imagePk, ImageAction action)
{
	switch(action)
	{
	case ImageAction::SELECT:
		selectedImages.clear();
		if(imagePk>0)
			selectedImages<<imagePk;
		break;
	case ImageAction::SELECT_MULTI:
	case ImageAction::MARK:
	case ImageAction::UNMARK:
	case ImageAction::MARK_MULTI:
	case ImageAction::DELETE:
	case ImageAction::SAVE_AS:
	case ImageAction::LABELS_CHANGED:
	case ImageAction::ATTRIB_CHANGED:
	case ImageAction::PIXEL_CHANGED:
		break;
	}
}

void DataContainer::imageMultiSelection(QList<int> ipks)
{
	selectedImages=ipks;
	qCDebug(datacontainer)<<"imagesSelectedSl"<<ipks;
}

void DataContainer::imagesMarkedSl(QList<int> ipks)
{
	markedImages=ipks;
	emit imageActionGSl(-1, ImageAction::MARK_MULTI);
	qCDebug(datacontainer)<<"imagesMarkedSl"<<ipks;
}

void DataContainer::timerEvent(QTimerEvent *event)
{
	QObject::timerEvent(event);
}


void DataContainer::add2ImageCache(Image * img)
{
	if((int)imageCaches.size()==cacheSizeImage)
	{
		auto &tstIndex=imageCaches.get<ImageTst>();
		qCDebug(datacontainer)<<"add2ImageCache cache drop"<<img->getPk()<<imageCaches.size()<<tstIndex.begin()->pk<<tstIndex.begin()->img->getPing();
		// delete tstIndex.begin()->img; QSharedPointer does this
		tstIndex.erase(tstIndex.begin());
	}
	imageCaches.insert(ImageCache(img->getPk(), img));
	connect(img, &Image::pixChangedSig, [this](int imagePk){emit imageActionGSig(imagePk, ImageAction::PIXEL_CHANGED);});
	qCDebug(datacontainer)<<"getImage cache insert"<<img->getPk()<<img->getPing();
}
