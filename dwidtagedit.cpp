#include "dwidtagedit.h"
#include "dwidtagedit_fac.h"
#include "ui_dwidtagedit.h"
#include <QSqlQuery>
#include <QSqlError>
#include "dlgedittag.h"
#include "idxi.h"

Q_LOGGING_CATEGORY(dwidtagedit, "dwidtagedit")

DWidTagEdit::DWidTagEdit(QWidget *parent) :
	QWidget(parent),
	ui(new ::Ui::DWidTagEdit)
{
	setObjectName(techname);
	ui->setupUi(this);
	connect(ui->treeTags->selectionModel(), &QItemSelectionModel::currentChanged, this, &DWidTagEdit::updatePreviewSl);
	connect(ui->treeTags, &QTreeWidget::itemChanged, this, &DWidTagEdit::updatePreview2Sl);
	connect(ui->treeTags, &WidTagTree::somethingChangedSig, this, &DWidTagEdit::tagtreeChangedSl);
}

DWidTagEdit::~DWidTagEdit()
{
	delete ui;
	delete preview;
}

void DWidTagEdit::connectAll(ObjectMSP &otherActors)
{
	qDebug()<<"connectAll"<<objectName();
	for(auto it=otherActors.begin(); it!=otherActors.end(); ++it)
	{
		QObject *obj=it->second;
		if(obj->objectName()==objectName())
			continue;
		//const QMetaObject* mo = obj->metaObject();
		/*if(obj->objectName()=="dwid_images")
		{
			for(int i=0; i<mo->methodCount(); ++i)
			{
				QMetaMethod mm=mo->method(i);
				qDebug()<<mm.typeName()<<mm.name()<<mm.methodSignature();
			}
		}*/
		/*if(mo->indexOfSignal("imageSelectedSig(int)")>=0)
		{
			qDebug()<<"    "<<obj->objectName()<<"imageSelectedSig(int)";
			connect(obj, SIGNAL(imageSelectedSig(int)), this, SLOT(imageSelectedSl(int)));
		}*/
	}
}

void DWidTagEdit::setupSl()
{
	addSubItems(nullptr, 0, 0);
	updateButtonsSl();
}

void DWidTagEdit::updatePreviewSl(const QModelIndex &current, const QModelIndex &previous)
{
	//qDebug()<<objectName()<<"updatePreviewSl";
	Q_UNUSED(previous)
	bool ok=false;
	QModelIndex idx=ui->treeTags->model()->index(current.row(), 0, current.parent());
	int tagPk=ui->treeTags->model()->data(idx, Qt::UserRole).toInt(&ok);
	if(!ok)
	{
		qCritical()<<objectName()<<"updatePreviewSl invalid pk"<<ui->treeTags->model()->data(current, Qt::UserRole);
		return;
	}
	updatePreviewInt(tagPk, ui->treeTags->model()->data(idx, Qt::DisplayRole).toString(),
					 ui->treeTags->model()->data(idx, Qt::UserRole+1).toString());
}

void DWidTagEdit::updatePreview2Sl(QTreeWidgetItem *item, int column)
{
	Q_UNUSED(column)
	bool ok=false;
	int tagPk=item->data(0, Qt::UserRole).toInt(&ok);
	if(!ok)
	{
		qCritical()<<objectName()<<"updatePreview2Sl invalid pk"<<item->data(0, Qt::UserRole);
		return;
	}
	updatePreviewInt(tagPk, item->data(0, Qt::DisplayRole).toString(), item->data(0, Qt::UserRole+1).toString());
}

void DWidTagEdit::updatePreviewInt(int tagPk, QString val, QString deco)
{
	if(preview)
	{
		delete preview;
	}
	preview=new TagWid(tagPk, val, deco, false, DCON.getMinTreeTagWidth(), 0, ui->groupPreview);
	ui->groupPreview->layout()->addWidget(preview);
	preview->setVisible(true);
	updateButtonsSl();
}

void DWidTagEdit::on_pushAddSib_clicked()
{
	//qDebug()<<objectName()<<"on_pushAddSib_clicked";
	QTreeWidgetItem *tws=ui->treeTags->currentItem();
	QTreeWidgetItem *twp=nullptr;
	if(tws)
		twp=tws->parent();
	on_pushAdd(twp);
}

void DWidTagEdit::on_pushAddSub_clicked()
{
	//qDebug()<<objectName()<<"on_pushAddSub_clicked";
	QTreeWidgetItem *twp=ui->treeTags->currentItem();
	on_pushAdd(twp);
}

void DWidTagEdit::on_pushAdd(QTreeWidgetItem *parent)
{
	if(parent && parent->data(0, Qt::UserRole+2).toString()=="new")
	{
		QMessageBox::warning(this, tr("Uncommited Label"), tr("You try to add a sub-label to a label which was not yet commited to the database. Please commit in order to do so. (anticipatory insert not supported)"));
		return;
	}
	QTreeWidgetItem *twi=new QTreeWidgetItem(QStringList()<<tr("New Label")<<"");
	twi->setData(0, Qt::UserRole, -1); // pk
	twi->setData(0, Qt::UserRole+1, ""); // deco
	twi->setData(0, Qt::UserRole+2, QVariant("new")); // sql marker, redundant to pk
	twi->setIcon(0, QIcon(":/ic/res/icons8-add-48.png"));
	if(parent)
		parent->addChild(twi);
	else
		ui->treeTags->insertTopLevelItem(0, twi);
	setDirtySl();
}

void DWidTagEdit::on_pushEdit_clicked()
{
	QTreeWidgetItem *twi=ui->treeTags->currentItem();
	//qDebug()<<objectName()<<"on_pushEdit_clicked"<<twi;
	if(twi)
	{
		DlgEditTag dlg(twi, this);
		if(dlg.exec()==QDialog::Accepted)
		{
			qDebug()<<"WidTagTree::editSl Accepted";
			tagtreeChangedSl();
		}
	}
}

void DWidTagEdit::on_pushDelete_clicked()
{
	qDebug()<<objectName()<<"on_pushDelete_clicked";
	QTreeWidgetItem *twi=ui->treeTags->currentItem();
	if(twi)
	{// no style sheets in item text :-( "text-decoration:line-through"
		int ccnt=twi->childCount();
		int livingChildren=0;
		for(int i=0; i<ccnt; ++i)
		{
			qDebug()<<objectName()<<i<<ccnt;
			QTreeWidgetItem *ctwi=twi->child(i);
			if(ctwi && ctwi->data(0, Qt::UserRole+2).toString()!="deleted")
				++livingChildren;
		}
		if(livingChildren)
		{
			QMessageBox::warning(this, tr("Tag has Sub-Tags"), tr("The label you are trying to delete has %1 sub-tag(s) attached. Please delete these before deleting the parent tag. (recursive delete not supported)").arg(livingChildren));
			return;
		}
		QSqlQuery qcnt(QStringLiteral(u"select count(1) from taggings where tag=%1").arg(twi->data(0, Qt::UserRole).toString()));
		if(qcnt.next())
		{
			bool ok=false;
			int icnt=qcnt.value(0).toInt(&ok);
			if(icnt || !ok)
			{
				QMessageBox::warning(this, tr("Tag is Used"), tr("The tag you are trying to delete is used in %1 image(s). Please please un-tag in advance.").arg(qcnt.value(0).toString()));
				return;
			}
		}
		else
		{
			qWarning()<<"iterateItem tagged image count failed"<<qcnt.lastError().text()<<qcnt.lastQuery();
			return;
		}
		QString state=twi->data(0, Qt::UserRole+2).toString();
		if(state=="new")
		{
			delete twi;
		}
		else if(state=="deleted")
		{
			twi->setForeground(0, QBrush(Qt::black));
			twi->setForeground(1, QBrush(Qt::black));
			twi->setIcon(0, QIcon(":/ic/res/icons8-change-48.png"));
			twi->setData(0, Qt::UserRole+2, QVariant("touched")); // "new" can never get here
			setDirtySl();
		}
		else
		{
			twi->setForeground(0, QBrush(Qt::gray));
			twi->setForeground(1, QBrush(Qt::gray));
			twi->setIcon(0, QIcon(":/ic/res/icons8-delete-48.png"));
			twi->setData(0, Qt::UserRole+2, QVariant("deleted")); // sql marker
			setDirtySl();
		}
	}
}

void DWidTagEdit::on_pushCommit_clicked()
{
	//qDebug()<<objectName()<<"on_pushCommit_clicked";
	int rowcnt=ui->treeTags->topLevelItemCount();
	DeleteItemMMP deleteItems;
	SequSP sequUpdates;
	for(int r=0; r<rowcnt; ++r)
	{
		QTreeWidgetItem *twi=ui->treeTags->topLevelItem(r);
		iterateItem(twi, r, 0, sequUpdates, deleteItems);
	}
	for(auto dit=deleteItems.begin(); dit!=deleteItems.end(); ++dit)
	{
		QTreeWidgetItem *twi=dit->second;
		QSqlQuery q(QStringLiteral(u"delete from tags where pk=%1").arg(twi->data(0, Qt::UserRole).toString()));
		//qDebug()<<"delete loop"<<q.lastError().text()<<q.lastQuery();
		if(q.lastError().isValid())
			qCritical()<<objectName()<<"delete loop FAILED"<<q.lastError().text()<<q.lastQuery();
		else
			delete twi;
	}
	// re-write all sequ's in groups where a member was inserted, moved-to or upadated; no optimization
	for(auto sit=sequUpdates.begin(); sit!=sequUpdates.end(); ++sit)
	{
		//qDebug()<<"sequ update for"<<((*sit)?(*sit)->data(0, Qt::UserRole).toString():QStringLiteral("<top-level>"));
		int chcnt=(*sit)?(*sit)->childCount():ui->treeTags->topLevelItemCount();
		for(int ch=0; ch<chcnt; ++ch)
		{
			QTreeWidgetItem *twi=(*sit)?(*sit)->child(ch):ui->treeTags->topLevelItem(ch);
			QSqlQuery q(QStringLiteral(u"update tags set sequ=%1 where pk=%2").arg(ch+1).arg(twi->data(0, Qt::UserRole).toString()));
			//qDebug()<<"sequ update"<<twi->data(0, Qt::UserRole)<<ch+1;
			//qDebug()<<"sequ update"<<q.lastQuery();
			if(q.lastError().isValid())
			{
				qCritical()<<objectName()<<"updateSequ FAILED"<<q.lastError().text()<<q.lastQuery();
			}
		}
	}
	ui->pushCommit->setEnabled(false);
	ui->pushCommit->setStyleSheet("");
	ui->pushReset->setEnabled(false);
	ui->pushReset->setStyleSheet("");
	emit tagsChangedSig();
}

void DWidTagEdit::iterateItem(QTreeWidgetItem *item, int sequence, int level, SequSP &sequUpdates, DeleteItemMMP &deleteItems)
{
	QTreeWidgetItem *paritem=item->parent();
	QString parentPk=paritem?QString::number(paritem->data(0, Qt::UserRole).toInt()):QStringLiteral("null");
	QString state=item->data(0, Qt::UserRole+2).toString();
	if(state=="new")
	{
		QSqlQuery q(QStringLiteral(u"insert into tags (parent, sequ, val, description, deco) values (%1, %2, '%3', '%4', '%5')").\
					arg(parentPk).arg(sequence+1).arg(item->data(0, Qt::DisplayRole).toString(), item->data(1, Qt::DisplayRole).toString(),
					item->data(0, Qt::UserRole+1).toString()));
		//qDebug()<<"iterateItem"<<q.lastError().text()<<q.lastInsertId().toInt()<<q.lastQuery();
		if(!q.lastError().isValid())
		{
			item->setData(0, Qt::UserRole, q.lastInsertId().toInt());
			item->setData(0, Qt::UserRole+2, QStringLiteral("pristine"));
			item->setIcon(0, QIcon());
			sequUpdates.insert(paritem);
		}
	}
	else if(state=="touched")
	{
		QSqlQuery q(QStringLiteral(u"update tags set parent = %1 , sequ=%2, val='%3', description='%4', deco='%5' where pk=%6").\
					arg(parentPk).arg(sequence+1).arg(item->data(0, Qt::DisplayRole).toString(), item->data(1, Qt::DisplayRole).toString(),
														   item->data(0, Qt::UserRole+1).toString(), item->data(0, Qt::UserRole).toString()));
		//qDebug()<<"iterateItem"<<q.lastError().text()<<q.lastQuery();
		if(!q.lastError().isValid())
		{
			item->setData(0, Qt::UserRole+2, QStringLiteral("pristine"));
			item->setIcon(0, QIcon());
			sequUpdates.insert(paritem); // if parents changed, the old group doesn't need an update
		}
	}
	else if(state=="deleted")
	{
		deleteItems.insert(DeleteItemMMP::value_type(-1*level, item)); // delete can use forward iterator
	}
	int chcnt=item->childCount();
	for(int ch=0; ch<chcnt; ++ch)
	{
		QTreeWidgetItem *twi=item->child(ch);
		iterateItem(twi, ch, level+1, sequUpdates, deleteItems);
	}
}

void DWidTagEdit::on_pushReset_clicked()
{
	delete preview;
	preview=nullptr;
	ui->treeTags->clear();
	setupSl();
	ui->pushCommit->setEnabled(false);
	ui->pushCommit->setStyleSheet("");
	ui->pushReset->setEnabled(false);
	ui->pushReset->setStyleSheet("");
}

void DWidTagEdit::tagtreeChangedSl()
{
	QTreeWidgetItem *twi=ui->treeTags->currentItem();
	if(twi)
	{
		QString sqlState=twi->data(0, Qt::UserRole+2).toString();
		if(sqlState=="deleted")
			QApplication::beep();
		else if(sqlState!="new")
		{
			twi->setData(0, Qt::UserRole+2, QVariant("touched")); // sql marker
			twi->setIcon(0, QIcon(":/ic/res/icons8-change-48.png"));
		}
		// do nothing on sqlState=="new"
	}
	setDirtySl();
}

void DWidTagEdit::setDirtySl()
{
	ui->pushCommit->setEnabled(true);
	ui->pushCommit->setStyleSheet("background-color:#aaff7f;");
	ui->pushReset->setEnabled(true);
	ui->pushReset->setStyleSheet("background-color:#ffaa7f;");
	updateButtonsSl();
}

void DWidTagEdit::updateButtonsSl()
{
	QTreeWidgetItem *twi=ui->treeTags->currentItem();
	if(twi)
	{
		QString sqlState=twi->data(0, Qt::UserRole+2).toString();
		if(sqlState=="new")
		{
			ui->pushAddSib->setEnabled(true);
			ui->pushAddSub->setEnabled(false);
			ui->pushEdit->setEnabled(true);
			ui->pushDelete->setText(tr("Delete"));
			ui->pushDelete->setEnabled(true);
		}
		else if(sqlState=="touched" || sqlState=="pristine")
		{
			ui->pushAddSib->setEnabled(true);
			ui->pushAddSub->setEnabled(twi->data(0, Qt::UserRole+3).toInt()<2);
			ui->pushEdit->setEnabled(true);
			ui->pushDelete->setText(tr("Delete"));
			ui->pushDelete->setEnabled(true);
		}
		else if(sqlState=="deleted")
		{
			ui->pushAddSib->setEnabled(true);
			ui->pushAddSub->setEnabled(false);
			ui->pushEdit->setEnabled(false);
			ui->pushDelete->setText(tr("Un-Delete"));
			ui->pushDelete->setEnabled(true);
		}
	}
	else
	{
		ui->pushAddSib->setEnabled(true);
		ui->pushAddSub->setEnabled(false);
		ui->pushEdit->setEnabled(false);
		ui->pushDelete->setText(tr("Delete"));
		ui->pushDelete->setEnabled(false);
	}
}

void DWidTagEdit::addSubItems(QTreeWidgetItem *twp, int tagPk, int level)
{
	QString query;
	if(level)
		//                            0   1     2    3            4
		query=QStringLiteral(u"select pk, sequ, val, description, deco from tags where parent=%1 order by sequ asc").arg(tagPk);
	else
		query=QStringLiteral(u"select pk, sequ, val, description, deco from tags where parent is null order by sequ desc");
	QSqlQuery q(query);
	if(q.lastError().isValid())
	{
		qCritical()<<objectName()<<"DWidTagEdit::addSubItems select FAILED"<<q.lastQuery()<<q.lastError();
		if(level)
			twp->addChild(new QTreeWidgetItem(QStringList()<<"DB error"<<q.lastError().text()<<QString::number(tagPk)));
		else
			ui->treeTags->insertTopLevelItem(0, new QTreeWidgetItem(QStringList()<<"DB error"<<q.lastError().text()));
		//QMessageBox::critical(this, tr("Database Error"), tr("Error during Delete (Query: %1, Error: %2)").arg(q.lastQuery(), q.lastError().text()));
		return;
	}
	while(q.next())
	{
		QTreeWidgetItem *twi=new QTreeWidgetItem(QStringList()<<q.value(2).toString()<<q.value(3).toString());
		twi->setData(0, Qt::UserRole, q.value(0)); // pk
		twi->setData(0, Qt::UserRole+1, q.value(4)); // deco
		twi->setData(0, Qt::UserRole+2, QVariant("pristine")); // sql marker
		twi->setData(0, Qt::UserRole+3, QVariant(level)); // level
		if(level<2)
			addSubItems(twi, q.value(0).toInt(), level+1);
		if(level)
			twp->addChild(twi);
		else
			ui->treeTags->insertTopLevelItem(0, twi);
	}
}

