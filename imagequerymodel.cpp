#include "imagequerymodel.h"
#include <QSqlQuery>
#include <QSqlError>
#include "image.h"
#include "idxi.h"

Q_LOGGING_CATEGORY(imagequerymodel, "imagequerymodel")

int ImageQueryModel::cacheMaxSize=100;
int ImageQueryModel::cacheChunkSize=10;


ImageQueryModel::ImageQueryModel(QObject *parent, QSqlDatabase db) :
	QAbstractTableModel(parent), db(db), orderFieldStr("img1.pk"), orderDirStr("asc")
{
}

ImageQueryModel::~ImageQueryModel()
{
}

Qt::ItemFlags ImageQueryModel::flags(const QModelIndex &index) const
{
	if(!isDataIndex(index))
		return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
	if(index.column()!=1 && index.column()!=3)
		return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
	return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

QVariant ImageQueryModel::data(const QModelIndex &index, int role) const
{
	if(!isDataIndex(index))
		return QVariant();
	switch(role)
	{
	case Qt::DisplayRole:
	case Qt::EditRole:
	{
		int pk=row2ImagePk(index.row());
		auto pit=dataMap.find(pk);
		if(pit==dataMap.end())
		{
			qCritical()<<"ImageQueryModel::data invalid data map"<<pk<<index<<querySkip<<sortVec.size();
			return QVariant(QStringLiteral("dataMap error"));
		}
		switch(index.column())
		{
		case 0:
			//qDebug()<<"data"<<index.row()<<pk;
			return pit->second.filename;
		case 1:
			return pit->second.description;
		case 2:
			return pit->second.timestamp;
		case 3:
			return pit->second.GPS;
		case 4:
			return pit->second.camera;
		case 5:
			return pit->second.settings;
		case 6:
			return pit->second.orientation;
		case 7:
			return pit->second.size;
		case 8:
			return pit->second.source;
		default:
			return QStringLiteral("col %1?").arg(index.column());
		}
		break;
	}
	case Qt::ToolTipRole:
		if(index.column())
			return QVariant();
		else
			return QVariant(row2ImagePk(index.row()));
		break;
	default:
		return QVariant();
	}
	return QVariant();
}

QVariant ImageQueryModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if(role!=Qt::DisplayRole)
		return QVariant();
	if(orientation==Qt::Horizontal)
		// filename description timestamp GPS camera settings orientation size
		switch(section)
		{
		case 0:
			return tr("filename");
		case 1:
			return tr("description");
		case 2:
			return tr("timestamp");
		case 3:
			return tr("GPS");
		case 4:
			return tr("camera");
		case 5:
			return tr("settings");
		case 6:
			return tr("orientation");
		case 7:
			return tr("size");
		case 8:
			return tr("source");
		default:
			return QStringLiteral("column %1").arg(section);
		}
	else
		return QString::number(section+1); // row numbers shall start at 1
}

bool ImageQueryModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	if(!isDataIndex(index) || role!=Qt::EditRole)
		return false;
	int pk=row2ImagePk(index.row());
	auto pit=dataMap.find(pk);
	if(pit==dataMap.end())
	{
		qCritical()<<"ImageQueryModel::setData invalid data map"<<pk<<index<<querySkip<<sortVec.size();
		return false;
	}
	Image &img=DCON.getImage(pk);
	switch(index.column())
	{
	case 1:
		pit->second.description=value.toString();
		img.setDesc(pit->second.description, true);
		emit imageActionGSig(pk, ImageAction::ATTRIB_CHANGED);
		break;
	case 3: // GPS coords
	{
		pit->second.GPS=value.toString();
		GpsCoord gps(pit->second.GPS);
		if(!pit->second.GPS.isEmpty() && !gps.hasValidLonLat() && !gps.hasValidHeight())
			QMessageBox::warning(nullptr, tr("Invalid Coordinate"), tr("Use longitude/latitude(height) OR longitude/latitude OR Hheight. Examples: 9.16484/41.38836(49) or 9.16484/41.38836 or H2300"));
		img.setCoord(gps, true);
		emit imageActionGSig(pk, ImageAction::ATTRIB_CHANGED);
		break;
	}
	default:
		return false;
	}
	emit dataChanged(index, index, {role});
	return true;
}

void ImageQueryModel::sort(int column, Qt::SortOrder order)
{
	qCDebug(imagequerymodel)<<"sort"<<column<<(int)order;
	if(order==Qt::AscendingOrder)
		orderDirStr=QStringLiteral("asc");
	else
		orderDirStr=QStringLiteral("desc");
	switch(column)
	{
	case 0:
		orderFieldStr=QStringLiteral("img1.val COLLATE NOCASE");
		break;
	case 1:
		orderFieldStr=QStringLiteral("img1.description COLLATE NOCASE");
		break;
	case 2:
		orderFieldStr=QStringLiteral("img1.date_exif, img1.date_file");
		break;
	case 4:
		orderFieldStr=QStringLiteral("img1.camera COLLATE NOCASE");
		break;
	case 6:
		orderFieldStr=QStringLiteral("img1.orientation");
		break;
	case 7:
		orderFieldStr=QStringLiteral("img1.size");
		break;
	default:
		QApplication::beep();
		orderFieldStr=QStringLiteral("img1.pk");
		break;
	}
	reload();
}

void ImageQueryModel::reloadData(const QModelIndex &index)
{
	int pk=row2ImagePk(index.row());
	auto pit=dataMap.find(pk);
	if(pit==dataMap.end())
	{
		qCritical()<<"ImageQueryModel::reloadData invalid data map"<<pk<<index<<querySkip<<sortVec.size();
		return;
	}
	Image &img=DCON.getImage(pk);
	pit->second.description=img.getDesc();
	pit->second.GPS=img.getCoord().toString();
	pit->second.orientation=Image::orientationStr(img.getOrientation());
	emit dataChanged(index, index, {Qt::EditRole});
	emit imageActionGSig(pk, ImageAction::ATTRIB_CHANGED);
}

void ImageQueryModel::reload()
{
	beginResetModel();
	sortVec.clear();
	dataMap.clear();
	querySkip=0;
	//orderFieldStr=QStringLiteral("img1.pk");
	//orderDirStr=QStringLiteral("asc");
	QSqlQuery squery(sizeQuery, db);
	if(squery.next())
		querySize=squery.value(0).toInt();
	else
		querySize=0;
	endResetModel();
}

bool ImageQueryModel::checkSize()
{
	if(mode!=Mode::SEARCH)
		return false; // only relevant for searches
	int newQSize=0;
	QSqlQuery squery(sizeQuery, db);
	if(squery.next())
		newQSize=squery.value(0).toInt();
	else
	{
		qCritical()<<"ImageQueryModel::checkSize error"<<squery.lastError().driverText()<<squery.lastError().databaseText();
		return false; // you should worry but we can't help
	}
	if(newQSize==querySize)
		return false; // nothing to worry about
	// rebuild sortvec
	reload();
	return true;
}

void ImageQueryModel::setQuery(int sourceId)
{
	qCDebug(imagequerymodel)<<"setQuery"<<sourceId;
	beginResetModel();
	mode=Mode::SOURCE;
	lastSourceId=sourceId;
	//                               0        1         2                 3          4               5               6            7               8              9            10                    11
	queryFields=QStringLiteral("img1.pk, img1.val, img1.description, img1.size, img1.date_file, img1.date_exif, img1.camera, img1.longitude, img1.latitude, img1.height, img1.camera_settings, img1.orientation");
	query=QStringLiteral("select %1 from images img1 where img1.source=%2 order by %3 %4 limit %5 offset %6").arg(queryFields).arg(sourceId);
	sizeQuery=QStringLiteral("select count(1) from images where source=%2").arg(sourceId);
	QSqlQuery squery(sizeQuery, db);
	sortVec.clear();
	dataMap.clear();
	querySkip=0;
	if(squery.next())
		querySize=squery.value(0).toInt();
	else
		querySize=0;
	endResetModel();
	qCDebug(imagequerymodel)<<"setQuery done"<<sourceId;
}

void ImageQueryModel::setQuery(SqlObject *sql)
{
	int jctDummy=0, joinCardTagMax=0;
	QString clause=sql->toSqlString(jctDummy, joinCardTagMax);
	qCDebug(imagequerymodel)<<"ImageQueryModel::setQuery sql"<<clause;
	beginResetModel();
	mode=Mode::SEARCH;
	//                                   0        1         2                 3          4               5               6            7               8              9            10                    11               12
	queryFields=QStringLiteral("distinct img1.pk, img1.val, img1.description, img1.size, img1.date_file, img1.date_exif, img1.camera, img1.longitude, img1.latitude, img1.height, img1.camera_settings, img1.orientation, src.val");
	query=QStringLiteral("select %1 from images img1 inner join sources src on img1.source=src.pk ").arg(queryFields);
	sizeQuery=QStringLiteral("select count(1) from images img1 ");
	for(int i=1; i<=joinCardTagMax; ++i)
	{
		QString jc=QStringLiteral("inner join taggings tg%1 on img1.pk=tg%1.image ").arg(i);
		query+=jc;
		sizeQuery+=jc;
	}
	query+=QStringLiteral("where %2 order by %3 %4 limit %5 offset %6").arg(clause);
	sizeQuery+=QStringLiteral("where %1").arg(clause);
	QSqlQuery squery(sizeQuery, db);
	sortVec.clear();
	dataMap.clear();
	querySkip=0;
	if(squery.next())
		querySize=squery.value(0).toInt();
	else
		querySize=0;
	endResetModel();
	qCDebug(imagequerymodel)<<"setQuery data"<<query;
	qCDebug(imagequerymodel)<<"setQuery size"<<sizeQuery;
	qCDebug(imagequerymodel)<<"setQuery size is"<<querySize;
}

int ImageQueryModel::row2ImagePk(int row) const
{
	//qCDebug(imagequerymodel)<<"row2ImagePk start"<<row<<querySkip<<sortVec.size();
	if(row<querySkip)
	{ // backward cache
		const_cast<ImageQueryModel*>(this)->cacheDataDown(row);
	}
	else if(row>=querySkip+(int)sortVec.size())
	{ // forward cache
		const_cast<ImageQueryModel*>(this)->cacheDataUp(row);
	}
	row-=querySkip;
	if(row<0 || row>=(int)sortVec.size())
	{
		qCritical()<<"ImageQueryModel::row2ImagePk invalid sort vector"<<row<<querySkip<<sortVec.size();
		return -1;
	}
	return sortVec.at(row);
}

int ImageQueryModel::imagePk2Row(int imagePk) const
{
	for(int i=0; i<(int)sortVec.size(); ++i)
	{
		if(sortVec.at(i)==imagePk)
			return querySkip+i;
	}
	qWarning()<<"reverse lookup imagePk not cached!"<<imagePk;
	return -1;
}

bool ImageQueryModel::isDataIndex(const QModelIndex &idx) const
{
	return idx.isValid() && idx.column()<columnCount() && idx.row()<querySize;
}

void ImageQueryModel::cacheDataUp(int upToRow)
{
	int fetchCount=upToRow-(querySkip+sortVec.size())+1;
	if(fetchCount<=0)
	{
		qWarning()<<"ImageQueryModel::cacheDataUp no count"<<fetchCount;
		return;
	}
	if(fetchCount<cacheChunkSize)
		fetchCount=cacheChunkSize;
	// select ... from images where source=... order by %3 %4 limit %5 offset %6
	qCDebug(imagequerymodel)<<"caching img up"<<querySkip<<sortVec.size()<<fetchCount;
	QSqlQuery q(query.arg(orderFieldStr, orderDirStr).arg(fetchCount).arg(querySkip+sortVec.size()), db);
	while(q.next())
	{
		int pk=q.value(0).toInt();
		ImgData imgData;
		imgData.filename=q.value(1).toString();
		imgData.description=q.value(2).toString();
		imgData.size=q.value(3).toString();
		imgData.timestamp=q.value(5).isNull()?q.value(4).toString():q.value(5).toString();
		imgData.camera=q.value(6).toString();
		GpsCoord gps(q.value(7), q.value(8), q.value(9));
		imgData.GPS=gps.toString();
		imgData.settings=q.value(10).toString();
		imgData.orientation=Image::orientationStr((Image::ExifOrientation)(q.value(11).toInt()));
		if(mode==Mode::SEARCH)
			imgData.source=q.value(12).toString();
		sortVec.push_back(pk);
		dataMap.insert(DataMap::value_type(pk, imgData));
	}
	qCDebug(imagequerymodel)<<"caching img up qu compl"<<sortVec.size();
	if((int)sortVec.size()>cacheMaxSize)
	{
		int rmCnt=cacheMaxSize/2;
		for(int i=0; i<rmCnt; ++i)
		{
			dataMap.erase(sortVec.at(i));
		}
		sortVec.erase(sortVec.begin(), sortVec.begin()+rmCnt);
		querySkip+=rmCnt;
		qCDebug(imagequerymodel)<<"cache reduction up"<<rmCnt<<querySkip;
	}
}

void ImageQueryModel::cacheDataDown(int downToRow)
{
	if(downToRow>=querySkip)
	{
		qWarning()<<"ImageQueryModel::cacheDataDown no count";
		return;
	}
	int fetchCount=querySkip-downToRow;
	if(fetchCount<cacheChunkSize)
		fetchCount=cacheChunkSize;
	if(fetchCount>querySkip)
		fetchCount=querySkip;
	// select ... from images where source=... order by %3 %4 limit %5 offset %6
	qCDebug(imagequerymodel)<<"caching img down"<<querySkip<<sortVec.size()<<fetchCount;
	SortVec oldSv=sortVec;
	sortVec.clear();
	QSqlQuery q(query.arg(orderFieldStr, orderDirStr).arg(fetchCount).arg(querySkip-fetchCount), db);
	while(q.next())
	{
		int pk=q.value(0).toInt();
		ImgData imgData;
		imgData.filename=q.value(1).toString();
		imgData.description=q.value(2).toString();
		imgData.size=q.value(3).toString();
		imgData.timestamp=q.value(5).isNull()?q.value(4).toString():q.value(5).toString();
		imgData.camera=q.value(6).toString();
		GpsCoord gps(q.value(7), q.value(8), q.value(9));
		imgData.GPS=gps.toString();
		imgData.settings=q.value(10).toString();
		imgData.orientation=Image::orientationStr((Image::ExifOrientation)(q.value(11).toInt()));
		if(mode==Mode::SEARCH)
			imgData.source=q.value(12).toString();
		sortVec.push_back(pk);
		dataMap.insert(DataMap::value_type(pk, imgData));
	}
	int i=0;
	for(; i<(int)oldSv.size() && i+fetchCount<=cacheMaxSize; ++i)
	{
		sortVec.push_back(oldSv.at(i));
	}
	querySkip-=fetchCount;
	if(i<(int)oldSv.size())
	{
		for(; i<(int)oldSv.size(); ++i)
		{
			dataMap.erase(oldSv.at(i));
		}
		qCDebug(imagequerymodel)<<"cache reduction down"<<fetchCount<<querySkip;
	}
}
