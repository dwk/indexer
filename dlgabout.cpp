#include "dlgabout.h"
#include "dockwidfactory.h"
#include "idxi.h"
#include "ui_dlgabout.h"

Q_LOGGING_CATEGORY(dlgabout, "dlgabout")

DlgAbout::DlgAbout(QWidget *parent) : QDialog(parent), ui(new Ui::DlgAbout)
{
	ui->setupUi(this);
	ui->label_ProgVersion->setText(VERSION_STR);
	//ui->label_ProgBuild->setText(QString("Build timestamp: ")+buildTimestamp());
	ui->label_QtVersion->setText(QString("Qt Version: ")+QT_VERSION_STR+" (open source)");
	ui->label_SQLiteVersion->setText(DCON.db()->getVersion());
	/*ui->comboCentral->addItem(tr("none"), QVariant());
	QStringList dws=DockWidFactory::get().allDockWids();
	foreach(QString dwname, dws)
	{
		DockWidFactory::DWInfo *dwi=DockWidFactory::get().produce(dwname);
		if(!dwi)
			continue;
		ui->comboCentral->addItem(dwi->guiName, QVariant(dwi->techName));
	}*/
	QSettings settings;
	/*QString cent=settings.value("layout/central").toString();
	int ind=ui->comboCentral->findData(QVariant(cent));
	ui->comboCentral->setCurrentIndex(ind<0?0:ind);*/
	ui->checkDoubleInsertIsDelete->setChecked(settings.value("pref/tagDoubleInsertIsDelete", true).toBool());
	ui->checkVWheelInverted->setChecked(settings.value("pref/vertWheelInverted", true).toBool());
	ui->checkHWheelInverted->setChecked(settings.value("pref/horizWheelInverted", true).toBool());
	ui->spinMinTreeTagWidth->setValue(settings.value("pref/minTreeTagWidth", 80).toInt());
	ui->spinThumbSize->setValue(settings.value("pref/thumbSize", 200).toInt());
	ui->spinTinySize->setValue(settings.value("pref/tinySize", 320).toInt());
	ui->spinMagMag->setValue(settings.value("pref/magMag", 5).toInt());
	ui->spinMagSize->setValue(settings.value("pref/magWidth", 60).toInt());
	ui->spinCacheSourceSize->setValue(settings.value("perf/cacheSizeSource", 32).toInt());
	ui->spinCacheImageSize->setValue(settings.value("perf/cacheSizeImage", 64).toInt());
	ui->spinCachePixmapSize->setValue(settings.value("perf/cacheSizePixmap", 3).toInt());
	ui->lineCmdGimp->setText(settings.value("cmd/cmdGimp", QStringLiteral("gimp")).toString());
	ui->lineCmdFiles->setText(settings.value("cmd/cmdFiles", QStringLiteral("nautilus")).toString());
	t=new QThread(this);
}

void DlgAbout::accept()
{
	t->quit();
	t->wait(1000);
	QSettings settings;
	//settings.setValue("layout/central", ui->comboCentral->currentData().toString());
	settings.setValue("pref/tagDoubleInsertIsDelete", ui->checkDoubleInsertIsDelete->isChecked());
	settings.setValue("pref/vertWheelInverted", ui->checkVWheelInverted->isChecked());
	settings.setValue("pref/horizWheelInverted", ui->checkHWheelInverted->isChecked());
	settings.setValue("pref/minTreeTagWidth", ui->spinMinTreeTagWidth->value());
	settings.setValue("pref/thumbSize", ui->spinThumbSize->value());
	settings.setValue("pref/tinySize", ui->spinTinySize->value());
	settings.setValue("pref/magMag", ui->spinMagMag->value());
	settings.setValue("pref/magWidth", ui->spinMagSize->value());
	settings.setValue("perf/cacheSizeSource", ui->spinCacheSourceSize->value());
	settings.setValue("perf/cacheSizeImage", ui->spinCacheImageSize->value());
	settings.setValue("perf/cacheSizePixmap", ui->spinCachePixmapSize->value());
	settings.setValue("cmd/cmdGimp", ui->lineCmdGimp->text());
	settings.setValue("cmd/cmdFiles", ui->lineCmdFiles->text());
	DCON.readPrefsSl();
	QDialog::accept();
}

void DlgAbout::reject()
{
	t->quit();
	t->wait(1000);
	QDialog::reject();
}

#include <QThread>

void DlgAbout::on_pushTEST_clicked()
{
	qDebug()<<"DlgAbout::on_pushTEST_clicked";
	ThreadWorker *worker = new ThreadWorker(this);
	worker->moveToThread(t);
	connect(t, &QThread::finished, worker, &QObject::deleteLater);
	connect(this, &DlgAbout::operateSig, worker, &ThreadWorker::doWork);
	connect(worker, &ThreadWorker::resultReady, this, &DlgAbout::resultReadySl);
	t->start();
	emit operateSig(":/ic/res/BrokenImage.JPG");
}

void DlgAbout::resultReadySl(QPixmap *result)
{
	//result->moveToThread(this->thread());
	ui->label->setPixmap(*result);
	QLabel *res2=new QLabel(this);
	res2->setText("bla");
	ui->verticalLayout_2->addWidget(res2);
}

void ThreadWorker::doWork(const QString &parameter)
{
	QPixmap *result=new QPixmap(parameter);
	//result->setText(parameter);
	emit resultReady(result);
}
