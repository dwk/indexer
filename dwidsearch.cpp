#include "dwidsearch.h"
#include "dwidsearch_fac.h"
#include "ui_dwidsearch.h"
#include <QSqlRecord>
#include <QSqlQuery>
#include <QSqlError>
#include <QTreeWidgetItem>
#include <QComboBox>
#include <QLineEdit>
#include <QDateTimeEdit>
#include "widlabelsmenu.h"
#include "idxi.h"

Q_LOGGING_CATEGORY(dwidsearch, "dwidsearch")

QWidget *SearchDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &index) const
{
	const QAbstractItemModel *mod=index.model();
	SqlObject * so=(SqlObject *)mod->data(mod->index(index.row(),0,index.parent()), Qt::UserRole).value<void *>();
	//qDebug()<<"createEditor"<<index.row()<<index.column()<<so<<(so?so->toDisplayStrings():QStringList());
	return so?so->createEditor(parent, index.column()):nullptr;
}

void SearchDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	const QAbstractItemModel *mod=index.model();
	SqlObject * so=(SqlObject *)mod->data(mod->index(index.row(),0,index.parent()), Qt::UserRole).value<void *>();
	//qDebug()<<"setEditorData"<<index.row()<<index.column()<<so<<(so?so->toDisplayStrings():QStringList());
	if(!so)
		return;
	so->setEditorData(editor, index.column());
}

void SearchDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
	SqlObject * so=(SqlObject *)model->data(model->index(index.row(),0,index.parent()), Qt::UserRole).value<void *>();
	//qDebug()<<"setModelData"<<index.row()<<index.column()<<so<<(so?so->toDisplayStrings():QStringList());
	if(!so)
		return;
	model->setData(index, so->setModelData(editor, index.column()), Qt::EditRole);
}


DWidSearch::DWidSearch(QWidget *parent) :
	QWidget(parent),
	ui(new ::Ui::DWidSearch), topsql(new SqlRelation(SqlRelation::Relation::LOGIC_AND))
{
	setObjectName(techname);
	ui->setupUi(this);
	QTreeWidgetItem *twi=new QTreeWidgetItem(QStringList()<<topsql->toDisplayStrings());
	twi->setData(0, Qt::UserRole, QVariant::fromValue((void*)topsql));
	ui->treeQuery->addTopLevelItem(twi);
	ui->treeQuery->setItemDelegate(new SearchDelegate(this));
	ui->treeQuery->resizeColumnToContents(0);
	ui->treeQuery->resizeColumnToContents(1);
	ui->treeQuery->resizeColumnToContents(2);
	connect(ui->pushFilterTag, SIGNAL(clicked()), this, SLOT(pushFilterTagClicked())); // because of default parameter
}

DWidSearch::~DWidSearch()
{
	delete ui;
	delete topsql;
}

void DWidSearch::connectAll(ObjectMSP &otherActors)
{
	qDebug()<<"connectAll"<<objectName();
	for(auto it=otherActors.begin(); it!=otherActors.end(); ++it)
	{
		QObject *obj=it->second;
		if(obj->objectName()==objectName())
			continue;
		const QMetaObject* mo = obj->metaObject();
		if(mo->indexOfSignal("labelActionGSig(int,LabelAction)")>=0)
		{
			qDebug()<<"    "<<obj->objectName()<<"labelActionGSig(int,LabelAction)";
			connect(obj, SIGNAL(labelActionGSig(int,LabelAction)), this, SLOT(labelActionGSl(int,LabelAction)));
		}
	}
}

void DWidSearch::setupSl()
{
	ui->treeQuery->topLevelItem(0)->setSelected(true);
}

void DWidSearch::labelActionGSl(int tagPk, LabelAction la)
{
	if(la==LabelAction::SEARCH)
		pushFilterTagClicked(tagPk);
}

SqlRelation *DWidSearch::getAnchorRelation(QTreeWidgetItem **parent)
{
	QList<QTreeWidgetItem *> sels=ui->treeQuery->selectedItems();
	if(parent)
		*parent=nullptr;
	if(!sels.size() || sels.size()>1)
	{
		return nullptr;
	}
	SqlObject *so=(SqlObject *)(sels.at(0)->data(0, Qt::UserRole).value<void*>());
	SqlRelation *sr=dynamic_cast< SqlRelation * >(so);
	//qDebug()<<"casting"<<(void*)so<<(void*)sr;
	if(sr)
	{
		if(parent)
			*parent=sels.at(0);
	}
	else
	{
		QTreeWidgetItem *twi=sels.at(0)->parent();
		if(twi)
			sr=dynamic_cast< SqlRelation * >((SqlObject *)(twi->data(0, Qt::UserRole).value<void*>()));
		if(sr && parent)
			*parent=twi;
	}
	return sr;
}

SqlObject *DWidSearch::getCurrentObject(QTreeWidgetItem **item)
{
	QList<QTreeWidgetItem *> sels=ui->treeQuery->selectedItems();
	if(item)
		*item=nullptr;
	if(!sels.size() || sels.size()>1)
	{
		return nullptr;
	}
	if(item)
		*item=sels.at(0);
	return (SqlObject *)(sels.at(0)->data(0, Qt::UserRole).value<void*>());
}

void DWidSearch::insertNewObject(QTreeWidgetItem *parent, SqlRelation *sqlParent, SqlObject *newObject)
{
	if(sqlParent && newObject)
	{
		//qDebug()<<"inserting"<<(void*)sqlParent<<(void*)newObject;
		sqlParent->addObject(newObject);
		QTreeWidgetItem *twi=new QTreeWidgetItem(parent, newObject->toDisplayStrings());
		twi->setData(0, Qt::UserRole, QVariant::fromValue((void *)newObject));
		if(!newObject->useDialogEditor())
			twi->setFlags(twi->flags() | Qt::ItemIsEditable);
		//ui->treeQuery->setItemDelegateForRow()
		parent->setExpanded(true);
		ui->treeQuery->resizeColumnToContents(2);
	}
	else
	{
		QApplication::beep();
		qDebug()<<"neither sel nor parent is a relation";
	}
}

void DWidSearch::on_toolGo_clicked()
{
	ui->treeQuery->setCurrentIndex(ui->treeQuery->model()->index(0,0)); // hack for closing pending editors
	emit querySelectedSig((void*)topsql);
}

void DWidSearch::on_treeQuery_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
	if(column>0)
	{
		ui->treeQuery->editItem(item, column);
	}
}

void DWidSearch::on_treeQuery_itemSelectionChanged()
{
	SqlRelation *sr=getAnchorRelation(nullptr);
	if(!sr)
	{
		ui->pushRelAnd->setEnabled(false);
		ui->pushRelOr->setEnabled(false);
	}
	else
	{
		ui->pushRelAnd->setEnabled(sr->relation==SqlObject::Relation::LOGIC_OR);
		ui->pushRelOr->setEnabled(sr->relation==SqlObject::Relation::LOGIC_AND);
	}
}

void DWidSearch::on_treeQuery_activated(const QModelIndex &index)
{
	const QAbstractItemModel *mod=index.model();
	SqlObject * so=(SqlObject *)mod->data(mod->index(index.row(),0,index.parent()), Qt::UserRole).value<void *>();
	if(so->modalDialogUpdate())
	{
		QStringList sl=so->toDisplayStrings();
		//qDebug()<<"updating clause display"<<sl.join('|');
		QTreeWidgetItem *twi=ui->treeQuery->currentItem();
		twi->setText(0, sl.at(0));
		twi->setText(1, sl.at(1));
		twi->setText(2, sl.at(2));
	}
}

void DWidSearch::pushFilterTagClicked(int tagPk)
{
	QTreeWidgetItem *parent;
	SqlRelation *sr=getAnchorRelation(&parent);
	SqlObject * newObj=nullptr;
	if(sr)
		newObj=new SqlTagClause(tagPk);
	insertNewObject(parent, sr, newObj);
}

void DWidSearch::on_pushFilterDateFile_clicked()
{
	QTreeWidgetItem *parent;
	SqlRelation *sr=getAnchorRelation(&parent);
	SqlObject * newObj=nullptr;
	if(sr)
		newObj=new SqlDateTimeClause(true);
	insertNewObject(parent, sr, newObj);
}

void DWidSearch::on_pushFilterDateExif_clicked()
{
	QTreeWidgetItem *parent;
	SqlRelation *sr=getAnchorRelation(&parent);
	SqlObject * newObj=nullptr;
	if(sr)
		newObj=new SqlDateTimeClause(false);
	insertNewObject(parent, sr, newObj);
}

void DWidSearch::on_pushFilterDesc_clicked()
{
	QTreeWidgetItem *parent;
	SqlRelation *sr=getAnchorRelation(&parent);
	SqlObject * newObj=nullptr;
	if(sr)
		newObj=new SqlDescClause();
	insertNewObject(parent, sr, newObj);
}

void DWidSearch::on_pushFilterSource_clicked()
{
	QTreeWidgetItem *parent;
	SqlRelation *sr=getAnchorRelation(&parent);
	SqlObject * newObj=nullptr;
	if(sr)
		newObj=new SqlSourceClause();
	insertNewObject(parent, sr, newObj);
}

void DWidSearch::on_pushFilterGps_clicked()
{
	QTreeWidgetItem *parent;
	SqlRelation *sr=getAnchorRelation(&parent);
	SqlObject * newObj=nullptr;
	if(sr)
		newObj=new SqlGpsClause();
	insertNewObject(parent, sr, newObj);
}

void DWidSearch::on_pushRelAnd_clicked()
{
	QTreeWidgetItem *parent;
	SqlRelation *sr=getAnchorRelation(&parent);
	SqlObject * newObj=nullptr;
	if(sr)
		newObj=new SqlRelation(SqlRelation::Relation::LOGIC_AND);
	insertNewObject(parent, sr, newObj);
}

void DWidSearch::on_pushRelOr_clicked()
{
	QTreeWidgetItem *parent;
	SqlRelation *sr=getAnchorRelation(&parent);
	SqlObject * newObj=nullptr;
	if(sr)
		newObj=new SqlRelation(SqlRelation::Relation::LOGIC_OR);
	insertNewObject(parent, sr, newObj);
}

void DWidSearch::on_pushEditDelete_clicked()
{
	QList<QTreeWidgetItem *> sels=ui->treeQuery->selectedItems();
	if(!sels.size() || sels.size()>1)
		return;
	QTreeWidgetItem *twit=sels.at(0);
	QTreeWidgetItem *pi=twit->parent();
	if(!pi)
		return; // never delete top level
	ui->treeQuery->setCurrentIndex(ui->treeQuery->model()->index(0,0)); // hack for closing pending editors
	SqlObject *so=(SqlObject *)(twit->data(0, Qt::UserRole).value<void*>());
	SqlRelation *sr=dynamic_cast< SqlRelation * >((SqlObject *)(pi->data(0, Qt::UserRole).value<void*>()));
	//qDebug()<<"casting"<<(void*)so<<(void*)sr;
	if(so && sr)
	{
		sr->removeObject(so);
		delete pi->takeChild(pi->indexOfChild(twit));
	}
}

void DWidSearch::on_pushEditDeleteAll_clicked()
{
	SqlRelation *tr=dynamic_cast<SqlRelation *>(topsql);
	if(!tr)
		return;
	ui->treeQuery->setCurrentIndex(ui->treeQuery->model()->index(0,0)); // hack for closing pending editors
	tr->removeAllObjects();
	QTreeWidgetItem *tli=ui->treeQuery->topLevelItem(0);
	while(tli->childCount())
		delete tli->takeChild(0);
}
