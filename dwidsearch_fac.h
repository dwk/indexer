#ifndef DWIDSEARCHFAC_H
#define DWIDSEARCHFAC_H

#include "dockwidfactory.h"

namespace
{
QWidget * DockCreator(QWidget * parent)
{
	return new DWidSearch(parent);
}
const char *techname="dwid_search";
bool dockWidRegistered=DockWidFactory::get().registerDockWid("Search", techname, Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea, Qt::BottomDockWidgetArea, DockCreator);
}
#endif // DWIDSEARCHFAC_H
