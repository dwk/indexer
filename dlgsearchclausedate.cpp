#include "dlgsearchclausedate.h"
#include "sqlobject.h"
#include "idxi.h"
#include "ui_dlgsearchclausedate.h"

Q_LOGGING_CATEGORY(dlgsearchclausedate, "dlgsearchclausedate")

DlgSearchclauseDate::DlgSearchclauseDate(SqlDateTimeClause *dtc) :
	QDialog(nullptr), ui(new Ui::DlgSearchclauseDate), dtc(dtc)
{
	ui->setupUi(this);
	QDateTime dt=dtc->val.toDateTime();
	ui->dateEdit->setDateTime(dt);
	ui->timeEdit->setDateTime(dt);
	if(dtc->field.contains(QStringLiteral("exif")))
		ui->radioUseExif->setChecked(true);
	else
		ui->radioUseFile->setChecked(true);
	if(dt.time()==QTime(0, 0))
		ui->radioTimeStart->setChecked(true);
	else if(dt.time()==QTime(23, 59))
		ui->radioTimeEnd->setChecked(true);
	else
	{
		ui->radioTime->setChecked(true);
		ui->timeEdit->setEnabled(true);
	}
	int idx=(int)SqlObject::Relation::GREATER;
	ui->comboRelation->addItem(SqlObject::templRelationText[idx], idx);
	idx=(int)SqlObject::Relation::GREATEREQUAL;
	ui->comboRelation->addItem(SqlObject::templRelationText[idx], idx);
	idx=(int)SqlObject::Relation::EQUAL;
	ui->comboRelation->addItem(SqlObject::templRelationText[idx], idx);
	idx=(int)SqlObject::Relation::LESSEQUAL;
	ui->comboRelation->addItem(SqlObject::templRelationText[idx], idx);
	idx=(int)SqlObject::Relation::LESS;
	ui->comboRelation->addItem(SqlObject::templRelationText[idx], idx);
	ui->comboRelation->setCurrentText(dtc->templRelationText[(int)(dtc->relation)]);
}

void DlgSearchclauseDate::accept()
{
	dtc->field=ui->radioUseFile->isChecked()?QStringLiteral(u"img1.date_file"):QStringLiteral(u"img1.date_exif");
	dtc->relation=(SqlObject::Relation)ui->comboRelation->currentData().toInt();
	QDateTime tst=ui->dateEdit->dateTime();
	tst.setTime(ui->timeEdit->time());
	dtc->val=tst;
	dtc->updateDisp();
	QDialog::accept();
}

void DlgSearchclauseDate::on_radioTimeStart_toggled(bool checked)
{
	if(!checked)
		return;
	ui->timeEdit->setTime(QTime(0,0));
	ui->timeEdit->setEnabled(false);
}

void DlgSearchclauseDate::on_radioTimeEnd_toggled(bool checked)
{
	if(!checked)
		return;
	ui->timeEdit->setTime(QTime(23,59));
	ui->timeEdit->setEnabled(false);
}

void DlgSearchclauseDate::on_radioTime_toggled(bool checked)
{
	if(!checked)
		return;
	ui->timeEdit->setEnabled(true);
}


