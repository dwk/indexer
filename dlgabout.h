#ifndef DLGABOUT_H
#define DLGABOUT_H

#include "idxh.h"
#include <QDialog>

Q_DECLARE_LOGGING_CATEGORY(dlgabout)

class QThread;
class QLabel;

namespace Ui
{
	class DlgAbout;
}


class ThreadWorker : public QObject
{
	Q_OBJECT
public:
	ThreadWorker(QWidget * p) : guiParent(p) {}
public slots:
	void doWork(const QString &parameter);
signals:
	void resultReady(QPixmap * result);
private:
	QWidget * guiParent;
};


class DlgAbout : public QDialog
{
	Q_OBJECT
public:
	explicit DlgAbout(QWidget *parent);
public slots:
	virtual void accept();
	virtual void reject();
private slots:
	void on_pushTEST_clicked();
	void resultReadySl(QPixmap * result);
private:
	Ui::DlgAbout *ui;
	QThread *t=nullptr;
signals:
	void operateSig(const QString &parameter);
};
#endif // DLGABOUT_H
