#ifndef DLGFROMCAM_H
#define DLGFROMCAM_H

#include "idxh.h"
#include <QDialog>
#include <QDir>

Q_DECLARE_LOGGING_CATEGORY(dlgfromcam)

class QLineEdit;
namespace Ui
{
	class DlgFromCam;
}

class DlgFromCam : public QDialog
{
	Q_OBJECT
public:
	explicit DlgFromCam(QWidget *parent);
	~DlgFromCam();
public slots:
	//virtual void accept();
private slots:
	void on_lineBase_textChanged(const QString &);
	void on_toolBase_clicked();
	void on_lineCam_textChanged(const QString &);
	void on_toolCam_clicked();
	void on_pushCheck_clicked();
	void on_pushCopy_clicked();
	void on_pushAdd2Old_clicked();
private:
	bool doCopy(const QString &targetDirPath); // ok=true
	Ui::DlgFromCam *ui;
	QDir *dbase=nullptr, *dcam=nullptr;
	QString refdir;
	QStringList filesToCopy;
};
#endif // DLGFROMCAM_H
