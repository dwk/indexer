#ifndef SQLOBJECT_H
#define SQLOBJECT_H

#include "idxh.h"
#include <list>
#include <QWidget>
#include <QVariant>
#include <QDateTime>
#include "gpscoord.h"

Q_DECLARE_LOGGING_CATEGORY(sqlobject)

class QTreeWidgetItem;
class DlgSearchclauseDate;

class SqlObject
{
public:
	//                   0        1        2     3          4        5             6     7          8         9
	enum class Relation {EQUAL=0, UNEQUAL, LESS, LESSEQUAL, GREATER, GREATEREQUAL, LIKE, LOGIC_AND, LOGIC_OR, SUBSETOF};
	enum class TableRef {IMAGES=0, TAGGINGS};
	virtual ~SqlObject() {}
	virtual QString toSqlString(int & joinCardTag, int & joinCardTagMax)=0;
	virtual QStringList toDisplayStrings()=0;
	virtual bool useDialogEditor() {return false;} // activates delegate base inline editing by default, return true for modalDialogUpdate being called
	virtual QWidget * createEditor(QWidget *parent, int col)=0;
	virtual bool modalDialogUpdate() {return false;} // if true, tree view shall update display
	virtual void setEditorData(QWidget *editor, int col)=0;
	virtual QVariant setModelData(QWidget *editor, int col)=0; // return should be displayable Text
protected:
	static QString templRelation[];
	static QString templRelationText[];
};
typedef std::vector< SqlObject * > SqlObjectVP;
typedef std::set< SqlObject * > SqlObjectSP;


class SqlClause : public SqlObject
{
public:
	SqlClause(SqlObject::TableRef tableRef, const QString & field, const QString & value, SqlObject::Relation relation=SqlObject::Relation::EQUAL);
	SqlClause(SqlObject::TableRef tableRef, const QString & field, const QVariant & value, SqlObject::Relation relation=SqlObject::Relation::EQUAL);
	virtual QString toSqlString(int & joinCardTag, int &joinCardTagMax);
	virtual QStringList toDisplayStrings()
		{return QStringList()<<(dispField.isEmpty()?field:dispField)<<templRelationText[(int)relation]<<(dispVal.isEmpty()?val.toString():dispVal);}
	virtual QWidget * createEditor(QWidget *parent, int col);
	virtual void setEditorData(QWidget *editor, int col);
	virtual QVariant setModelData(QWidget *editor, int col);
	QString field, dispField, dispVal;
	QVariant val;
	SqlObject::Relation relation;
	bool tagClause=false, imageClause=false;
	bool quoteIt=false;
private:
	void commonCtor(SqlObject::TableRef tableRef);
};

class SqlTagClause : public SqlClause
{
public:
	SqlTagClause(int tagPk);
	virtual QString toSqlString(int & joinCardTag, int &joinCardTagMax);
	virtual QWidget * createEditor(QWidget *parent, int col);
	virtual void setEditorData(QWidget *editor, int col);
	virtual QVariant setModelData(QWidget *editor, int col);
protected:
	// val is tagPk
};

class SqlDateTimeClause : public SqlClause
{
	friend class DlgSearchclauseDate;
public:
	SqlDateTimeClause(bool useFileDate);
	virtual QString toSqlString(int &, int &);
	//virtual QStringList toDisplayStrings() {return QStringList()<<field<<templRelationText[(int)relation]<<dispVal;}
	virtual bool useDialogEditor() {return true;} // use dialog
	virtual QWidget * createEditor(QWidget *, int ) {return nullptr;}
	virtual bool modalDialogUpdate(); // if true, tree view shall update display
	virtual void setEditorData(QWidget *, int ) {}
	virtual QVariant setModelData(QWidget *, int ) {return QVariant();}
protected:
	static QString dtformat;
	void updateDisp();
};

class SqlDescClause : public SqlClause
{
public:
	SqlDescClause(); // todo feature: search for NULL values
	virtual QWidget * createEditor(QWidget *parent, int col);
	//virtual void setEditorData(QWidget *editor, int col);
	//virtual QVariant setModelData(QWidget *editor, int col);
protected:
	// val is tagPk
};

class SqlSourceClause : public SqlClause
{
public:
	SqlSourceClause();
	virtual QWidget * createEditor(QWidget *parent, int col);
	virtual void setEditorData(QWidget *editor, int col);
	virtual QVariant setModelData(QWidget *editor, int col);
protected:
	// val is sourcePk
};

class SqlGpsClause : public SqlClause
{
	friend class DlgSearchclauseGps;
public:
	SqlGpsClause();
	virtual QString toSqlString(int &, int &);
	//virtual QStringList toDisplayStrings() {return QStringList()<<field<<templRelationText[(int)relation]<<dispVal;}
	virtual bool useDialogEditor() {return true;} // use dialog
	virtual QWidget * createEditor(QWidget *, int ) {return nullptr;}
	virtual bool modalDialogUpdate(); // if true, tree view shall update display
	virtual void setEditorData(QWidget *, int ) {}
	virtual QVariant setModelData(QWidget *, int ) {return QVariant();}
protected:
	double lonTol=-1, latTol=-1.; // unit km; 1 deg lat=111km; 1 deg lon~111km*cos(lat)
	int heiTol=-1; // unit m
};

class SqlRelation : public SqlObject
{
public:
	SqlRelation(SqlObject::Relation relation=SqlObject::Relation::LOGIC_AND) : relation(relation) {}
	~SqlRelation();
	virtual QString toSqlString(int &, int & joinCardTagMax);
	virtual QStringList toDisplayStrings();
	virtual QWidget * createEditor(QWidget *parent, int col);
	virtual void setEditorData(QWidget *editor, int col);
	virtual QVariant setModelData(QWidget *editor, int col);
	void addObject(SqlObject * obj); // takes ownership
	void removeObject(SqlObject * obj);
	void removeAllObjects();
	SqlObject::Relation relation;
private:
	SqlObjectSP objs;
	static QString relStrs[];
};


#endif // SQLOBJECT_H
