#include "dwidsources.h"
#include "dwidsources_fac.h"
#include "ui_dwidsources.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QProcess>
#include <QMenu>
#include "dlgeditsource.h"
#include "idxi.h"

Q_LOGGING_CATEGORY(dwidsources, "dwidsources")

DWidSources::DWidSources(QWidget *parent) :
	QWidget(parent),
	ui(new ::Ui::DWidSources)
{
	setObjectName(techname);
	ui->setupUi(this);
	ui->pushNew->setDisabled(true);
	ui->pushDelete->setDisabled(true);
	ui->pushReload->setDisabled(true);

	model=new SourceQueryModel(this, DCON.db()->db());
	connect(model, &QAbstractItemModel::dataChanged, this, &DWidSources::editFinishedSl);
	ui->treeView->setModel(model);
	applyTableGeom();
	connect(ui->treeView->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)),
		this, SLOT(tableSelChangedSl(const QItemSelection &, const QItemSelection &)));
	ui->treeView->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(ui->treeView, SIGNAL(customContextMenuRequested(QPoint)), SLOT(customContextMenuRequestedSl(QPoint)));
}

DWidSources::~DWidSources()
{
	QSettings settings;
	settings.setValue("pref/sourcetree/colwidth0", ui->treeView->columnWidth(0));
	settings.setValue("pref/sourcetree/colwidth1", ui->treeView->columnWidth(1));
	delete ui;
}

void DWidSources::connectAll(ObjectMSP &otherActors)
{
	qDebug()<<"connectAll"<<objectName();
	for(auto it=otherActors.begin(); it!=otherActors.end(); ++it)
	{
		QObject *obj=it->second;
		if(obj->objectName()==objectName())
			continue;
		const QMetaObject* mo = obj->metaObject();
		if(mo->indexOfSignal("sourceSelectedSig(int)")>=0)
		{
			qDebug()<<"    "<<obj->objectName()<<"sourceSelectedSig(int)";
			connect(obj, SIGNAL(sourceSelectedSig(int)), this, SLOT(sourceSelectedSl(int)));
		}
		if(mo->indexOfSignal("imageActionGSig(int,ImageAction)")>=0)
		{
			qDebug()<<"    "<<obj->objectName()<<"imageActionGSig(int,ImageAction)";
			connect(obj, SIGNAL(imageActionGSig(int,ImageAction)), this, SLOT(imageActionGSl(int,ImageAction)));
		}
	}
}

void DWidSources::setupSl()
{
	on_pushReload_clicked();
}

void DWidSources::openSourceSl()
{
	QModelIndex mi=ui->treeView->currentIndex();
	int spk=qobject_cast<SourceQueryModel*>(ui->treeView->model())->index2Pk(mi);
	if(spk<=0)
	{
		QApplication::beep();
		return;
	}
	Source & src=DCON.getSource(spk);
	QStringList params;
	params<<src.location();
	QProcess p;
	qInfo()<<"executing browser"<<params;
	p.startDetached(DCON.getCmdFiles(), params);
	p.waitForStarted();
}

void DWidSources::applyTableGeom()
{
	QSettings settings;
	ui->treeView->setColumnWidth(0, settings.value("pref/sourcetree/colwidth0", 100).toInt());
	ui->treeView->setColumnWidth(1, settings.value("pref/sourcetree/colwidth1", 200).toInt());
}

void DWidSources::sourceSelectedSl(int sourcePk)
{
	supressSelectionSignal=true;
	ui->treeView->selectionModel()->clear();
	if(sourcePk<0)
	{
		supressSelectionSignal=false;
		return;
	}
	QModelIndex mil=model->findSource(sourcePk);
	if(!mil.isValid())
	{
		qWarning()<<"DWidSources::sourceSelectedSl pk not available"<<sourcePk;
		supressSelectionSignal=false;
		return;
	}
	ui->treeView->setCurrentIndex(mil);
	// done by currrent ui->treeView->selectionModel()->select(mil, QItemSelectionModel::Select | QItemSelectionModel::Current | QItemSelectionModel::Rows);
	ui->treeView->scrollTo(mil);
	supressSelectionSignal=false;
}

void DWidSources::imageActionGSl(int imagePk, ImageAction action)
{
	switch(action)
	{
	case ImageAction::SELECT:
	case ImageAction::SELECT_MULTI:
	case ImageAction::MARK:
	case ImageAction::UNMARK:
	case ImageAction::MARK_MULTI:
		break;
	case ImageAction::DELETE:
	{
		Image & img=DCON.getImage(imagePk);
		Source & src=DCON.getSource(img.getSourcePk());
		if(!src.isValid())
			return;
		if(QMessageBox::question(this, tr("Permanent Delete"), tr("Delete the original image-file (%1) from the image source? This cannot be undone and the image will be lost.").arg(src.getFullUrl(img.getName())),
								 QMessageBox::Yes | QMessageBox::No, QMessageBox::No)!=QMessageBox::Yes)
			return;
		if(!src.deleteImagePermanent(img.getName()))
			QMessageBox::warning(this, tr("Image Action failed"), tr("Failed to remove image %1.").arg(src.getFullUrl(img.getName())));
		break;
	} // DELETE
	case ImageAction::SAVE_AS:
	case ImageAction::LABELS_CHANGED:
	case ImageAction::ATTRIB_CHANGED:
	case ImageAction::PIXEL_CHANGED:
		break;
	}
}

void DWidSources::editFinishedSl(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
	if(topLeft.row()!=bottomRight.row())
		qWarning()<<"DWidSources multi line edit not supported!"<<topLeft.row()<<bottomRight.row();
	int firstPk=model->index2Pk(topLeft);
	int lastPk=model->index2Pk(bottomRight);
	for(int i=firstPk; i<=lastPk; ++i)
	{
		qCDebug(dwidsources)<<"Sources: edited"<<i;
	}
	emit sourceSelectedSig(firstPk);
}

void DWidSources::tableSelChangedSl(const QItemSelection &selected, const QItemSelection &deselected)
{
	Q_UNUSED(selected)
	Q_UNUSED(deselected)
	QModelIndexList li=ui->treeView->selectionModel()->selectedRows();
	std::set< int > selPks;
	foreach(QModelIndex index, li)
	{
		//qDebug()<<"Source selection"<<index.row()<<index.column();
		selPks.insert(qobject_cast<SourceQueryModel*>(ui->treeView->model())->index2Pk(index));
	}
	if(selPks.size()==1)
	{
		if(!supressSelectionSignal)
			emit sourceSelectedSig(*(selPks.begin()));
		ui->pushDelete->setEnabled(true);
		ui->pushNewRecursive->setEnabled(true);
		//qDebug()<<model->data(model->index(*(selr.begin()), 1), Qt::DisplayRole);
	}
	else
	{
		ui->pushDelete->setEnabled(false);
		ui->pushNewRecursive->setEnabled(false);
	}
}

void DWidSources::on_pushNew_clicked()
{
	QModelIndex mi=ui->treeView->currentIndex();
	Source *pfn=nullptr;
	if(mi.isValid())
	{
		int spk=qobject_cast<SourceQueryModel*>(ui->treeView->model())->index2Pk(mi);
		Source &psrc=DCON.getSource(spk);
		if((psrc.isValid() && psrc.getPk()>0))
			pfn=&psrc;
	}
	DlgEditSource dlg(pfn, this);
	if(dlg.exec()==QDialog::Accepted)
	{
		qobject_cast<SourceQueryModel*>(ui->treeView->model())->reload(dlg.getParent());
	}
}

void DWidSources::on_pushNewRecursive_clicked()
{
	QModelIndex mi=ui->treeView->currentIndex();
	Source *pfn=nullptr;
	if(mi.isValid())
	{
		int spk=qobject_cast<SourceQueryModel*>(ui->treeView->model())->index2Pk(mi);
		Source &psrc=DCON.getSource(spk);
		if((psrc.isValid() && psrc.getPk()>0))
			pfn=&psrc;
	}
	if(!pfn)
	{
		QMessageBox::warning(this, tr("Invalid Location"), tr("Automatic creation of sources on root level makes no sense. Select a sourcee to sytart from."));
		return;
	}
	QDir dir(pfn->location());
	QStringList alldirs=dir.entryList(QStringList(), QDir::AllDirs | QDir::NoDotAndDotDot | QDir::NoSymLinks);
	QStringList dirs;
	foreach(QString d, alldirs)
	{
		QSqlQuery qcnt(QStringLiteral(u"select count(1) from sources where val='%1'").arg(dir.absoluteFilePath(d)));
		if(qcnt.next())
		{
			int icnt=qcnt.value(0).toInt();
			qCDebug(dwidsources)<<qcnt.lastQuery()<<"returned"<<icnt;
			if(!icnt)
				dirs<<d;
		}
		else
			qWarning()<<"DWidSources::on_pushNewRecursive_clicked source count failed"<<qcnt.lastError().text()<<qcnt.lastQuery();
	}
	if(!dirs.size())
	{
		QMessageBox::information(this, tr("Automatic Source creation"), tr("Found no (new) sub-folders in %1.").arg(pfn->location()));
		return;
	}
	if(QMessageBox::question(this, tr("Automatic Source creation"), tr("Shall I create the folowing folder as new sources? %1").arg(dirs.join(", ")), QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes)==QMessageBox::No)
		return;
	foreach(QString d, dirs)
	{
		Source nsrc(pfn, nullptr);
		//nsrc.setComment("created by...", false);
		nsrc.setLocation(dir.absoluteFilePath(d), true);
	}
	qobject_cast<SourceQueryModel*>(ui->treeView->model())->reload(pfn);
}

void DWidSources::on_pushDelete_clicked()
{
	QModelIndex mi=ui->treeView->currentIndex();
	int spk=qobject_cast<SourceQueryModel*>(ui->treeView->model())->index2Pk(mi);
	if(spk<=0)
	{
		qWarning()<<"on_pushDelete_clicked on invalid index"<<mi;
		ui->pushDelete->setEnabled(false);
		return;
	}
	emit sourceSelectedSig(0); // take away the attention
	ui->pushDelete->setEnabled(false);
	ui->pushNewRecursive->setEnabled(false);
	Source &psrc=DCON.getSource(DCON.getSource(spk).getParentPk());
	if(Source::deleteComplete(spk, DCON.db()->db(), this))
		model->reload(&psrc);
}

void DWidSources::on_pushReload_clicked()
{
	emit sourceSelectedSig(0);
	DCON.dropSourceCacheAll();
	model->reload(nullptr);
	ui->treeView->update();
	ui->pushNew->setEnabled(true);
	ui->pushNewRecursive->setEnabled(false);
	ui->pushReload->setEnabled(true);
	ui->pushDelete->setEnabled(false);
}

void DWidSources::on_treeView_doubleClicked(const QModelIndex &index)
{
	if(index.column()==0)
	{
		DlgEditSource dlg(static_cast<SourceQueryModel*>(ui->treeView->model())->index2Pk(index), this);
		dlg.exec(); //==QDialog::Accepted) no explicit update required
	}
}

void DWidSources::customContextMenuRequestedSl(const QPoint &pos)
{
	QModelIndex idx=ui->treeView->indexAt(pos);
	QMenu menu(this);
	//QAction *act=
	menu.addAction(QIcon(":/ic/res/icons8-edit-image-48.png"), tr("edit"), [this, idx]() {on_treeView_doubleClicked(idx);});
	//act->setEnabled;
	menu.exec(ui->treeView->mapToGlobal(pos));
}
