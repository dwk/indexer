#ifndef DWIDTAGS_H
#define DWIDTAGS_H

#include "idxh.h"
#include <list>
#include <QWidget>
#include "globalenum.h"
#include "widactor.h"
#include "widlabelsmenu.h"

Q_DECLARE_LOGGING_CATEGORY(dwidtags)

namespace Ui {
class DWidTags;
}

class DWidTags : public QWidget, public WidActor
{
	Q_OBJECT
public:
	explicit DWidTags(QWidget *parent = 0);
	~DWidTags();
	// WidActor
	virtual void connectAll(ObjectMSP &otherActors);
public slots:
	virtual void setupSl() {}
	//void imageActionGSl(int imagePk, ImageAction action);
	void selectTagSl(int tagIndex); // needed for key shortcuts
	void loadTagsSl(); // used for tag editor updates and reset-tree key shortcut
private slots:
private:
	Ui::DWidTags *ui;
signals:
	void labelActionGSig(int labelPk, LabelAction action);
	void imageActionGSig(int imagePk, ImageAction action);
};

#endif // DWIDTAGS_H
