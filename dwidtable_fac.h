#ifndef DWIDTABLEFAC_H
#define DWIDTABLEFAC_H

#include "dockwidfactory.h"

namespace
{
QWidget * DockCreator(QWidget * parent)
{
	return new DWidTable(parent);
}
const char *techname="dwid_table";
bool dockWidRegistered=DockWidFactory::get().registerDockWid("Table", techname, Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea, Qt::BottomDockWidgetArea, DockCreator);
}
#endif // DWIDTABLEFAC_H
