#ifndef DWIDTAGSFAC_H
#define DWIDTAGSFAC_H

#include "dockwidfactory.h"

namespace
{
QWidget * DockCreator(QWidget * parent)
{
	return new DWidTags(parent);
}
const char *techname="dwid_tags";
bool dockWidRegistered=DockWidFactory::get().registerDockWid("Tags", techname, Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea, Qt::BottomDockWidgetArea, DockCreator);
}
#endif // DWIDTAGSFAC_H
