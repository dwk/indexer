#include "globalenum.h"

QString enumImageActionStr(ImageAction ia)
{
	switch(ia)
	{
	case ImageAction::SELECT:
		return QString("SELECT");
	case ImageAction::SELECT_MULTI:
		return QString("SELECT_MULTI");
	case ImageAction::MARK:
		return QString("MARK");
	case ImageAction::UNMARK:
		return QString("UNMARK");
	case ImageAction::MARK_MULTI:
		return QString("MARK_MULTI");
	case ImageAction::DELETE:
		return QString("DELETE");
	case ImageAction::SAVE_AS:
		return QString("SAVE_AS");
	case ImageAction::LABELS_CHANGED:
		return QString("LABELS_CHANGED");
	case ImageAction::ATTRIB_CHANGED:
		return QString("ATTRIB_CHANGED");
	case ImageAction::PIXEL_CHANGED:
		return QString("PIXEL_CHANGED");
	}
	return QStringLiteral("unknown: %1").arg((int)ia);
}

QString enumLabelActionStr(LabelAction ia)
{
	switch(ia)
	{
	case LabelAction::EXPAND:
		return QString("EXPAND");
	case LabelAction::MARKED_FORCECLEAR:
		return QString("MARKED_FORCECLEAR");
	case LabelAction::MARKED_FORCESET:
		return QString("MARKED_FORCECLEAR");
	case LabelAction::NONE:
		return QString("NONE");
	case LabelAction::SEARCH:
		return QString("SEARCH");
	case LabelAction::SELECTION_APPLY:
		return QString("SELECTION_APPLY");
	case LabelAction::SELECTION_FORCECLEAR:
		return QString("SELECTION_FORCECLEAR");
	case LabelAction::SELECTION_FORCESET:
		return QString("SELECTION_FORCESET");
	case LabelAction::SOURCE_FORCECLEAR:
		return QString("SOURCE_FORCECLEAR");
	case LabelAction::SOURCE_FORCESET:
		return QString("SOURCE_FORCESET");
	}
	return QStringLiteral("unknown: %1").arg((int)ia);
}

QString enumWidOperationModeStr(WidOperationMode wom)
{
	switch(wom)
	{
	case WidOperationMode::DATABASE_UPDATE:
		return QString("DATABASE_UPDATE");
	case WidOperationMode::APPLY_SIG:
		return QString("APPLY_SIG");
	}
	return QStringLiteral("unknown: %1").arg((int)wom);
}
