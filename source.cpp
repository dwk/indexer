#include "source.h"
#include <QSqlQuery>
#include <QSqlError>
//#include <QRegularExpression>
//#include <QRegularExpressionMatchIterator>
#include "idxi.h"

Q_LOGGING_CATEGORY(source, "source")


int Source::location2Pk(const QString &urlStr)
{
	QSqlQuery q(QString("select pk from sources where val=%1").arg(DataContainer::toSqlString(urlStr)));
	if(q.lastError().isValid())
	{
		qWarning()<<"sourceUrl2Pk: failed query"<<q.lastQuery()<<q.lastError();
		return -1;
	}
	if(q.next())
	{
		return q.value(0).toInt();
	}
	qWarning()<<"No such source: "<<urlStr<<q.lastQuery()<<q.lastError();
	return -1;
}

int Source::sizeOfDir(const QString &dirPath)
{
	QDir d(dirPath);
	QStringList files=d.entryList(QStringList()<<"*.jpg"<<"*.jpeg"<<"*.png", QDir::Files);
	return static_cast<int>(files.size());
}

bool Source::deleteComplete(int srcPk, QSqlDatabase & db, QWidget *messageParent)
{
	int ipkCnt=0;
	{ // brace query
		QSqlQuery q(QString("select count(1) from images where source=%1").arg(srcPk), db);
		if(q.next())
		{
			bool ok=false;
			ipkCnt=q.value(0).toInt(&ok);
			if(!ok)
			{
				qCritical()<<"Source::deleteComplete very strange...";
				QMessageBox::critical(messageParent, tr("Database Error"), tr("Error during Delete (Query: %1, Error: %2)").arg(q.lastQuery(), q.lastError().text()));
				return false;
			}
		}
		else
		{
			qCritical()<<"Source::deleteComplete ipk select FAILED"<<q.lastQuery()<<q.lastError();
			QMessageBox::critical(messageParent, tr("Database Error"), tr("Error during Delete (Query: %1, Error: %2)").arg(q.lastQuery(), q.lastError().text()));
			return false;
		}
	}
	db.transaction();
	if(ipkCnt>0)
	{
		if(QMessageBox::question(messageParent, tr("Extended Delete Operation"), tr("This source conatins %1 references to tagged images. Deleting the source will also delete all these references and associated tags. Delete anyway?").arg(ipkCnt),
			QMessageBox::Yes | QMessageBox::No, QMessageBox::No)==QMessageBox::No)
		{
			db.rollback();
			return false;
		}
		QSqlQuery q(QString("delete from images where source=%1").arg(srcPk), db);
		if(q.lastError().isValid())
		{
			db.rollback();
			qCritical()<<"Source::deleteComplete ipk delete FAILED"<<q.lastQuery()<<q.lastError();
			QMessageBox::critical(messageParent, tr("Database Error"), tr("Error during Delete (Query: %1, Error: %2)").arg(q.lastQuery(), q.lastError().text()));
			return false;
		}
	}
	QSqlQuery q(QStringLiteral("delete from sources where pk=%1").arg(srcPk), db);
	if(q.lastError().isValid())
	{
		db.rollback();
		qCritical()<<"delete from merge FAILED"<<q.lastError().text()<<q.lastQuery();
		return false;
	}
	bool res=db.commit();
	if(res)
		DCON.dropSourceCache(srcPk); // will delete underlying
	return res;
}

Source::Source(Source *parentForNew, QObject *parent) : QObject(parent)
{
	pk=-3; // -2 would do eiter...
	dbDirty=true;
	if(parentForNew)
		parentPk=parentForNew->getPk();
	ok=true;
}

Source::Source(int sourcePk, QObject *parent) : QObject(parent), pk(sourcePk)
{
	setObjectName(QString("Source%1").arg(pk));
	if(sourcePk<0)
	{
		if(sourcePk==-1)
		{
			ok=true;
			parentPk=-42; // really invalid
			comment=tr("tree root");
			qCDebug(source)<<"Source: tree root dummy";
		}
		else
			qCDebug(source)<<"Source: invalid pk"<<sourcePk;
		return;
	}
	QSqlQuery q(QString("select s.type, s.parent, s.val, s.comment, p.val from sources s LEFT join sources p on p.pk=s.parent where s.pk=%1").arg(pk));
	if(q.lastError().isValid())
	{
		qWarning()<<"Source: failed query"<<q.lastQuery()<<q.lastError();
		return;
	}
	if(!q.next())
	{
		qWarning()<<"No such Source: "<<sourcePk<<q.lastQuery()<<q.lastError();
		return;
	}
	if(q.value(0).toInt()==1)
		ok=true;
	else
		qCDebug(source)<<"Source:"<<pk<<"unsupported type"<<q.value(0).toInt();
	QString val=q.value(2).toString();
	dir=QDir(val);
	if(q.value(1).isNull())
	{
		parentPk=-1; // pointing to a virtual tree root
		disp=val;
	}
	else
	{
		parentPk=q.value(1).toInt();
		QString base=q.value(4).toString();
		int cut=0;
		if(val.size()>base.size())
		{
			for(int maxs=base.size(); cut<maxs && base[cut]==val[cut]; ++cut)
				;
		}
		disp=val.mid(cut);
	}
	files=dir.entryList(QStringList()<<"*.jpg"<<"*.jpeg"<<"*.png"<<"*.JPG"<<"*.JPEG"<<"*.PNG", QDir::Files);
	comment=q.value(3).toString();
	qCDebug(source)<<"Source:"<<pk<<dir.path()<<"containing"<<files.size()<<"files";
}

bool Source::commit2Db()
{
	if(!isValid() || !dbDirty || pk==-1)
		return false;
	if(pk<0)
	{
		QSqlQuery q(QStringLiteral("insert into sources "
								   "(type, parent, val, comment) values "
								   "(%1,   %2,     %3,  %4)").\
					arg("1").arg(parentPk>0?QString::number(parentPk):QStringLiteral("NULL")).arg(DataContainer::toSqlString(dir.path()), DataContainer::toSqlString(comment, true)),\
					DCON.db()->db());
		if(q.lastError().isValid())
		{
			qCritical()<<objectName()<<"Source::commit2Db FAILED"<<q.lastError().text()<<q.lastQuery();
			return false;
		}
		dbDirty=false;
		bool pok=false;
		pk=q.lastInsertId().toInt(&pok);
		if(!pok)
		{
			qCritical()<<"Source::commit2Db insert pk FAILED"<<objectName();
			return false;
		}
		qCDebug(source)<<"Source::commit2Db inserted"<<objectName()<<pk;
		return true;
	}
	// update
	QSqlQuery q(QString("update sources set val=%2, comment=%3 where pk=%1").arg(pk).\
				arg(DataContainer::toSqlString(location()), DataContainer::toSqlString(comment, true)), DCON.db()->db());
	if(q.lastError().isValid())
	{
		qCritical()<<"Source::commit2Db update FAILED"<<q.lastError()<<q.lastQuery();
		return false;
	}
	dbDirty=false;
	return true;
}

int Source::getChildCnt()
{
	if(!ok)
		return 0;
	if(!childrenLoaded)
	{
		QSqlQuery q(QString("select pk from sources where parent%1%2 order by val").arg(pk<0?QStringLiteral(" is NULL"):QStringLiteral("=")).arg(pk<0?QStringLiteral(""):QString::number(pk)));
		if(q.lastError().isValid())
		{
			qWarning()<<"Source: failed query"<<q.lastQuery()<<q.lastError();
			return 0;
		}
		while(q.next())
		{
			children.push_back(q.value(0).toInt());
		}
		childrenLoaded=true;
	}
	return children.size();
}

int Source::getChildPk(int row)
{
	if(!ok)
	{
		qWarning()<<"Source: getChildPk on invalid"<<row<<pk;
		return -43;
	}
	if(!childrenLoaded)
		getChildCnt();
	if(row<0 || row>=(int)children.size())
	{
		qWarning()<<"Source: getChildPk invalid row"<<row<<children.size();
		return -44;
	}
	return children.at(row);
}

int Source::getRow()
{
	if(!ok || pk<0)
		return 0;
	Source &src=DCON.getSource(parentPk);
	return src.getIndexOf(pk);
}

bool Source::setLocation(const QString &loc, bool autoCommit)
{
	if(dir.path()==loc)
		return true;
	dir=QDir(loc);
	if(parentPk>0)
	{
		QString base=DCON.getSource(parentPk).location();
		int cut=0;
		if(loc.size()>base.size())
		{
			for(int maxs=base.size(); cut<maxs && base[cut]==loc[cut]; ++cut)
				;
		}
		disp=loc.mid(cut);
	}
	else
	{
		disp=loc;
	}
	files=dir.entryList(QStringList()<<"*.jpg"<<"*.jpeg"<<"*.png"<<"*.JPG"<<"*.JPEG"<<"*.PNG", QDir::Files);
	dbDirty=true;
	if(autoCommit)
		return commit2Db();
	return true;
}

bool Source::renameLocation(const QString &loc, bool autoCommit)
{
	QDir d;
	if(d.rename(location(), loc))
		return setLocation(loc, autoCommit);
	return false;
}

bool Source::setComment(const QString &c, bool autoCommit)
{
	if(comment==c)
		return true;
	comment=c;
	dbDirty=true;
	if(autoCommit)
		return commit2Db();
	return true;
}

void Source::imageMergeLists(QStringList &missingInDb, std::vector<int> &missingInSource)
{
	if(!ok || pk<0)
		return;
	missingInDb.clear();
	missingInSource.clear();
	std::set<QString> fs;
	foreach(QString str, files)
	{
		fs.insert(str);
	}
	QSqlQuery q(QString("select pk, val from images where source=%1").arg(pk));
	qCDebug(source)<<"mergeList: query"<<q.lastQuery();
	while(q.next())
	{
		qCDebug(source)<<"mergeList: checking"<<q.value(1).toString();
		auto fit=fs.find(q.value(1).toString());
		if(fit==fs.end())
		{
			missingInSource.push_back(q.value(0).toInt());
			qCDebug(source)<<"mergeList: missing in source"<<q.value(0).toInt()<<q.value(1).toString();
		}
		else
		{
			qCDebug(source)<<"mergeList: confirmed"<<*fit;
			fs.erase(fit);
		}
	}
	for(auto fit=fs.begin(); fit!=fs.end(); ++fit)
	{
		qCDebug(source)<<"mergeList: missing in db"<<*fit;
		missingInDb<<*fit;
	}
}

bool Source::readImageInfo(const QString &imageName, Image::ImageAttrs &attrs)
{
	if(!ok || pk<0)
		return false;
	attrs.clear();
	QFileInfo fi(dir, imageName);
	if(!fi.size())
		return false;
	attrs.insert(Image::ImageAttrs::value_type(Image::ImageAttr::FILESIZE, (int)fi.size()));
	QDateTime dt=fi.birthTime();
	if(!dt.isValid())
		dt=fi.lastModified();
	if(!dt.isValid())
		dt=fi.metadataChangeTime();
	if(!dt.isValid())
		dt=QDateTime::currentDateTime();
	attrs.insert(Image::ImageAttrs::value_type(Image::ImageAttr::CREATE_DATE_FILE, dt));
	addExifInfo(imageName, attrs); // return true even if exif read fails
	return true;
}

QPixmap *Source::readImagePixels(const QString &imageName, int scale2Width, Image::ExifOrientation ori)
{
	if(!ok || pk<0)
		return nullptr;
	QPixmap *res=new QPixmap(dir.absoluteFilePath(imageName));
	if(res->isNull())
	{
		qCDebug(source)<<"Source: NULL"<<imageName;
		delete res;
		return nullptr;
	}
	QPixmap *newres=nullptr;
	if(ori==Image::ExifOrientation::UP)
	{
		if(scale2Width)
		{
			if(res->width()>res->height())
				newres=new QPixmap(res->scaledToWidth(scale2Width));
			else
				newres=new QPixmap(res->scaledToHeight(scale2Width));
			delete res;
			return newres;
		}
		return res;
	}
	QTransform t;
	switch(ori)
	{
	case Image::ExifOrientation::UP:
		break; // never here...
	case Image::ExifOrientation::LEFT:
		t.rotate(90.);
		break;
	case Image::ExifOrientation::RIGHT:
		t.rotate(-90.);
		break;
	case Image::ExifOrientation::DOWN:
		t.rotate(180.);
		break;
	case Image::ExifOrientation::MIRROR_UP:
		t.scale(-1., 1.);
		break;
	case Image::ExifOrientation::MIRROR_LEFT:
		t.rotate(90.);
		t.scale(-1., 1.);
		break;
	case Image::ExifOrientation::MIRROR_RIGHT:
		t.rotate(-90.);
		t.scale(-1., 1.);
		break;
	case Image::ExifOrientation::MIRROR_DOWN:
		t.rotate(180.);
		t.scale(-1., 1.);
		break;
	}
	if(scale2Width)
	{
		if(res->width()>res->height())
			newres=new QPixmap(res->scaledToWidth(scale2Width).transformed(t));
		else
			newres=new QPixmap(res->scaledToHeight(scale2Width).transformed(t));
	}
	else
		newres=new QPixmap(res->transformed(t));
	delete res;
	return newres;
}

bool Source::deleteImagePermanent(const QString &imageName)
{
	if(!ok || pk<0)
		return false;
	return dir.remove(imageName);
}

int Source::getIndexOf(int childPk)
{
	if(!ok)
		return 0;
	if(!childrenLoaded)
		getChildCnt();
	int r=0;
	for(auto it=children.begin(); it!=children.end(); ++it, ++r)
	{
		if(*it==childPk)
			return r;
	}
	return 0;
}

bool Source::addExifInfo(const QString &imageName, Image::ImageAttrs &attrs)
{
	if(!ok || pk<0)
		return false;
	// see https://libexif.github.io/api/index.html
	// see https://github.com/libexif/exif/tree/master/exif
	QString fn=dir.absoluteFilePath(imageName);
	ExifData *ed = exif_data_new_from_file(fn.toLocal8Bit().constData());
	if(!ed)
	{
		//qWarning()<<"File not readable or no EXIF data in file"<<fn;
		return false;
	}
	char cbuf[1024];
	ExifEntry *entry = exif_content_get_entry(ed->ifd[EXIF_IFD_0], EXIF_TAG_MODEL);
	if(!entry)
		entry = exif_content_get_entry(ed->ifd[EXIF_IFD_1], EXIF_TAG_MODEL);
	if(!entry)
		entry = exif_content_get_entry(ed->ifd[EXIF_IFD_EXIF], EXIF_TAG_MODEL);
	if(entry)
		attrs.insert(Image::ImageAttrs::value_type(Image::ImageAttr::CAMERA, QString::fromUtf8((char*)entry->data).trimmed()));
	entry = exif_content_get_entry(ed->ifd[EXIF_IFD_0], EXIF_TAG_DATE_TIME);
	if(!entry)
		entry = exif_content_get_entry(ed->ifd[EXIF_IFD_1], EXIF_TAG_DATE_TIME);
	if(!entry)
		entry = exif_content_get_entry(ed->ifd[EXIF_IFD_EXIF], EXIF_TAG_DATE_TIME);
	if(entry)
	{
		QDateTime dt=QDateTime::fromString(QString::fromUtf8((char*)entry->data).trimmed(), "yyyy:MM:dd HH:mm:ss");
		if(dt.isValid())
			attrs.insert(Image::ImageAttrs::value_type(Image::ImageAttr::CREATE_DATE_EXIF, dt));
	}
	entry = exif_content_get_entry(ed->ifd[EXIF_IFD_0], EXIF_TAG_ORIENTATION);
	if(!entry)
		entry = exif_content_get_entry(ed->ifd[EXIF_IFD_1], EXIF_TAG_ORIENTATION);
	if(!entry)
		entry = exif_content_get_entry(ed->ifd[EXIF_IFD_EXIF], EXIF_TAG_ORIENTATION);
	if(entry)
	{
		ExifByteOrder o=exif_data_get_byte_order(entry->parent->parent);
		// 1 upright
		// 2 mirrored upright
		// 3 upside down
		// 4 mirrored upside down
		// 5 mirrored turned on left side
		// 6 turned on left side
		// 7 mirrored turned on right side
		// 8 turned on right side
		int ori=exif_get_short (entry->data, o);
		attrs.insert(Image::ImageAttrs::value_type(Image::ImageAttr::EXIF_ORIENTATION, ori));
		//qDebug()<<"EXIF orientation"<<ori;
	}
	QString lat, latref, lon, lonref, heistr;
	entry=exif_content_get_entry(ed->ifd[EXIF_IFD_GPS], (ExifTag)EXIF_TAG_GPS_LATITUDE);
	if(entry)
	{
		exif_entry_get_value(entry, cbuf, 1024);
		lat=QString(cbuf).trimmed();
	}
	entry=exif_content_get_entry(ed->ifd[EXIF_IFD_GPS], (ExifTag)EXIF_TAG_GPS_LATITUDE_REF);
	if(entry)
	{
		exif_entry_get_value(entry, cbuf, 1024);
		latref=QString(cbuf).trimmed();
	}
	entry=exif_content_get_entry(ed->ifd[EXIF_IFD_GPS], (ExifTag)EXIF_TAG_GPS_LONGITUDE);
	if(entry)
	{
		exif_entry_get_value(entry, cbuf, 1024);
		lon=QString(cbuf).trimmed();
	}
	entry=exif_content_get_entry(ed->ifd[EXIF_IFD_GPS], (ExifTag)EXIF_TAG_GPS_LONGITUDE_REF);
	if(entry)
	{
		exif_entry_get_value(entry, cbuf, 1024);
		lonref=QString(cbuf).trimmed();
	}
	entry=exif_content_get_entry(ed->ifd[EXIF_IFD_GPS], (ExifTag)EXIF_TAG_GPS_ALTITUDE);
	if(entry)
	{
		exif_entry_get_value(entry, cbuf, 1024);
		heistr=QString(cbuf).trimmed();
	}
	if(!lon.isEmpty() && !lat.isEmpty())
	{
		GpsCoord gps(lon, lonref, lat, latref, heistr);
		if(gps.hasValidLonLat() || gps.hasValidHeight())
		{
			QVariant vgps;
			vgps.setValue(gps);
			attrs.insert(Image::ImageAttrs::value_type(Image::ImageAttr::GPS, vgps));
		}
	}
	QStringList camSets;
	entry = exif_content_get_entry(ed->ifd[EXIF_IFD_EXIF], EXIF_TAG_EXPOSURE_TIME);
	if(!entry)
		entry = exif_content_get_entry(ed->ifd[EXIF_IFD_0], EXIF_TAG_EXPOSURE_TIME);
	if(!entry)
		entry = exif_content_get_entry(ed->ifd[EXIF_IFD_1], EXIF_TAG_EXPOSURE_TIME);
	if(entry)
	{
		exif_entry_get_value(entry, cbuf, 1024);
		camSets<<QString(cbuf).trimmed();
	}
	entry = exif_content_get_entry(ed->ifd[EXIF_IFD_EXIF], EXIF_TAG_FNUMBER);
	if(!entry)
		entry = exif_content_get_entry(ed->ifd[EXIF_IFD_0], EXIF_TAG_FNUMBER);
	if(!entry)
		entry = exif_content_get_entry(ed->ifd[EXIF_IFD_1], EXIF_TAG_FNUMBER);
	if(entry)
	{
		exif_entry_get_value(entry, cbuf, 1024);
		camSets<<QString(cbuf).trimmed();
	}
	entry = exif_content_get_entry(ed->ifd[EXIF_IFD_EXIF], EXIF_TAG_FOCAL_LENGTH);
	if(!entry)
		entry = exif_content_get_entry(ed->ifd[EXIF_IFD_0], EXIF_TAG_FOCAL_LENGTH);
	if(!entry)
		entry = exif_content_get_entry(ed->ifd[EXIF_IFD_1], EXIF_TAG_FOCAL_LENGTH);
	if(entry)
	{
		exif_entry_get_value(entry, cbuf, 1024);
		camSets<<QString(cbuf).trimmed();
	}
	entry = exif_content_get_entry(ed->ifd[EXIF_IFD_EXIF], EXIF_TAG_FOCAL_LENGTH_IN_35MM_FILM);
	if(!entry)
		entry = exif_content_get_entry(ed->ifd[EXIF_IFD_0], EXIF_TAG_FOCAL_LENGTH_IN_35MM_FILM);
	if(!entry)
		entry = exif_content_get_entry(ed->ifd[EXIF_IFD_1], EXIF_TAG_FOCAL_LENGTH_IN_35MM_FILM);
	if(entry)
	{
		exif_entry_get_value(entry, cbuf, 1024);
		camSets<<QString(cbuf).trimmed();
	}
	if(camSets.size())
		attrs.insert(Image::ImageAttrs::value_type(Image::ImageAttr::CAMERA_SETTING, camSets.join(';')));
	return true;
}
