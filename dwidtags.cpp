#include "dwidtags.h"
#include "dwidtags_fac.h"
#include "ui_dwidtags.h"
#include "source.h"
#include "globalenum.h"
#include "idxi.h"

Q_LOGGING_CATEGORY(dwidtags, "dwidtags")

DWidTags::DWidTags(QWidget *parent) :
	QWidget(parent),
	ui(new ::Ui::DWidTags)
{
	setObjectName(techname);
	ui->setupUi(this);
	connect(ui->labelsMenu, &WidLabelsMenu::labelActionGSig, this, &DWidTags::labelActionGSig); // pass through SEARCH
	connect(ui->labelsMenu, &WidLabelsMenu::imageActionGSig, this, &DWidTags::imageActionGSig);
}

DWidTags::~DWidTags()
{
	delete ui;
}

void DWidTags::connectAll(ObjectMSP &otherActors)
{
	qDebug()<<"connectAll"<<objectName();
	for(auto it=otherActors.begin(); it!=otherActors.end(); ++it)
	{
		QObject *obj=it->second;
		if(obj->objectName()==objectName())
			continue;
		const QMetaObject* mo = obj->metaObject();
		//if(mo->indexOfSignal("imageActionGSig(int,ImageAction)")>=0)
		//{
		//	qDebug()<<"    "<<obj->objectName()<<"imageActionGSig(int,ImageAction)";
		//	connect(obj, SIGNAL(imageActionGSig(int,ImageAction)), this, SLOT(imageActionGSl(int,ImageAction)));
		//}
		if(mo->indexOfSignal("tagsChangedSig()")>=0)
		{
			qDebug()<<"    "<<obj->objectName()<<"tagsChangedSig()";
			connect(obj, SIGNAL(tagsChangedSig()), this, SLOT(loadTagsSl()));
		}
	}
}

/* actually - we don't care for images :-)
void DWidTags::imageActionGSl(int imagePk, ImageAction action)
{
	switch(action)
	{
	case ImageAction::SELECT:
	case ImageAction::SELECT_MULTI:
	case ImageAction::MARK:
	case ImageAction::UNMARK:
	case ImageAction::MARK_MULTI:
	case ImageAction::DELETE:
	case ImageAction::SAVE_AS:
	case ImageAction::LABELS_CHANGED:
	case ImageAction::ATTRIB_CHANGED:
	case ImageAction::PIXEL_CHANGED:
		break;
	}
}*/

void DWidTags::selectTagSl(int tagIndex)
{
	ui->labelsMenu->selectTagSl(tagIndex);
}

void DWidTags::loadTagsSl()
{
	ui->labelsMenu->resetSl();
}

