#ifndef DWIDTAGEDIT_H
#define DWIDTAGEDIT_H

#include "idxh.h"
#include <QWidget>
#include "widactor.h"
#include "widlabelsmenu.h"

Q_DECLARE_LOGGING_CATEGORY(dwidtagedit)

class QTreeWidgetItem;
namespace Ui {
class DWidTagEdit;
}

class DWidTagEdit : public QWidget, public WidActor
{
	Q_OBJECT
public:
	explicit DWidTagEdit(QWidget *parent = 0);
	~DWidTagEdit();
	// WidActor
	virtual void connectAll(ObjectMSP &otherActors);
public slots:
	virtual void setupSl();
	//
private slots:
	void updatePreviewSl(const QModelIndex &current, const QModelIndex &previous);
	void updatePreview2Sl(QTreeWidgetItem *item, int column);
	void updatePreviewInt(int tagPk, QString val, QString deco);
	void on_pushAddSib_clicked();
	void on_pushAddSub_clicked();
	void on_pushAdd(QTreeWidgetItem *parent);
	void on_pushEdit_clicked();
	void on_pushDelete_clicked();
	void on_pushCommit_clicked();
	void on_pushReset_clicked();
	void tagtreeChangedSl();
	void setDirtySl();
	void updateButtonsSl();
private:
	typedef std::multimap<int, QTreeWidgetItem *> DeleteItemMMP;
	typedef std::set<QTreeWidgetItem *> SequSP;
	void addSubItems(QTreeWidgetItem *twp, int tagPk, int level);
	void iterateItem(QTreeWidgetItem *item, int sequence, int level, SequSP &sequUpdates, DeleteItemMMP & deleteItems);
	Ui::DWidTagEdit *ui;
	TagWid *preview=nullptr;
signals:
	void tagsChangedSig();
};

#endif // DWIDTAGEDIT_H
