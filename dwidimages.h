#ifndef DWIDIMAGES_H
#define DWIDIMAGES_H

#include "idxh.h"
#include <list>
#include <QWidget>
#include "widactor.h"
#include "source.h"
#include "imagequerymodel.h"
#include "globalenum.h"
#include <QSqlRelationalTableModel>
#include <QItemSelection>
#include <QStandardItem>

Q_DECLARE_LOGGING_CATEGORY(dwidimages)

namespace Ui {
class DWidImages;
}

class DWidImages : public QWidget, public WidActor
{
	Q_OBJECT
public:
	explicit DWidImages(QWidget *parent = 0);
	~DWidImages();
// WidActor
public:
	virtual void connectAll(ObjectMSP &otherActors);
public slots:
	virtual void setupSl();
//
	void nextImageSl();
	void prevImageSl();
	void editDescriptionSl();
	void editImageSl();
	void deleteImageSl();
	void openGimpSl();
protected:
	enum class Mode {EMPTY=0, QUERY, TABLE};
	//virtual void keyPressEvent(QKeyEvent *event);
private:
	void checkForUnsavedChanges();
	void applyTableGeom();
private slots:
	void customMenuRequestedSl(QPoint pos);
	void switchToSourceSl(const QString &source);
	void sourceSelectedSl(int pk);
	void querySelectedSl(void *so);
	void imageActionGSl(int imagePk, ImageAction action);
	void modelPreResetSl();
	void modelResetSl();
	void rowResizedSl(int logicalIndex, int oldSize, int newSize);
	void columnResizedSl(int logicalIndex, int oldSize, int newSize);
	void tableSelChangedSl(const QItemSelection &selected, const QItemSelection &deselected);
	void on_pushSubmit_clicked();
	void on_pushMerge_clicked();
	void on_pushReload_clicked();
signals:
	void imageActionGSig(int imagePk, ImageAction action);
	void sourceSelectedSig(int sourcePk);
private:
	Ui::DWidImages *ui;
	ImageQueryModel *iModel;
	QItemSelection lastSelected, lastDeselected;
	int selectedImagePk=-1;
	bool supressSelectionSgnal=false;
};

#endif // DWIDIMAGES_H
